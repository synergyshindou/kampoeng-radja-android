package com.cleva.kampoengradja.database;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.util.Log;

import com.cleva.kampoengradja.model.OutletItem;
import com.cleva.kampoengradja.model.TiketParkirResp;

@Database(entities = {TiketParkirResp.class, OutletItem.class}, version = 1, exportSchema = false)
public abstract class TiketDatabase extends RoomDatabase {

    private static final String TAG = "TiketDatabase";
    private static TiketDatabase instance;

    public abstract TiketDao noteDao();
    public abstract OutletDao outletDao();

    public static synchronized TiketDatabase getInstance(Context context){
        if (instance == null){
            instance = Room.databaseBuilder(context.getApplicationContext(),
                    TiketDatabase.class, "kampoeng_database")
                    .fallbackToDestructiveMigration()
                    .addCallback(roomCallback)
                    .build();
        }
        return instance;
    }

    private static Callback roomCallback = new Callback(){
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);
            Log.i(TAG, "onCreate: DB ");
        }
    };

}
