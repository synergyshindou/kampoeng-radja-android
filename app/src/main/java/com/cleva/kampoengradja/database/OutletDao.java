package com.cleva.kampoengradja.database;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.cleva.kampoengradja.model.OutletItem;

import java.util.List;

@Dao
public interface OutletDao {

    @Insert
    void insert(OutletItem note);

    @Update
    void update(OutletItem note);

    @Delete
    void delete(OutletItem note);

    @Query("DELETE FROM outlet_item_table")
    void deleteAllOutletItems();

    @Query("SELECT * FROM outlet_item_table ORDER BY itemId DESC")
    LiveData<List<OutletItem>> getAllOutletItems();

    @Query("SELECT * FROM outlet_item_table WHERE itemId = :itemId")
    List<OutletItem> getItemById(int itemId);
}
