package com.cleva.kampoengradja.database;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.cleva.kampoengradja.model.TiketParkirResp;

import java.util.List;

@Dao
public interface TiketDao {

    @Insert
    void insert(TiketParkirResp note);

    @Update
    void update(TiketParkirResp note);

    @Delete
    void delete(TiketParkirResp note);

    @Query("DELETE FROM tiket_table")
    void deleteAllTiketParkirResps();

    @Query("SELECT * FROM tiket_table ORDER BY itemId DESC")
    LiveData<List<TiketParkirResp>> getAllTiketParkirResps();

    @Query("SELECT * FROM tiket_table WHERE itemId = :itemId")
    List<TiketParkirResp> getTiketById(int itemId);
}
