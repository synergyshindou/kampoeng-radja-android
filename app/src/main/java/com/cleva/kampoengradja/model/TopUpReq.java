
package com.cleva.kampoengradja.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TopUpReq implements Parcelable
{

    @SerializedName("cardUid")
    @Expose
    private String cardUid;
    @SerializedName("terminalSerialNumber")
    @Expose
    private String terminalSerialNumber;
    @SerializedName("topUpAmount")
    @Expose
    private int topUpAmount;
    public final static Parcelable.Creator<TopUpReq> CREATOR = new Creator<TopUpReq>() {


        @SuppressWarnings({
            "unchecked"
        })
        public TopUpReq createFromParcel(Parcel in) {
            TopUpReq instance = new TopUpReq();
            instance.cardUid = ((String) in.readValue((String.class.getClassLoader())));
            instance.terminalSerialNumber = ((String) in.readValue((String.class.getClassLoader())));
            instance.topUpAmount = ((int) in.readValue((int.class.getClassLoader())));
            return instance;
        }

        public TopUpReq[] newArray(int size) {
            return (new TopUpReq[size]);
        }

    }
    ;

    /**
     * 
     * @return
     *     The cardUid
     */
    public String getCardUid() {
        return cardUid;
    }

    /**
     * 
     * @param cardUid
     *     The cardUid
     */
    public void setCardUid(String cardUid) {
        this.cardUid = cardUid;
    }

    /**
     * 
     * @return
     *     The terminalSerialNumber
     */
    public String getTerminalSerialNumber() {
        return terminalSerialNumber;
    }

    /**
     * 
     * @param terminalSerialNumber
     *     The terminalSerialNumber
     */
    public void setTerminalSerialNumber(String terminalSerialNumber) {
        this.terminalSerialNumber = terminalSerialNumber;
    }

    /**
     * 
     * @return
     *     The topUpAmount
     */
    public int getTopUpAmount() {
        return topUpAmount;
    }

    /**
     * 
     * @param topUpAmount
     *     The topUpAmount
     */
    public void setTopUpAmount(int topUpAmount) {
        this.topUpAmount = topUpAmount;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(cardUid);
        dest.writeValue(terminalSerialNumber);
        dest.writeValue(topUpAmount);
    }

    public int describeContents() {
        return  0;
    }

}
