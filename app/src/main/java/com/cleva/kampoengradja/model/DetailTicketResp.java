
package com.cleva.kampoengradja.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DetailTicketResp implements Parcelable
{

    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("imagePath")
    @Expose
    private String imagePath;
    @SerializedName("ageType")
    @Expose
    private String ageType;
    @SerializedName("price")
    @Expose
    private int price;
    @SerializedName("validPeriod")
    @Expose
    private String validPeriod;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("balanceAddition")
    @Expose
    private int balanceAddition;
    @SerializedName("bonusAddition")
    @Expose
    private int bonusAddition;
    public final static Parcelable.Creator<DetailTicketResp> CREATOR = new Creator<DetailTicketResp>() {


        @SuppressWarnings({
            "unchecked"
        })
        public DetailTicketResp createFromParcel(Parcel in) {
            DetailTicketResp instance = new DetailTicketResp();
            instance.title = ((String) in.readValue((String.class.getClassLoader())));
            instance.imagePath = ((String) in.readValue((String.class.getClassLoader())));
            instance.ageType = ((String) in.readValue((String.class.getClassLoader())));
            instance.price = ((int) in.readValue((int.class.getClassLoader())));
            instance.validPeriod = ((String) in.readValue((String.class.getClassLoader())));
            instance.description = ((String) in.readValue((String.class.getClassLoader())));
            instance.balanceAddition = ((int) in.readValue((int.class.getClassLoader())));
            instance.bonusAddition = ((int) in.readValue((int.class.getClassLoader())));
            return instance;
        }

        public DetailTicketResp[] newArray(int size) {
            return (new DetailTicketResp[size]);
        }

    }
    ;

    /**
     * 
     * @return
     *     The title
     */
    public String getTitle() {
        return title;
    }

    /**
     * 
     * @param title
     *     The title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * 
     * @return
     *     The imagePath
     */
    public String getImagePath() {
        return imagePath;
    }

    /**
     * 
     * @param imagePath
     *     The imagePath
     */
    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    /**
     * 
     * @return
     *     The ageType
     */
    public String getAgeType() {
        return ageType;
    }

    /**
     * 
     * @param ageType
     *     The ageType
     */
    public void setAgeType(String ageType) {
        this.ageType = ageType;
    }

    /**
     * 
     * @return
     *     The price
     */
    public int getPrice() {
        return price;
    }

    /**
     * 
     * @param price
     *     The price
     */
    public void setPrice(int price) {
        this.price = price;
    }

    /**
     * 
     * @return
     *     The validPeriod
     */
    public String getValidPeriod() {
        return validPeriod;
    }

    /**
     * 
     * @param validPeriod
     *     The validPeriod
     */
    public void setValidPeriod(String validPeriod) {
        this.validPeriod = validPeriod;
    }

    /**
     * 
     * @return
     *     The description
     */
    public String getDescription() {
        return description;
    }

    /**
     * 
     * @param description
     *     The description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * 
     * @return
     *     The balanceAddition
     */
    public int getBalanceAddition() {
        return balanceAddition;
    }

    /**
     * 
     * @param balanceAddition
     *     The balanceAddition
     */
    public void setBalanceAddition(int balanceAddition) {
        this.balanceAddition = balanceAddition;
    }

    /**
     * 
     * @return
     *     The bonusAddition
     */
    public int getBonusAddition() {
        return bonusAddition;
    }

    /**
     * 
     * @param bonusAddition
     *     The bonusAddition
     */
    public void setBonusAddition(int bonusAddition) {
        this.bonusAddition = bonusAddition;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(title);
        dest.writeValue(imagePath);
        dest.writeValue(ageType);
        dest.writeValue(price);
        dest.writeValue(validPeriod);
        dest.writeValue(description);
        dest.writeValue(balanceAddition);
        dest.writeValue(bonusAddition);
    }

    public int describeContents() {
        return  0;
    }

}
