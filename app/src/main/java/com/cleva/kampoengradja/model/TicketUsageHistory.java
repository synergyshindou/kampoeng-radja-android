
package com.cleva.kampoengradja.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TicketUsageHistory implements Parcelable
{

    @SerializedName("attractionName")
    @Expose
    private String attractionName;
    @SerializedName("balanceUsed")
    @Expose
    private int balanceUsed;
    @SerializedName("bonusUsed")
    @Expose
    private int bonusUsed;
    @SerializedName("timestamp")
    @Expose
    private String timestamp;
    public final static Parcelable.Creator<TicketUsageHistory> CREATOR = new Creator<TicketUsageHistory>() {


        @SuppressWarnings({
            "unchecked"
        })
        public TicketUsageHistory createFromParcel(Parcel in) {
            TicketUsageHistory instance = new TicketUsageHistory();
            instance.attractionName = ((String) in.readValue((String.class.getClassLoader())));
            instance.balanceUsed = ((int) in.readValue((int.class.getClassLoader())));
            instance.bonusUsed = ((int) in.readValue((int.class.getClassLoader())));
            instance.timestamp = ((String) in.readValue((String.class.getClassLoader())));
            return instance;
        }

        public TicketUsageHistory[] newArray(int size) {
            return (new TicketUsageHistory[size]);
        }

    }
    ;

    /**
     * 
     * @return
     *     The attractionName
     */
    public String getAttractionName() {
        return attractionName;
    }

    /**
     * 
     * @param attractionName
     *     The attractionName
     */
    public void setAttractionName(String attractionName) {
        this.attractionName = attractionName;
    }

    /**
     * 
     * @return
     *     The balanceUsed
     */
    public int getBalanceUsed() {
        return balanceUsed;
    }

    /**
     * 
     * @param balanceUsed
     *     The balanceUsed
     */
    public void setBalanceUsed(int balanceUsed) {
        this.balanceUsed = balanceUsed;
    }

    /**
     * 
     * @return
     *     The bonusUsed
     */
    public int getBonusUsed() {
        return bonusUsed;
    }

    /**
     * 
     * @param bonusUsed
     *     The bonusUsed
     */
    public void setBonusUsed(int bonusUsed) {
        this.bonusUsed = bonusUsed;
    }

    /**
     * 
     * @return
     *     The timestamp
     */
    public String getTimestamp() {
        return timestamp;
    }

    /**
     * 
     * @param timestamp
     *     The timestamp
     */
    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(attractionName);
        dest.writeValue(balanceUsed);
        dest.writeValue(bonusUsed);
        dest.writeValue(timestamp);
    }

    public int describeContents() {
        return  0;
    }

}
