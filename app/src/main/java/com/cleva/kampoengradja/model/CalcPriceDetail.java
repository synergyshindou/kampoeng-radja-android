package com.cleva.kampoengradja.model;

import java.util.List;

public class CalcPriceDetail {

    String cardUid;

    List<TiketParkirResp> chartDetails;

    String terminalSerialNumber;

    public CalcPriceDetail(String cardUID, List<TiketParkirResp> chartDetails, String terminalSerialNumber) {
        this.cardUid = cardUID;
        this.chartDetails = chartDetails;
        this.terminalSerialNumber = terminalSerialNumber;
    }

    public String getCardUID() {
        return cardUid;
    }

    public void setCardUID(String cardUID) {
        this.cardUid = cardUID;
    }

    public List<TiketParkirResp> getChartDetails() {
        return chartDetails;
    }

    public void setChartDetails(List<TiketParkirResp> chartDetails) {
        this.chartDetails = chartDetails;
    }

    public String getTerminalSerialNumber() {
        return terminalSerialNumber;
    }

    public void setTerminalSerialNumber(String terminalSerialNumber) {
        this.terminalSerialNumber = terminalSerialNumber;
    }
}
