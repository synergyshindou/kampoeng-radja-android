
package com.cleva.kampoengradja.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FreeToPlay implements Parcelable
{

    @SerializedName("attractionName")
    @Expose
    private String attractionName;
    @SerializedName("validUntil")
    @Expose
    private String validUntil;
    public final static Parcelable.Creator<FreeToPlay> CREATOR = new Creator<FreeToPlay>() {


        @SuppressWarnings({
            "unchecked"
        })
        public FreeToPlay createFromParcel(Parcel in) {
            FreeToPlay instance = new FreeToPlay();
            instance.attractionName = ((String) in.readValue((String.class.getClassLoader())));
            instance.validUntil = ((String) in.readValue((String.class.getClassLoader())));
            return instance;
        }

        public FreeToPlay[] newArray(int size) {
            return (new FreeToPlay[size]);
        }

    }
    ;

    /**
     * 
     * @return
     *     The attractionName
     */
    public String getAttractionName() {
        return attractionName;
    }

    /**
     * 
     * @param attractionName
     *     The attractionName
     */
    public void setAttractionName(String attractionName) {
        this.attractionName = attractionName;
    }

    /**
     * 
     * @return
     *     The validUntil
     */
    public String getValidUntil() {
        return validUntil;
    }

    /**
     * 
     * @param validUntil
     *     The validUntil
     */
    public void setValidUntil(String validUntil) {
        this.validUntil = validUntil;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(attractionName);
        dest.writeValue(validUntil);
    }

    public int describeContents() {
        return  0;
    }

}
