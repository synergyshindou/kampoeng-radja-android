
package com.cleva.kampoengradja.model;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity(tableName = "outlet_item_table")
public class OutletItem implements Parcelable {
    @PrimaryKey()
    @SerializedName("itemId")
    @Expose
    private int itemId;
    @SerializedName("imagePath")
    @Expose
    private String imagePath;
    @SerializedName("price")
    @Expose
    private int price;
    @SerializedName("title")
    @Expose
    private String title;

    private int quantity;
    public final static Creator<OutletItem> CREATOR = new Creator<OutletItem>() {


        @SuppressWarnings({
                "unchecked"
        })
        public OutletItem createFromParcel(Parcel in) {
            OutletItem instance = new OutletItem();
            instance.itemId = ((int) in.readValue((int.class.getClassLoader())));
            instance.imagePath = ((String) in.readValue((String.class.getClassLoader())));
            instance.price = ((int) in.readValue((int.class.getClassLoader())));
            instance.title = ((String) in.readValue((String.class.getClassLoader())));
            instance.quantity = ((int) in.readValue((int.class.getClassLoader())));
            return instance;
        }

        public OutletItem[] newArray(int size) {
            return (new OutletItem[size]);
        }

    };

    /**
     * @return The itemId
     */
    public int getItemId() {
        return itemId;
    }

    /**
     * @param itemId The itemId
     */
    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    /**
     * @return The imagePath
     */
    public String getImagePath() {
        return imagePath;
    }

    /**
     * @param imagePath The imagePath
     */
    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    /**
     * @return The price
     */
    public int getPrice() {
        return price;
    }

    /**
     * @param price The price
     */
    public void setPrice(int price) {
        this.price = price;
    }

    /**
     * @return The title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title The title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(itemId);
        dest.writeValue(imagePath);
        dest.writeValue(price);
        dest.writeValue(title);
        dest.writeValue(quantity);
    }

    public int describeContents() {
        return 0;
    }

}
