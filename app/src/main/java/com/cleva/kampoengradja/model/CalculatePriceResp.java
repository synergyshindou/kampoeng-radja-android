
package com.cleva.kampoengradja.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CalculatePriceResp implements Parcelable
{

    @SerializedName("discountedAmount")
    @Expose
    private int discountedAmount;
    @SerializedName("priceBeforeDiscount")
    @Expose
    private int priceBeforeDiscount;
    @SerializedName("totalPrice")
    @Expose
    private int totalPrice;
    public final static Parcelable.Creator<CalculatePriceResp> CREATOR = new Creator<CalculatePriceResp>() {


        @SuppressWarnings({
            "unchecked"
        })
        public CalculatePriceResp createFromParcel(Parcel in) {
            CalculatePriceResp instance = new CalculatePriceResp();
            instance.discountedAmount = ((int) in.readValue((int.class.getClassLoader())));
            instance.priceBeforeDiscount = ((int) in.readValue((int.class.getClassLoader())));
            instance.totalPrice = ((int) in.readValue((int.class.getClassLoader())));
            return instance;
        }

        public CalculatePriceResp[] newArray(int size) {
            return (new CalculatePriceResp[size]);
        }

    }
    ;

    /**
     * 
     * @return
     *     The discountedAmount
     */
    public int getDiscountedAmount() {
        return discountedAmount;
    }

    /**
     * 
     * @param discountedAmount
     *     The discountedAmount
     */
    public void setDiscountedAmount(int discountedAmount) {
        this.discountedAmount = discountedAmount;
    }

    /**
     * 
     * @return
     *     The priceBeforeDiscount
     */
    public int getPriceBeforeDiscount() {
        return priceBeforeDiscount;
    }

    /**
     * 
     * @param priceBeforeDiscount
     *     The priceBeforeDiscount
     */
    public void setPriceBeforeDiscount(int priceBeforeDiscount) {
        this.priceBeforeDiscount = priceBeforeDiscount;
    }

    /**
     * 
     * @return
     *     The totalPrice
     */
    public int getTotalPrice() {
        return totalPrice;
    }

    /**
     * 
     * @param totalPrice
     *     The totalPrice
     */
    public void setTotalPrice(int totalPrice) {
        this.totalPrice = totalPrice;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(discountedAmount);
        dest.writeValue(priceBeforeDiscount);
        dest.writeValue(totalPrice);
    }

    public int describeContents() {
        return  0;
    }

}
