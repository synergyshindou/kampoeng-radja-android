
package com.cleva.kampoengradja.model;

import java.util.ArrayList;
import java.util.List;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CardInfoDataJava implements Parcelable
{

    @SerializedName("membership")
    @Expose
    private String membership;
    @SerializedName("memberName")
    @Expose
    private String memberName;
    @SerializedName("cardIdentifierId")
    @Expose
    private String cardIdentifierId;
    @SerializedName("balance")
    @Expose
    private int balance;
    @SerializedName("bonus")
    @Expose
    private int bonus;
    @SerializedName("deposit")
    @Expose
    private int deposit;
    @SerializedName("freeToPlay")
    @Expose
    private List<FreeToPlay> freeToPlay = new ArrayList<FreeToPlay>();
    @SerializedName("topUpHistory")
    @Expose
    private List<TopUpHistory> topUpHistory = new ArrayList<TopUpHistory>();
    @SerializedName("ticketUsageHistory")
    @Expose
    private List<TicketUsageHistory> ticketUsageHistory = new ArrayList<TicketUsageHistory>();
    public final static Parcelable.Creator<CardInfoDataJava> CREATOR = new Creator<CardInfoDataJava>() {


        @SuppressWarnings({
            "unchecked"
        })
        public CardInfoDataJava createFromParcel(Parcel in) {
            CardInfoDataJava instance = new CardInfoDataJava();
            instance.membership = ((String) in.readValue((String.class.getClassLoader())));
            instance.memberName = ((String) in.readValue((String.class.getClassLoader())));
            instance.cardIdentifierId = ((String) in.readValue((String.class.getClassLoader())));
            instance.balance = ((int) in.readValue((int.class.getClassLoader())));
            instance.bonus = ((int) in.readValue((int.class.getClassLoader())));
            instance.deposit = ((int) in.readValue((int.class.getClassLoader())));
            in.readList(instance.freeToPlay, (com.cleva.kampoengradja.model.FreeToPlay.class.getClassLoader()));
            in.readList(instance.topUpHistory, (com.cleva.kampoengradja.model.TopUpHistory.class.getClassLoader()));
            in.readList(instance.ticketUsageHistory, (com.cleva.kampoengradja.model.TicketUsageHistory.class.getClassLoader()));
            return instance;
        }

        public CardInfoDataJava[] newArray(int size) {
            return (new CardInfoDataJava[size]);
        }

    }
    ;

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public String getCardIdentifierId() {
        return cardIdentifierId;
    }

    public void setCardIdentifierId(String cardIdentifierId) {
        this.cardIdentifierId = cardIdentifierId;
    }

    /**
     * 
     * @return
     *     The membership
     */
    public String getMembership() {
        return membership;
    }

    /**
     * 
     * @param membership
     *     The membership
     */
    public void setMembership(String membership) {
        this.membership = membership;
    }

    /**
     * 
     * @return
     *     The balance
     */
    public int getBalance() {
        return balance;
    }

    /**
     * 
     * @param balance
     *     The balance
     */
    public void setBalance(int balance) {
        this.balance = balance;
    }

    /**
     * 
     * @return
     *     The bonus
     */
    public int getBonus() {
        return bonus;
    }

    /**
     * 
     * @param bonus
     *     The bonus
     */
    public void setBonus(int bonus) {
        this.bonus = bonus;
    }

    /**
     * 
     * @return
     *     The deposit
     */
    public int getDeposit() {
        return deposit;
    }

    /**
     * 
     * @param deposit
     *     The deposit
     */
    public void setDeposit(int deposit) {
        this.deposit = deposit;
    }

    /**
     * 
     * @return
     *     The freeToPlay
     */
    public List<FreeToPlay> getFreeToPlay() {
        return freeToPlay;
    }

    /**
     * 
     * @param freeToPlay
     *     The freeToPlay
     */
    public void setFreeToPlay(List<FreeToPlay> freeToPlay) {
        this.freeToPlay = freeToPlay;
    }

    /**
     * 
     * @return
     *     The topUpHistory
     */
    public List<TopUpHistory> getTopUpHistory() {
        return topUpHistory;
    }

    /**
     * 
     * @param topUpHistory
     *     The topUpHistory
     */
    public void setTopUpHistory(List<TopUpHistory> topUpHistory) {
        this.topUpHistory = topUpHistory;
    }

    /**
     * 
     * @return
     *     The packagePurchaseHistory
     */
    public List<TicketUsageHistory> getTicketUsageHistory() {
        return ticketUsageHistory;
    }

    /**
     * 
     * @param ticketUsageHistory
     *     The packagePurchaseHistory
     */
    public void setTicketUsageHistory(List<TicketUsageHistory> ticketUsageHistory) {
        this.ticketUsageHistory = ticketUsageHistory;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(membership);
        dest.writeValue(memberName);
        dest.writeValue(cardIdentifierId);
        dest.writeValue(balance);
        dest.writeValue(bonus);
        dest.writeValue(deposit);
        dest.writeList(freeToPlay);
        dest.writeList(topUpHistory);
        dest.writeList(ticketUsageHistory);
    }

    public int describeContents() {
        return  0;
    }

}
