
package com.cleva.kampoengradja.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OutletPurchase implements Parcelable
{

    @SerializedName("remainingBalance")
    @Expose
    private int remainingBalance;
    @SerializedName("totalPrice")
    @Expose
    private int totalPrice;
    @SerializedName("transactionId")
    @Expose
    private String transactionId;
    public final static Parcelable.Creator<OutletPurchase> CREATOR = new Creator<OutletPurchase>() {


        @SuppressWarnings({
            "unchecked"
        })
        public OutletPurchase createFromParcel(Parcel in) {
            OutletPurchase instance = new OutletPurchase();
            instance.remainingBalance = ((int) in.readValue((int.class.getClassLoader())));
            instance.totalPrice = ((int) in.readValue((int.class.getClassLoader())));
            instance.transactionId = ((String) in.readValue((String.class.getClassLoader())));
            return instance;
        }

        public OutletPurchase[] newArray(int size) {
            return (new OutletPurchase[size]);
        }

    }
    ;

    /**
     * 
     * @return
     *     The remainingBalance
     */
    public int getRemainingBalance() {
        return remainingBalance;
    }

    /**
     * 
     * @param remainingBalance
     *     The remainingBalance
     */
    public void setRemainingBalance(int remainingBalance) {
        this.remainingBalance = remainingBalance;
    }

    /**
     * 
     * @return
     *     The totalPrice
     */
    public int getTotalPrice() {
        return totalPrice;
    }

    /**
     * 
     * @param totalPrice
     *     The totalPrice
     */
    public void setTotalPrice(int totalPrice) {
        this.totalPrice = totalPrice;
    }

    /**
     * 
     * @return
     *     The transactionId
     */
    public String getTransactionId() {
        return transactionId;
    }

    /**
     * 
     * @param transactionId
     *     The transactionId
     */
    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(remainingBalance);
        dest.writeValue(totalPrice);
        dest.writeValue(transactionId);
    }

    public int describeContents() {
        return  0;
    }

}
