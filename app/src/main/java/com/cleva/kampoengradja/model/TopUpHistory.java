
package com.cleva.kampoengradja.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TopUpHistory implements Parcelable
{

    @SerializedName("topUpAmount")
    @Expose
    private int topUpAmount;
    @SerializedName("topUpAddition")
    @Expose
    private int topUpAddition;
    @SerializedName("bonus")
    @Expose
    private int bonus;
    @SerializedName("timestamp")
    @Expose
    private String timestamp;
    public final static Parcelable.Creator<TopUpHistory> CREATOR = new Creator<TopUpHistory>() {


        @SuppressWarnings({
            "unchecked"
        })
        public TopUpHistory createFromParcel(Parcel in) {
            TopUpHistory instance = new TopUpHistory();
            instance.topUpAmount = ((int) in.readValue((int.class.getClassLoader())));
            instance.topUpAddition = ((int) in.readValue((int.class.getClassLoader())));
            instance.bonus = ((int) in.readValue((int.class.getClassLoader())));
            instance.timestamp = ((String) in.readValue((String.class.getClassLoader())));
            return instance;
        }

        public TopUpHistory[] newArray(int size) {
            return (new TopUpHistory[size]);
        }

    }
    ;

    /**
     * 
     * @return
     *     The topUpAmount
     */
    public int getTopUpAmount() {
        return topUpAmount;
    }

    /**
     * 
     * @param topUpAmount
     *     The topUpAmount
     */
    public void setTopUpAmount(int topUpAmount) {
        this.topUpAmount = topUpAmount;
    }

    /**
     * 
     * @return
     *     The topUpAddition
     */
    public int getTopUpAddition() {
        return topUpAddition;
    }

    /**
     * 
     * @param topUpAddition
     *     The topUpAddition
     */
    public void setTopUpAddition(int topUpAddition) {
        this.topUpAddition = topUpAddition;
    }

    /**
     * 
     * @return
     *     The bonus
     */
    public int getBonus() {
        return bonus;
    }

    /**
     * 
     * @param bonus
     *     The bonus
     */
    public void setBonus(int bonus) {
        this.bonus = bonus;
    }

    /**
     * 
     * @return
     *     The timestamp
     */
    public String getTimestamp() {
        return timestamp;
    }

    /**
     * 
     * @param timestamp
     *     The timestamp
     */
    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(topUpAmount);
        dest.writeValue(topUpAddition);
        dest.writeValue(bonus);
        dest.writeValue(timestamp);
    }

    public int describeContents() {
        return  0;
    }

}
