
package com.cleva.kampoengradja.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TerminalAllocation implements Parcelable
{

    @SerializedName("allocationType")
    @Expose
    private String allocationType;
    @SerializedName("attractionName")
    @Expose
    private String attractionName;
    @SerializedName("outletCode")
    @Expose
    private String outletCode;
    @SerializedName("outletName")
    @Expose
    private String outletName;
    public final static Parcelable.Creator<TerminalAllocation> CREATOR = new Creator<TerminalAllocation>() {


        @SuppressWarnings({
            "unchecked"
        })
        public TerminalAllocation createFromParcel(Parcel in) {
            TerminalAllocation instance = new TerminalAllocation();
            instance.allocationType = ((String) in.readValue((String.class.getClassLoader())));
            instance.attractionName = ((String) in.readValue((String.class.getClassLoader())));
            instance.outletCode = ((String) in.readValue((String.class.getClassLoader())));
            instance.outletName = ((String) in.readValue((String.class.getClassLoader())));
            return instance;
        }

        public TerminalAllocation[] newArray(int size) {
            return (new TerminalAllocation[size]);
        }

    }
    ;

    /**
     * 
     * @return
     *     The allocationType
     */
    public String getAllocationType() {
        return allocationType;
    }

    /**
     * 
     * @param allocationType
     *     The allocationType
     */
    public void setAllocationType(String allocationType) {
        this.allocationType = allocationType;
    }

    /**
     * 
     * @return
     *     The attractionName
     */
    public String getAttractionName() {
        return attractionName;
    }

    /**
     * 
     * @param attractionName
     *     The attractionName
     */
    public void setAttractionName(String attractionName) {
        this.attractionName = attractionName;
    }

    /**
     * 
     * @return
     *     The outletCode
     */
    public String getOutletCode() {
        return outletCode;
    }

    /**
     * 
     * @param outletCode
     *     The outletCode
     */
    public void setOutletCode(String outletCode) {
        this.outletCode = outletCode;
    }

    /**
     * 
     * @return
     *     The outletName
     */
    public String getOutletName() {
        return outletName;
    }

    /**
     * 
     * @param outletName
     *     The outletName
     */
    public void setOutletName(String outletName) {
        this.outletName = outletName;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(allocationType);
        dest.writeValue(attractionName);
        dest.writeValue(outletCode);
        dest.writeValue(outletName);
    }

    public int describeContents() {
        return  0;
    }

}
