
package com.cleva.kampoengradja.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ItemCategory implements Parcelable
{

    @SerializedName("label")
    @Expose
    private String label;
    @SerializedName("value")
    @Expose
    private String value;
    public final static Parcelable.Creator<ItemCategory> CREATOR = new Creator<ItemCategory>() {


        @SuppressWarnings({
            "unchecked"
        })
        public ItemCategory createFromParcel(Parcel in) {
            ItemCategory instance = new ItemCategory();
            instance.label = ((String) in.readValue((String.class.getClassLoader())));
            instance.value = ((String) in.readValue((String.class.getClassLoader())));
            return instance;
        }

        public ItemCategory[] newArray(int size) {
            return (new ItemCategory[size]);
        }

    }
    ;

    /**
     * 
     * @return
     *     The label
     */
    public String getLabel() {
        return label;
    }

    /**
     * 
     * @param label
     *     The label
     */
    public void setLabel(String label) {
        this.label = label;
    }

    /**
     * 
     * @return
     *     The value
     */
    public String getValue() {
        return value;
    }

    /**
     * 
     * @param value
     *     The value
     */
    public void setValue(String value) {
        this.value = value;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(label);
        dest.writeValue(value);
    }

    public int describeContents() {
        return  0;
    }

}
