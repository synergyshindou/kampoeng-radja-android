
package com.cleva.kampoengradja.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RefundDetailReq implements Parcelable
{

    @SerializedName("cardUid")
    @Expose
    private String cardUid;
    @SerializedName("terminalSerialNumber")
    @Expose
    private String terminalSerialNumber;
    public final static Parcelable.Creator<RefundDetailReq> CREATOR = new Creator<RefundDetailReq>() {


        @SuppressWarnings({
            "unchecked"
        })
        public RefundDetailReq createFromParcel(Parcel in) {
            RefundDetailReq instance = new RefundDetailReq();
            instance.cardUid = ((String) in.readValue((String.class.getClassLoader())));
            instance.terminalSerialNumber = ((String) in.readValue((String.class.getClassLoader())));
            return instance;
        }

        public RefundDetailReq[] newArray(int size) {
            return (new RefundDetailReq[size]);
        }

    }
    ;

    /**
     * 
     * @return
     *     The cardUid
     */
    public String getCardUid() {
        return cardUid;
    }

    /**
     * 
     * @param cardUid
     *     The cardUid
     */
    public void setCardUid(String cardUid) {
        this.cardUid = cardUid;
    }

    /**
     * 
     * @return
     *     The terminalSerialNumber
     */
    public String getTerminalSerialNumber() {
        return terminalSerialNumber;
    }

    /**
     * 
     * @param terminalSerialNumber
     *     The terminalSerialNumber
     */
    public void setTerminalSerialNumber(String terminalSerialNumber) {
        this.terminalSerialNumber = terminalSerialNumber;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(cardUid);
        dest.writeValue(terminalSerialNumber);
    }

    public int describeContents() {
        return  0;
    }

}
