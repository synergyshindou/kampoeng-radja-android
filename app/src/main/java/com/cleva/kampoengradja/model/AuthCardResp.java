
package com.cleva.kampoengradja.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AuthCardResp implements Parcelable
{

    @SerializedName("authKey")
    @Expose
    private String authKey;
    @SerializedName("authData")
    @Expose
    private String authData;
    public final static Parcelable.Creator<AuthCardResp> CREATOR = new Creator<AuthCardResp>() {


        @SuppressWarnings({
            "unchecked"
        })
        public AuthCardResp createFromParcel(Parcel in) {
            AuthCardResp instance = new AuthCardResp();
            instance.authKey = ((String) in.readValue((String.class.getClassLoader())));
            instance.authData = ((String) in.readValue((String.class.getClassLoader())));
            return instance;
        }

        public AuthCardResp[] newArray(int size) {
            return (new AuthCardResp[size]);
        }

    }
    ;

    /**
     * 
     * @return
     *     The authKey
     */
    public String getAuthKey() {
        return authKey;
    }

    /**
     * 
     * @param authKey
     *     The authKey
     */
    public void setAuthKey(String authKey) {
        this.authKey = authKey;
    }

    /**
     * 
     * @return
     *     The authData
     */
    public String getAuthData() {
        return authData;
    }

    /**
     * 
     * @param authData
     *     The authData
     */
    public void setAuthData(String authData) {
        this.authData = authData;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(authKey);
        dest.writeValue(authData);
    }

    public int describeContents() {
        return  0;
    }

}
