
package com.cleva.kampoengradja.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TicketPurchaseResp implements Parcelable
{

    @SerializedName("transactionId")
    @Expose
    private String transactionId;
    public final static Parcelable.Creator<TicketPurchaseResp> CREATOR = new Creator<TicketPurchaseResp>() {


        @SuppressWarnings({
            "unchecked"
        })
        public TicketPurchaseResp createFromParcel(Parcel in) {
            TicketPurchaseResp instance = new TicketPurchaseResp();
            instance.transactionId = ((String) in.readValue((String.class.getClassLoader())));
            return instance;
        }

        public TicketPurchaseResp[] newArray(int size) {
            return (new TicketPurchaseResp[size]);
        }

    }
    ;

    /**
     * 
     * @return
     *     The transactionId
     */
    public String getTransactionId() {
        return transactionId;
    }

    /**
     * 
     * @param transactionId
     *     The transactionId
     */
    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(transactionId);
    }

    public int describeContents() {
        return  0;
    }

}
