
package com.cleva.kampoengradja.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CardActivationReq implements Parcelable
{

    @SerializedName("cardUid")
    @Expose
    private String cardUid;
    @SerializedName("encryptedValue")
    @Expose
    private String encryptedValue;
    @SerializedName("terminalSerialNumber")
    @Expose
    private String terminalSerialNumber;
    public final static Parcelable.Creator<CardActivationReq> CREATOR = new Creator<CardActivationReq>() {


        @SuppressWarnings({
            "unchecked"
        })
        public CardActivationReq createFromParcel(Parcel in) {
            CardActivationReq instance = new CardActivationReq();
            instance.cardUid = ((String) in.readValue((String.class.getClassLoader())));
            instance.encryptedValue = ((String) in.readValue((String.class.getClassLoader())));
            instance.terminalSerialNumber = ((String) in.readValue((String.class.getClassLoader())));
            return instance;
        }

        public CardActivationReq[] newArray(int size) {
            return (new CardActivationReq[size]);
        }

    }
    ;

    /**
     * 
     * @return
     *     The cardUid
     */
    public String getCardUid() {
        return cardUid;
    }

    /**
     * 
     * @param cardUid
     *     The cardUid
     */
    public void setCardUid(String cardUid) {
        this.cardUid = cardUid;
    }

    /**
     * 
     * @return
     *     The encryptedValue
     */
    public String getEncryptedValue() {
        return encryptedValue;
    }

    /**
     * 
     * @param encryptedValue
     *     The encryptedValue
     */
    public void setEncryptedValue(String encryptedValue) {
        this.encryptedValue = encryptedValue;
    }

    /**
     * 
     * @return
     *     The terminalSerialNumber
     */
    public String getTerminalSerialNumber() {
        return terminalSerialNumber;
    }

    /**
     * 
     * @param terminalSerialNumber
     *     The terminalSerialNumber
     */
    public void setTerminalSerialNumber(String terminalSerialNumber) {
        this.terminalSerialNumber = terminalSerialNumber;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(cardUid);
        dest.writeValue(encryptedValue);
        dest.writeValue(terminalSerialNumber);
    }

    public int describeContents() {
        return  0;
    }

}
