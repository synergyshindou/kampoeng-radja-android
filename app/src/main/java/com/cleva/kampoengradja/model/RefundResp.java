package com.cleva.kampoengradja.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RefundResp implements Parcelable
{

    @SerializedName("deposit")
    @Expose
    private int deposit;
    @SerializedName("refundAmount")
    @Expose
    private int refundAmount;
    @SerializedName("remainingBalance")
    @Expose
    private int remainingBalance;
    @SerializedName("transactionId")
    @Expose
    private String transactionId;
    public final static Parcelable.Creator<RefundResp> CREATOR = new Creator<RefundResp>() {


        @SuppressWarnings({
            "unchecked"
        })
        public RefundResp createFromParcel(Parcel in) {
            RefundResp instance = new RefundResp();
            instance.deposit = ((int) in.readValue((int.class.getClassLoader())));
            instance.refundAmount = ((int) in.readValue((int.class.getClassLoader())));
            instance.remainingBalance = ((int) in.readValue((int.class.getClassLoader())));
            instance.transactionId = ((String) in.readValue((String.class.getClassLoader())));
            return instance;
        }

        public RefundResp[] newArray(int size) {
            return (new RefundResp[size]);
        }

    }
    ;

    /**
     * 
     * @return
     *     The deposit
     */
    public int getDeposit() {
        return deposit;
    }

    /**
     * 
     * @param deposit
     *     The deposit
     */
    public void setDeposit(int deposit) {
        this.deposit = deposit;
    }

    /**
     * 
     * @return
     *     The refundAmount
     */
    public int getRefundAmount() {
        return refundAmount;
    }

    /**
     * 
     * @param refundAmount
     *     The refundAmount
     */
    public void setRefundAmount(int refundAmount) {
        this.refundAmount = refundAmount;
    }

    /**
     * 
     * @return
     *     The remainingBalance
     */
    public int getRemainingBalance() {
        return remainingBalance;
    }

    /**
     * 
     * @param remainingBalance
     *     The remainingBalance
     */
    public void setRemainingBalance(int remainingBalance) {
        this.remainingBalance = remainingBalance;
    }

    /**
     * 
     * @return
     *     The transactionId
     */
    public String getTransactionId() {
        return transactionId;
    }

    /**
     * 
     * @param transactionId
     *     The transactionId
     */
    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(deposit);
        dest.writeValue(refundAmount);
        dest.writeValue(remainingBalance);
        dest.writeValue(transactionId);
    }

    public int describeContents() {
        return  0;
    }

}
