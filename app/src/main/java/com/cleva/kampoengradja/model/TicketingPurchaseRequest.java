package com.cleva.kampoengradja.model;

import java.util.List;

public class TicketingPurchaseRequest {
    String cardUid;

    List<TiketParkirResp> chartDetails;

    String terminalSerialNumber;

    String paymentMethodType;

    int paidAmount;

    CalculatePriceResp ticketingPriceDetail;

    public TicketingPurchaseRequest(String cardUid, List<TiketParkirResp> chartDetails,
                                    String terminalSerialNumber, int paidAmount, String paymentMethodType,
                                    CalculatePriceResp ticketingPriceDetail) {
        this.cardUid = cardUid;
        this.chartDetails = chartDetails;
        this.terminalSerialNumber = terminalSerialNumber;
        this.paidAmount = paidAmount;
        this.paymentMethodType = paymentMethodType;
        this.ticketingPriceDetail = ticketingPriceDetail;
    }

    public String getCardUid() {
        return cardUid;
    }

    public void setCardUid(String cardUid) {
        this.cardUid = cardUid;
    }

    public List<TiketParkirResp> getChartDetails() {
        return chartDetails;
    }

    public void setChartDetails(List<TiketParkirResp> chartDetails) {
        this.chartDetails = chartDetails;
    }

    public String getTerminalSerialNumber() {
        return terminalSerialNumber;
    }

    public void setTerminalSerialNumber(String terminalSerialNumber) {
        this.terminalSerialNumber = terminalSerialNumber;
    }

    public int getPaidAmount() {
        return paidAmount;
    }

    public void setPaidAmount(int paidAmount) {
        this.paidAmount = paidAmount;
    }

    public CalculatePriceResp getTicketingPriceDetail() {
        return ticketingPriceDetail;
    }

    public void setTicketingPriceDetail(CalculatePriceResp ticketingPriceDetail) {
        this.ticketingPriceDetail = ticketingPriceDetail;
    }

    public String getPaymentMethodType() {
        return paymentMethodType;
    }

    public void setPaymentMethodType(String paymentMethodType) {
        this.paymentMethodType = paymentMethodType;
    }
}
