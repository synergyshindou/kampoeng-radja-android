
package com.cleva.kampoengradja.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CardRepersoResp implements Parcelable
{

    @SerializedName("cardNumber")
    @Expose
    private String cardNumber;
    @SerializedName("authKey")
    @Expose
    private String authKey;
    @SerializedName("authData")
    @Expose
    private String authData;
    @SerializedName("secretData")
    @Expose
    private String secretData;
    public final static Parcelable.Creator<CardRepersoResp> CREATOR = new Creator<CardRepersoResp>() {


        @SuppressWarnings({
            "unchecked"
        })
        public CardRepersoResp createFromParcel(Parcel in) {
            CardRepersoResp instance = new CardRepersoResp();
            instance.cardNumber = ((String) in.readValue((String.class.getClassLoader())));
            instance.authKey = ((String) in.readValue((String.class.getClassLoader())));
            instance.authData = ((String) in.readValue((String.class.getClassLoader())));
            instance.secretData = ((String) in.readValue((String.class.getClassLoader())));
            return instance;
        }

        public CardRepersoResp[] newArray(int size) {
            return (new CardRepersoResp[size]);
        }

    }
    ;

    /**
     * 
     * @return
     *     The cardNumber
     */
    public String getCardNumber() {
        return cardNumber;
    }

    /**
     * 
     * @param cardNumber
     *     The cardNumber
     */
    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    /**
     * 
     * @return
     *     The authKey
     */
    public String getAuthKey() {
        return authKey;
    }

    /**
     * 
     * @param authKey
     *     The authKey
     */
    public void setAuthKey(String authKey) {
        this.authKey = authKey;
    }

    /**
     * 
     * @return
     *     The authData
     */
    public String getAuthData() {
        return authData;
    }

    /**
     * 
     * @param authData
     *     The authData
     */
    public void setAuthData(String authData) {
        this.authData = authData;
    }

    /**
     * 
     * @return
     *     The secretData
     */
    public String getSecretData() {
        return secretData;
    }

    /**
     * 
     * @param secretData
     *     The secretData
     */
    public void setSecretData(String secretData) {
        this.secretData = secretData;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(cardNumber);
        dest.writeValue(authKey);
        dest.writeValue(authData);
        dest.writeValue(secretData);
    }

    public int describeContents() {
        return  0;
    }

}
