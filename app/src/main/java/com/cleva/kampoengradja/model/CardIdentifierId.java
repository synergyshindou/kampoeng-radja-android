
package com.cleva.kampoengradja.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CardIdentifierId implements Parcelable
{

    @SerializedName("label")
    @Expose
    private String label;
    @SerializedName("value")
    @Expose
    private String value;
    public final static Parcelable.Creator<CardIdentifierId> CREATOR = new Creator<CardIdentifierId>() {


        @SuppressWarnings({
            "unchecked"
        })
        public CardIdentifierId createFromParcel(Parcel in) {
            CardIdentifierId instance = new CardIdentifierId();
            instance.label = ((String) in.readValue((String.class.getClassLoader())));
            instance.value = ((String) in.readValue((String.class.getClassLoader())));
            return instance;
        }

        public CardIdentifierId[] newArray(int size) {
            return (new CardIdentifierId[size]);
        }

    }
    ;

    /**
     * 
     * @return
     *     The label
     */
    public String getLabel() {
        return label;
    }

    /**
     * 
     * @param label
     *     The label
     */
    public void setLabel(String label) {
        this.label = label;
    }

    /**
     * 
     * @return
     *     The value
     */
    public String getValue() {
        return value;
    }

    /**
     * 
     * @param value
     *     The value
     */
    public void setValue(String value) {
        this.value = value;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(label);
        dest.writeValue(value);
    }

    public int describeContents() {
        return  0;
    }

}
