
package com.cleva.kampoengradja.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CardTerminal implements Parcelable
{

    @SerializedName("cardUid")
    @Expose
    private String cardUid;
    @SerializedName("terminalSerialNumber")
    @Expose
    private String terminalSerialNumber;
    public final static Creator<CardTerminal> CREATOR = new Creator<CardTerminal>() {


        @SuppressWarnings({
            "unchecked"
        })
        public CardTerminal createFromParcel(Parcel in) {
            CardTerminal instance = new CardTerminal();
            instance.cardUid = ((String) in.readValue((String.class.getClassLoader())));
            instance.terminalSerialNumber = ((String) in.readValue((String.class.getClassLoader())));
            return instance;
        }

        public CardTerminal[] newArray(int size) {
            return (new CardTerminal[size]);
        }

    }
    ;

    /**
     * 
     * @return
     *     The cardUid
     */
    public String getCardUid() {
        return cardUid;
    }

    /**
     * 
     * @param cardUid
     *     The cardUid
     */
    public void setCardUid(String cardUid) {
        this.cardUid = cardUid;
    }

    /**
     * 
     * @return
     *     The terminalSerialNumber
     */
    public String getTerminalSerialNumber() {
        return terminalSerialNumber;
    }

    /**
     * 
     * @param terminalSerialNumber
     *     The terminalSerialNumber
     */
    public void setTerminalSerialNumber(String terminalSerialNumber) {
        this.terminalSerialNumber = terminalSerialNumber;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(cardUid);
        dest.writeValue(terminalSerialNumber);
    }

    public int describeContents() {
        return  0;
    }

}
