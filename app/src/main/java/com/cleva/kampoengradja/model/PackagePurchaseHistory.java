
package com.cleva.kampoengradja.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PackagePurchaseHistory implements Parcelable
{

    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("priceBeforeDiscount")
    @Expose
    private int priceBeforeDiscount;
    @SerializedName("discountedAmount")
    @Expose
    private int discountedAmount;
    @SerializedName("timestamp")
    @Expose
    private String timestamp;
    public final static Parcelable.Creator<PackagePurchaseHistory> CREATOR = new Creator<PackagePurchaseHistory>() {


        @SuppressWarnings({
            "unchecked"
        })
        public PackagePurchaseHistory createFromParcel(Parcel in) {
            PackagePurchaseHistory instance = new PackagePurchaseHistory();
            instance.title = ((String) in.readValue((String.class.getClassLoader())));
            instance.priceBeforeDiscount = ((int) in.readValue((int.class.getClassLoader())));
            instance.discountedAmount = ((int) in.readValue((int.class.getClassLoader())));
            instance.timestamp = ((String) in.readValue((String.class.getClassLoader())));
            return instance;
        }

        public PackagePurchaseHistory[] newArray(int size) {
            return (new PackagePurchaseHistory[size]);
        }

    }
    ;

    /**
     * 
     * @return
     *     The title
     */
    public String getTitle() {
        return title;
    }

    /**
     * 
     * @param title
     *     The title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * 
     * @return
     *     The priceBeforeDiscount
     */
    public int getPriceBeforeDiscount() {
        return priceBeforeDiscount;
    }

    /**
     * 
     * @param priceBeforeDiscount
     *     The priceBeforeDiscount
     */
    public void setPriceBeforeDiscount(int priceBeforeDiscount) {
        this.priceBeforeDiscount = priceBeforeDiscount;
    }

    /**
     * 
     * @return
     *     The discountedAmount
     */
    public int getDiscountedAmount() {
        return discountedAmount;
    }

    /**
     * 
     * @param discountedAmount
     *     The discountedAmount
     */
    public void setDiscountedAmount(int discountedAmount) {
        this.discountedAmount = discountedAmount;
    }

    /**
     * 
     * @return
     *     The timestamp
     */
    public String getTimestamp() {
        return timestamp;
    }

    /**
     * 
     * @param timestamp
     *     The timestamp
     */
    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(title);
        dest.writeValue(priceBeforeDiscount);
        dest.writeValue(discountedAmount);
        dest.writeValue(timestamp);
    }

    public int describeContents() {
        return  0;
    }

}
