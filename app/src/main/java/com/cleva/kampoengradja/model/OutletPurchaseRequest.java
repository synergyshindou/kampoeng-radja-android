
package com.cleva.kampoengradja.model;

import java.util.ArrayList;
import java.util.List;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OutletPurchaseRequest implements Parcelable
{

    @SerializedName("cardUid")
    @Expose
    private String cardUid;
    @SerializedName("chartDetails")
    @Expose
    private List<OutletItem> chartDetails = new ArrayList<>();
    @SerializedName("terminalSerialNumber")
    @Expose
    private String terminalSerialNumber;
    @SerializedName("totalPrice")
    @Expose
    private int totalPrice;
    public final static Parcelable.Creator<OutletPurchaseRequest> CREATOR = new Creator<OutletPurchaseRequest>() {


        @SuppressWarnings({
            "unchecked"
        })
        public OutletPurchaseRequest createFromParcel(Parcel in) {
            OutletPurchaseRequest instance = new OutletPurchaseRequest();
            instance.cardUid = ((String) in.readValue((String.class.getClassLoader())));
            in.readList(instance.chartDetails, (com.cleva.kampoengradja.model.OutletItem.class.getClassLoader()));
            instance.terminalSerialNumber = ((String) in.readValue((String.class.getClassLoader())));
            instance.totalPrice = ((int) in.readValue((int.class.getClassLoader())));
            return instance;
        }

        public OutletPurchaseRequest[] newArray(int size) {
            return (new OutletPurchaseRequest[size]);
        }

    }
    ;

    /**
     * 
     * @return
     *     The cardUid
     */
    public String getCardUid() {
        return cardUid;
    }

    /**
     * 
     * @param cardUid
     *     The cardUid
     */
    public void setCardUid(String cardUid) {
        this.cardUid = cardUid;
    }

    /**
     * 
     * @return
     *     The chartOutletDetails
     */
    public List<OutletItem> getOutletItems() {
        return chartDetails;
    }

    /**
     * 
     * @param chartOutletDetails
     *     The chartOutletDetails
     */
    public void setOutletItems(List<OutletItem> chartOutletDetails) {
        this.chartDetails = chartOutletDetails;
    }

    /**
     * 
     * @return
     *     The terminalSerialNumber
     */
    public String getTerminalSerialNumber() {
        return terminalSerialNumber;
    }

    /**
     * 
     * @param terminalSerialNumber
     *     The terminalSerialNumber
     */
    public void setTerminalSerialNumber(String terminalSerialNumber) {
        this.terminalSerialNumber = terminalSerialNumber;
    }

    /**
     * 
     * @return
     *     The totalPrice
     */
    public int getTotalPrice() {
        return totalPrice;
    }

    /**
     * 
     * @param totalPrice
     *     The totalPrice
     */
    public void setTotalPrice(int totalPrice) {
        this.totalPrice = totalPrice;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(cardUid);
        dest.writeList(chartDetails);
        dest.writeValue(terminalSerialNumber);
        dest.writeValue(totalPrice);
    }

    public int describeContents() {
        return  0;
    }

}
