
package com.cleva.kampoengradja.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TopUpResp implements Parcelable
{

    @SerializedName("bonusGiven")
    @Expose
    private int bonusGiven;
    @SerializedName("currentBalance")
    @Expose
    private int currentBalance;
    @SerializedName("currentBonus")
    @Expose
    private int currentBonus;
    @SerializedName("topUpAdditionGiven")
    @Expose
    private int topUpAdditionGiven;
    @SerializedName("topUpAmount")
    @Expose
    private int topUpAmount;
    @SerializedName("topUpFinalAmount")
    @Expose
    private int topUpFinalAmount;
    public final static Parcelable.Creator<TopUpResp> CREATOR = new Creator<TopUpResp>() {


        @SuppressWarnings({
            "unchecked"
        })
        public TopUpResp createFromParcel(Parcel in) {
            TopUpResp instance = new TopUpResp();
            instance.bonusGiven = ((int) in.readValue((int.class.getClassLoader())));
            instance.currentBalance = ((int) in.readValue((int.class.getClassLoader())));
            instance.currentBonus = ((int) in.readValue((int.class.getClassLoader())));
            instance.topUpAdditionGiven = ((int) in.readValue((int.class.getClassLoader())));
            instance.topUpAmount = ((int) in.readValue((int.class.getClassLoader())));
            instance.topUpFinalAmount = ((int) in.readValue((int.class.getClassLoader())));
            return instance;
        }

        public TopUpResp[] newArray(int size) {
            return (new TopUpResp[size]);
        }

    }
    ;

    /**
     * 
     * @return
     *     The bonusGiven
     */
    public int getBonusGiven() {
        return bonusGiven;
    }

    /**
     * 
     * @param bonusGiven
     *     The bonusGiven
     */
    public void setBonusGiven(int bonusGiven) {
        this.bonusGiven = bonusGiven;
    }

    /**
     * 
     * @return
     *     The currentBalance
     */
    public int getCurrentBalance() {
        return currentBalance;
    }

    /**
     * 
     * @param currentBalance
     *     The currentBalance
     */
    public void setCurrentBalance(int currentBalance) {
        this.currentBalance = currentBalance;
    }

    /**
     * 
     * @return
     *     The currentBonus
     */
    public int getCurrentBonus() {
        return currentBonus;
    }

    /**
     * 
     * @param currentBonus
     *     The currentBonus
     */
    public void setCurrentBonus(int currentBonus) {
        this.currentBonus = currentBonus;
    }

    /**
     * 
     * @return
     *     The topUpAdditionGiven
     */
    public int getTopUpAdditionGiven() {
        return topUpAdditionGiven;
    }

    /**
     * 
     * @param topUpAdditionGiven
     *     The topUpAdditionGiven
     */
    public void setTopUpAdditionGiven(int topUpAdditionGiven) {
        this.topUpAdditionGiven = topUpAdditionGiven;
    }

    /**
     * 
     * @return
     *     The topUpAmount
     */
    public int getTopUpAmount() {
        return topUpAmount;
    }

    /**
     * 
     * @param topUpAmount
     *     The topUpAmount
     */
    public void setTopUpAmount(int topUpAmount) {
        this.topUpAmount = topUpAmount;
    }

    /**
     * 
     * @return
     *     The topUpFinalAmount
     */
    public int getTopUpFinalAmount() {
        return topUpFinalAmount;
    }

    /**
     * 
     * @param topUpFinalAmount
     *     The topUpFinalAmount
     */
    public void setTopUpFinalAmount(int topUpFinalAmount) {
        this.topUpFinalAmount = topUpFinalAmount;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(bonusGiven);
        dest.writeValue(currentBalance);
        dest.writeValue(currentBonus);
        dest.writeValue(topUpAdditionGiven);
        dest.writeValue(topUpAmount);
        dest.writeValue(topUpFinalAmount);
    }

    public int describeContents() {
        return  0;
    }

}
