
package com.cleva.kampoengradja.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RefundDetailResp implements Parcelable
{

    @SerializedName("deposit")
    @Expose
    private int deposit;
    @SerializedName("refundAmount")
    @Expose
    private int refundAmount;
    @SerializedName("remainingBalance")
    @Expose
    private int remainingBalance;
    public final static Parcelable.Creator<RefundDetailResp> CREATOR = new Creator<RefundDetailResp>() {


        @SuppressWarnings({
            "unchecked"
        })
        public RefundDetailResp createFromParcel(Parcel in) {
            RefundDetailResp instance = new RefundDetailResp();
            instance.deposit = ((int) in.readValue((int.class.getClassLoader())));
            instance.refundAmount = ((int) in.readValue((int.class.getClassLoader())));
            instance.remainingBalance = ((int) in.readValue((int.class.getClassLoader())));
            return instance;
        }

        public RefundDetailResp[] newArray(int size) {
            return (new RefundDetailResp[size]);
        }

    }
    ;

    /**
     * 
     * @return
     *     The deposit
     */
    public int getDeposit() {
        return deposit;
    }

    /**
     * 
     * @param deposit
     *     The deposit
     */
    public void setDeposit(int deposit) {
        this.deposit = deposit;
    }

    /**
     * 
     * @return
     *     The refundAmount
     */
    public int getRefundAmount() {
        return refundAmount;
    }

    /**
     * 
     * @param refundAmount
     *     The refundAmount
     */
    public void setRefundAmount(int refundAmount) {
        this.refundAmount = refundAmount;
    }

    /**
     * 
     * @return
     *     The remainingBalance
     */
    public int getRemainingBalance() {
        return remainingBalance;
    }

    /**
     * 
     * @param remainingBalance
     *     The remainingBalance
     */
    public void setRemainingBalance(int remainingBalance) {
        this.remainingBalance = remainingBalance;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(deposit);
        dest.writeValue(refundAmount);
        dest.writeValue(remainingBalance);
    }

    public int describeContents() {
        return  0;
    }

}
