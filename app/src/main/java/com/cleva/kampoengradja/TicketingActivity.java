package com.cleva.kampoengradja;

import android.app.Dialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cleva.kampoengradja.adapter.TicketMoreAdapter;
import com.cleva.kampoengradja.adapter.TicketParkingAdapter;
import com.cleva.kampoengradja.adapter.TicketLoadMoreAdapter;
import com.cleva.kampoengradja.model.TiketParkirResp;
import com.cleva.kampoengradja.utils.Constants;
import com.cleva.kampoengradja.utils.Method;
import com.cleva.kampoengradja.utils.OnLoadMoreListener;
import com.cleva.kampoengradja.viewmodel.TiketViewModel;
import com.cleva.kampoengradja.widgets.ElegantNumberButton;
import com.cleva.kampoengradja.widgets.EndlessRecyclerOnScrollListener;
import com.nex3z.notificationbadge.NotificationBadge;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.cleva.kampoengradja.MyApp.restApi;
import static com.cleva.kampoengradja.MyApp.session;

public class TicketingActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "TicketingActivity";
    private int back = 0;
    Toolbar toolbar;
    NotificationBadge badge;
    View parent_view, rlKeranjang, llMore, llList;

    String num = "";

    RecyclerView rvTiket, rvALL, rvKID, rvTEEN, rvADULT, rvMore;

    TicketLoadMoreAdapter ticketMoreAdapter;
    List<TiketParkirResp> tiketParkir, tiketKID, tiketTeen, tiketAll, tiketAdult, listCount;
    TiketViewModel tiketViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ticketing);

        initView();

        tiketViewModel = ViewModelProviders.of(this).get(TiketViewModel.class);
        tiketViewModel.getAllTiketParkirResps().observe(this, new Observer<List<TiketParkirResp>>() {
            @Override
            public void onChanged(@Nullable List<TiketParkirResp> notes) {
                if (notes != null){
                    int sum = 0;
                    for (int i = 0; i < notes.size(); i++) {
                        sum += notes.get(i).getQuantity();
                    }
                    badge.setNumber(sum);
                }
                listCount = notes;
            }
        });

        getTiket();
        getTiketBy("ALL", 0, 3);
        getTiketBy("KID", 0, 3);
        getTiketBy("TEENAGER", 0, 3);
        getTiketBy("ADULT", 0, 3);
    }

    @Override
    public void onBackPressed() {
        if (back == 1) {
            llList.setVisibility(View.VISIBLE);
            llMore.setVisibility(View.GONE);
            back = 0;
        } else
            super.onBackPressed();
    }

    private int visibleThreshold = 3;
    private int lastVisibleItem, totalItemCount;
    private boolean loading;int mPreviousTotal = 0;

    void initView() {
        badge = findViewById(R.id.badge);
        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Ticketing");

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        parent_view = findViewById(R.id.parent_view);


        rlKeranjang = findViewById(R.id.rlKeranjang);
        rlKeranjang.setOnClickListener(this);

        llList = findViewById(R.id.llList);
        llMore = findViewById(R.id.llMore);

        rvTiket = findViewById(R.id.rvTiketParkir);
        rvALL = findViewById(R.id.rvPaketAll);
        rvKID = findViewById(R.id.rvPaketKid);
        rvTEEN = findViewById(R.id.rvPaketTeen);
        rvADULT = findViewById(R.id.rvPaketAdult);

        rvMore = findViewById(R.id.rvPaketMore);
        LinearLayoutManager horizontalLayoutManagaer
                = new LinearLayoutManager(TicketingActivity.this, LinearLayoutManager.VERTICAL, false);
        rvMore.setLayoutManager(horizontalLayoutManagaer);

        rvMore.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView,
                                   int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                Log.i(TAG, "onScrolled: "+dy);

                Log.i(TAG, "onScrolled: "+dy);
                int visibleItemCount = recyclerView.getChildCount();
                int totalItemCount = recyclerView.getLayoutManager().getItemCount();
                int firstVisibleItem = ((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstVisibleItemPosition();

                if (loading) {
                    if (totalItemCount > mPreviousTotal) {
                        loading = false;
                        mPreviousTotal = totalItemCount;
                    }
                }
                int visibleThreshold = 3;
                if (!loading && (totalItemCount - visibleItemCount)
                        <= (firstVisibleItem + visibleThreshold)) {
                    // End has been reached
                    Log.i(TAG, "onScrolled: reached");

//                    onLoadMore();

                    loading = true;
                }
//                totalItemCount = horizontalLayoutManagaer.getItemCount();
//                lastVisibleItem = horizontalLayoutManagaer
//                        .findLastVisibleItemPosition();
//                if (!loading
//                        && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
//                    // End has been reached
//                    // Do something
////                    if (onLoadMoreListener != null) {
////                        onLoadMoreListener.onLoadMore();
////                    }
//                    Log.i(TAG, "onScrolled: LoadMORES");
//                    loading = true;
//                }
            }
        });
    }

    void getTiket() {
        restApi.parkirTicket(0, 3, session.getAccessToken()).enqueue(new Callback<List<TiketParkirResp>>() {
            @Override
            public void onResponse(Call<List<TiketParkirResp>> call, Response<List<TiketParkirResp>> response) {
                if (response.code() == 200) {
                    if (response.body() != null) {
                        Log.i(TAG, "onResponse: " + response.body().size());
                        if (response.body().size() > 0) {

                            tiketParkir = response.body();
                            GridLayoutManager gridLayoutManager = new GridLayoutManager(getApplicationContext(),
                                    response.body().size() < 3 ? response.body().size() : 3);
                            gridLayoutManager.setOrientation(LinearLayoutManager.VERTICAL); // set Horizontal Orientation
                            rvTiket.setLayoutManager(gridLayoutManager); // set LayoutManager to RecyclerView

                            TicketParkingAdapter tpa = new TicketParkingAdapter(TicketingActivity.this,
                                    tiketParkir, item -> {
                                        Log.i(TAG, "onItemClick: " + item.getTitle());
                                        showDialogImageCenter(item);
                                    });

                            rvTiket.setAdapter(tpa);
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<List<TiketParkirResp>> call, Throwable t) {

            }
        });
    }

    void getTikets(int pageNum, int pageSize, List<TiketParkirResp> tpr) {
        restApi.parkirTicket(pageNum, pageSize,session.getAccessToken()).enqueue(new Callback<List<TiketParkirResp>>() {
            @Override
            public void onResponse(Call<List<TiketParkirResp>> call, Response<List<TiketParkirResp>> response) {
                if (response.code() == 200) {
                    if (response.body() != null) {
                        Log.i(TAG, "onResponse: " + response.body().size());
                        if (response.body().size() > 0) {

                            tpr.remove(tpr.size() - 1);
                            ticketMoreAdapter.notifyItemRemoved(tpr.size());
                            for (int i = 0; i < response.body().size(); i++) {
                                TiketParkirResp tprz = new TiketParkirResp();
                                tprz.setItemId(response.body().get(i).getItemId());
                                tprz.setImagePath(response.body().get(i).getImagePath());
                                tprz.setPrice(response.body().get(i).getPrice());
                                tprz.setTitle(response.body().get(i).getTitle());
                                tpr.add(tprz);
                            }
                            ticketMoreAdapter.setLoaded();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<List<TiketParkirResp>> call, Throwable t) {

            }
        });
    }

    void getTiketBy(final String type, int pageNum, int pageSize) {
        restApi.packageByAge(type, pageNum, pageSize, session.getAccessToken()).enqueue(new Callback<List<TiketParkirResp>>() {
            @Override
            public void onResponse(Call<List<TiketParkirResp>> call, Response<List<TiketParkirResp>> response) {
                if (response.code() == 200) {
                    if (response.body() != null) {
                        Log.i(TAG, "onResponse: " + response.body().size());
                        if (response.body().size() > 0) {

                            GridLayoutManager gridLayoutManager = new GridLayoutManager(getApplicationContext(),
                                    response.body().size() < 3 ? response.body().size() : 3);
                            gridLayoutManager.setOrientation(LinearLayoutManager.VERTICAL); // set Horizontal Orientation

                            TicketParkingAdapter tpa = new TicketParkingAdapter(TicketingActivity.this,
                                    response.body(), new TicketParkingAdapter.OnItemClickListener() {
                                @Override
                                public void onItemClick(TiketParkirResp item) {
                                    Log.i(TAG, "onItemClick: " + item.getTitle());
                                    showDialogImageCenter(item);
                                }
                            });

                            switch (type) {
                                case "ALL":
                                    tiketAll = response.body();
                                    rvALL.setLayoutManager(gridLayoutManager); // set LayoutManager to RecyclerView
                                    rvALL.setAdapter(tpa);

                                    break;
                                case "KID":
                                    tiketKID = response.body();
                                    rvKID.setLayoutManager(gridLayoutManager); // set LayoutManager to RecyclerView
                                    rvKID.setAdapter(tpa);

                                    break;
                                case "TEENAGER":
                                    tiketTeen = response.body();
                                    rvTEEN.setLayoutManager(gridLayoutManager); // set LayoutManager to RecyclerView
                                    rvTEEN.setAdapter(tpa);

                                    break;
                                default:
                                    tiketAdult = response.body();
                                    rvADULT.setLayoutManager(gridLayoutManager); // set LayoutManager to RecyclerView
                                    rvADULT.setAdapter(tpa);

                                    break;
                            }
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<List<TiketParkirResp>> call, Throwable t) {

            }
        });
    }

    public void doDetail(View v) {
        startActivity(new Intent(this, DetailTicketingActivity.class));
    }

    public void doDetail(TiketParkirResp item) {
        Intent intent =new Intent(this, DetailTicketingActivity.class);
        intent.putExtra("itemID", item.getItemId());
        startActivity(intent);
    }

    public void doChart() {
        if (listCount.size() > 0)
            startActivity(new Intent(this, KeranjangActivity.class));
        else
            Snackbar.make(parent_view, "Keranjang masih kosong", Snackbar.LENGTH_INDEFINITE).setDuration(3000).show();

    }

    public void doLoadMore(View v) {
        back = 1;
        llList.setVisibility(View.GONE);
        llMore.setVisibility(View.VISIBLE);
        rvMore.setAdapter(null);
        if (v.getTag().equals("parkir")) {
            if (tiketParkir != null)
                setMore(tiketParkir);
        } else if (v.getTag().equals("KID")) {
            if (tiketKID != null)
                setMore(tiketKID);
        } else if (v.getTag().equals("All")) {
            if (tiketAll != null)
                setMore(tiketAll);
        } else if (v.getTag().equals("TEENAGER")) {
            if (tiketTeen != null)
                setMore(tiketTeen);
        } else if (v.getTag().equals("ADULT")) {
            if (tiketAdult != null)
                setMore(tiketAdult);
        }
    }

    void setMore(List<TiketParkirResp> tpr) {
        ticketMoreAdapter = new TicketLoadMoreAdapter(this, tpr, new TicketLoadMoreAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(TiketParkirResp item) {

            }

            @Override
            public void onItemClick(TiketParkirResp item, int w) {
                if (w == 1){
                    showDialogImageCenter(item);
                } else {
                    doDetail(item);
                }
            }
        }, rvMore);
        rvMore.setAdapter(ticketMoreAdapter);

//        ticketMoreAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
//            @Override
//            public void onLoadMore() {
//                Log.i(TAG, "onLoadMore: ");
//                //add null , so the adapter will check view_type and show progress bar at bottom
//                tpr.add(null);
////                rvMore.post(new Runnable() {
////                    public void run() {
//                        ticketMoreAdapter.notifyItemInserted(tpr.size() - 1);
////                    }
////                });
//
//                new Handler().postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        //   remove progress item
//
//                    getTikets(0, 3, tpr);
////                        tpr.remove(tpr.size() - 1);
////                        ticketMoreAdapter.notifyItemRemoved(tpr.size());
////                        //add items one by one
//////                        int start = tpr.size();
//////                        int end = start + 20;
////
//////                        for (int i = start + 1; i <= end; i++) {
//////                            studentList.add(new Student("Student " + i, "AndroidStudent" + i + "@gmail.com"));
//////                            ticketMoreAdapter.notifyItemInserted(studentList.size());
//////                        }
////                        ticketMoreAdapter.setLoaded();
//                        //or you can add all at once but do not forget to call mAdapter.notifyDataSetChanged();
//                    }
//                }, 2000);
//
//            }
//        });
    }

    public void showDialogImageCenter(final TiketParkirResp item) {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.dialog_tiket_keranjang);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(true);

        final TextView tvTitle, tvPrice, tvType, tvValid;

        tvTitle = dialog.findViewById(R.id.tvTitle);
        tvPrice = dialog.findViewById(R.id.tvPrice);
        tvType = dialog.findViewById(R.id.tvType);
        tvValid = dialog.findViewById(R.id.tvValid);

        tvTitle.setText(item.getTitle());
        tvPrice.setText(Method.getFormatIDRS(item.getPrice()));
        ImageView img = dialog.findViewById(R.id.image);
        if (item.getImagePath() != null) {
            // byte[] imageByteArray = Base64.decode(item.getImageBase64(), Base64.DEFAULT);
            // here imageBytes is base64String

            Glide.with(TicketingActivity.this)
                    .load(Constants.BASE_URL_IMAGE+item.getImagePath())
                    .into(img);
        }

        final ElegantNumberButton button = dialog.findViewById(R.id.number_button);
        button.setNumber("1");
        num = "1";
        button.setOnClickListener(new ElegantNumberButton.OnClickListener() {
            @Override
            public void onClick(View view) {
                num = button.getNumber();

                Log.i(TAG, "onClick: " + num);
                if (button.getNumbers() == 0)
                    dialog.hide();
            }
        });

        AppCompatButton btn = dialog.findViewById(R.id.bt_add_to_cart);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                List<TiketParkirResp> getTiket = tiketViewModel.getTiketById(item.getItemId());
                if (getTiket != null) {
                    Log.i(TAG, "onClick: "+getTiket.size());

                    TiketParkirResp tpr;
                    if (getTiket.size() == 1){
                        tpr = getTiket.get(0);
                        Snackbar.make(parent_view, tvTitle.getText().toString() + " berhasil masuk keranjang", Snackbar.LENGTH_INDEFINITE).setDuration(3000).show();
                        dialog.hide();

                        item.setQuantity(button.getNumbers()+tpr.getQuantity());
                        tiketViewModel.update(item);
                    } else {
                        Log.e(TAG, "onClick: Size 0");
                        Snackbar.make(parent_view, tvTitle.getText().toString() + " berhasil masuk keranjang", Snackbar.LENGTH_INDEFINITE).setDuration(3000).show();
                        dialog.hide();

                        item.setQuantity(button.getNumbers());
                        tiketViewModel.insert(item);
                    }

                } else {
                    Log.i(TAG, "onClick: else ");
                    Snackbar.make(parent_view, tvTitle.getText().toString() + " berhasil masuk keranjang", Snackbar.LENGTH_INDEFINITE).setDuration(3000).show();
                    dialog.hide();

                    item.setQuantity(button.getNumbers());
                    tiketViewModel.insert(item);
                }
            }
        });

        dialog.show();
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.rlKeranjang) {
            doChart();
        }
    }
}
