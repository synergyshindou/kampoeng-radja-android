package com.cleva.kampoengradja;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cleva.kampoengradja.adapter.FreeToPlayAdapter;
import com.cleva.kampoengradja.adapter.RiwayatTopUpAdapter;
import com.cleva.kampoengradja.adapter.TicketUsageAdapter;
import com.cleva.kampoengradja.model.AuthCardResp;
import com.cleva.kampoengradja.model.CardInfoDataJava;
import com.cleva.kampoengradja.model.CardTerminal;
import com.cleva.kampoengradja.model.FreeToPlay;
import com.cleva.kampoengradja.model.TicketUsageHistory;
import com.cleva.kampoengradja.model.TopUpHistory;
import com.cleva.kampoengradja.rest.ApiConfig;
import com.cleva.kampoengradja.rest.AppConfig;
import com.cleva.kampoengradja.utils.ISOUtil;
import com.cleva.kampoengradja.utils.Method;
import com.pos.device.SDKException;
import com.pos.device.config.DevConfig;
import com.pos.device.picc.EmvContactlessCard;
import com.pos.device.picc.MifareClassic;
import com.pos.device.picc.PiccReader;
import com.pos.device.picc.PiccReaderCallback;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.cleva.kampoengradja.MyApp.readCards;
import static com.cleva.kampoengradja.MyApp.session;
import static com.cleva.kampoengradja.utils.Constants.AUTH_0;
import static com.cleva.kampoengradja.utils.Constants.AUTH_M1;
import static com.cleva.kampoengradja.utils.Constants.AUTH_M2;
import static com.cleva.kampoengradja.utils.Constants.AUTH_M3;
import static com.cleva.kampoengradja.utils.Constants.AUTH_M4;
import static com.cleva.kampoengradja.utils.Method.getFormatIDR;
import static com.cleva.kampoengradja.MyApp.restApi;

public class InfoKartuActivity extends AppCompatActivity {

    private static final String TAG = "InfoKartuActivity";
    View parent_view, llTap, llInfo, llError, llNameMember;
    TextView tvMember, tvSaldo, tvBonus, tvDeposit, tvNamaMember;
    FreeToPlayAdapter freeToPlayAdapter;
    RecyclerView recyclerView, rvRTopup, rvRTopupPackage;
    ImageView imgLoad;

    PiccReader piccReader;
    private EmvContactlessCard emvContactlessCard = null;

    MifareClassic mifareClassic = null;

    int tap = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_kartu);
        initView();

        piccReader = PiccReader.getInstance();
        getCard();
    }

    @Override
    public void onBackPressed() {
        if (tap == 1){
            llError.setVisibility(View.GONE);
            llInfo.setVisibility(View.GONE);
            llTap.setVisibility(View.VISIBLE);
            tap = 0;
            getCard();
        } else
            super.onBackPressed();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (piccReader != null){
            piccReader.stopSearchCard();
            try {
                piccReader.release();
            } catch (SDKException e) {
                e.printStackTrace();
            }
        }
    }

    public void initView() {
        tvMember = findViewById(R.id.tvMember);
        tvSaldo = findViewById(R.id.tvSaldo);
        tvBonus = findViewById(R.id.tvBonus);
        tvDeposit = findViewById(R.id.tvDeposit);
        recyclerView = findViewById(R.id.rvFreetoplay);
        rvRTopup = findViewById(R.id.rvRTopup);
        rvRTopupPackage = findViewById(R.id.rvRTopupPackage);
        parent_view = findViewById(R.id.parent_view);
        llInfo = findViewById(R.id.llInfo);
        llTap = findViewById(R.id.llTap);
        llError = findViewById(R.id.llError);
        imgLoad = findViewById(R.id.imgloads);

        llNameMember = findViewById(R.id.llNamaMember);
        tvNamaMember = findViewById(R.id.tvName);

        Glide.with(this)
                .load(R.drawable.tap)
                .into(imgLoad);
    }

    public void timeoutScan(){

        llError.setVisibility(View.VISIBLE);
        llTap.setVisibility(View.GONE);
        Snackbar.make(parent_view, "Kartu tidak ditemukan", Snackbar.LENGTH_INDEFINITE).setDuration(3000).show();
        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        llError.setVisibility(View.GONE);
                        llTap.setVisibility(View.VISIBLE);
                        getCard();
                    }
                },
                3000);
    }

    public void getCard() {
        new Thread() {
            @Override
            public void run() {
                Log.i(TAG, "CardManager>>getCard>>NFC");
                piccReader.startSearchCard(0, new PiccReaderCallback() {
                    @Override
                    public void onSearchResult(int i, int i1) {
                        try {
                            Thread.sleep(400);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        Log.i(TAG, "CardManager>>getCard>>NFC>>i=" + i);
                        if (0 == i) {
//                                        listener.callback(handlePICC(i1));
                            Log.i(TAG, "CardManager>>getCard>>NFC_TYPE>>i=" + i1);
                            if (i1 == 0 || i1 == 1) {
                                initMifare();
                            }
//                            else
//                                initEMV();
                        } else {
                            Log.i(TAG, "onSearchResult: not found");
                            timeoutScan();
                        }
                    }
                });
            }
        }.start();
    }

    public void initMifare() {
        try {
            mifareClassic = MifareClassic.connect();

            if (mifareClassic != null) {
                byte[] bUID = mifareClassic.getUID();
                if (bUID != null) {
                    Log.i(TAG, "initMifare: UID = " + ISOUtil.hexString(bUID));
                    getCardAuth(ISOUtil.hexString(bUID), bUID);
                } else {
                    Log.e(TAG, "initMifare: UID FAILED");
                    timeoutScan();
                }

            } else {
                Log.e(TAG, "initMifare: FAILED");
                timeoutScan();
            }
        } catch (SDKException e) {
            e.printStackTrace();
        }
    }

    public void doTryAgain(View v){
        llError.setVisibility(View.GONE);
        llTap.setVisibility(View.VISIBLE);
        getCard();
    }

    void getCardAuth(String a, byte[] bUid){
        CardTerminal ct = new CardTerminal();
        ct.setCardUid(a);
        ct.setTerminalSerialNumber(DevConfig.getSN());

        restApi.authCard(ct, session.getAccessToken()).enqueue(new Callback<AuthCardResp>() {
            @Override
            public void onResponse(Call<AuthCardResp> call, Response<AuthCardResp> response) {
                if (response.isSuccessful()){
                    if (response.body() != null){
                        int ares = readCards(mifareClassic, response.body().getAuthKey(), bUid, response.body().getAuthData());
                        Log.i(TAG, "onResponse: ares = "+ ares);
                        switch (ares) {
                            case 1:
                                getCardInfo(ct.getCardUid());
                                break;
                            case 0:
                                showConfirmDialog(AUTH_0);
                                break;
                            case -4:
                                showConfirmDialog(AUTH_M4);
                                break;
                            case -3:
                                showConfirmDialog(AUTH_M3);
                                break;
                            case -2:
                                showConfirmDialog(AUTH_M2);
                                break;
                            case -1:
                                showConfirmDialog(AUTH_M1);
                                break;
                        }
                    } else {
                        try {
                            Method.showErorRest(parent_view, response.errorBody().string());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        getCard();
                    }
                } else {
                    try {
                        Method.showErorRest(parent_view, response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    getCard();
                }
            }

            @Override
            public void onFailure(Call<AuthCardResp> call, Throwable t) {
                t.printStackTrace();
                showCustomDialogs(InfoKartuActivity.this);

            }
        });

    }

    public void getCardInfo(String cardUID){
        restApi.cardInfo(cardUID, session.getAccessToken()).enqueue(new Callback<CardInfoDataJava>() {
            @Override
            public void onResponse(Call<CardInfoDataJava> call, Response<CardInfoDataJava> response) {
                if (response.code() == 200) {
                    if (response.body() != null) {
                        llError.setVisibility(View.GONE);
                        llInfo.setVisibility(View.VISIBLE);
                        llTap.setVisibility(View.GONE);
                        Log.i(TAG, "onResponse: CARD INFO Balanca " + response.body().getBalance());
                        if (response.body().getMemberName() != null){
                            llNameMember.setVisibility(View.VISIBLE);
                            tvNamaMember.setText(response.body().getMemberName());
                        } else
                            llNameMember.setVisibility(View.GONE);

                        tvMember.setText(response.body().getMembership());
                        tvSaldo.setText(getFormatIDR(response.body().getBalance()));
                        tvBonus.setText(getFormatIDR(response.body().getBonus()));
                        tvDeposit.setText(getFormatIDR(response.body().getDeposit()));
                        if (response.body().getFreeToPlay().size() > 0) {

                            LinearLayoutManager horizontalLayoutManagaer
                                    = new LinearLayoutManager(InfoKartuActivity.this, LinearLayoutManager.VERTICAL, false);
                            recyclerView.setLayoutManager(horizontalLayoutManagaer);

                            freeToPlayAdapter = new FreeToPlayAdapter(InfoKartuActivity.this, response.body().getFreeToPlay(), new FreeToPlayAdapter.OnItemClickListener() {
                                @Override
                                public void onItemClick(FreeToPlay item) {
                                    Log.i(TAG, "onItemClick: " + item.getAttractionName());
                                }
                            });

                            recyclerView.setAdapter(freeToPlayAdapter);
                        } else {
                            recyclerView.setVisibility(View.GONE);
                        }

                        if (response.body().getTopUpHistory().size() > 0){
                            LinearLayoutManager horizontalLayoutManagaer
                                    = new LinearLayoutManager(InfoKartuActivity.this, LinearLayoutManager.VERTICAL, false);
                            rvRTopup.setLayoutManager(horizontalLayoutManagaer);

                            RiwayatTopUpAdapter riwayatTopUpAdapter = new RiwayatTopUpAdapter(InfoKartuActivity.this,
                                    response.body().getTopUpHistory(), new RiwayatTopUpAdapter.OnItemClickListener() {
                                @Override
                                public void onItemClick(TopUpHistory item) {
                                    Log.i(TAG, "onItemClick: " + item.getTopUpAmount());
                                }
                            });

                            rvRTopup.setAdapter(riwayatTopUpAdapter);
                        }

                        if (response.body().getTicketUsageHistory().size() > 0){
                            LinearLayoutManager horizontalLayoutManagaer
                                    = new LinearLayoutManager(InfoKartuActivity.this, LinearLayoutManager.VERTICAL, false);
                            rvRTopupPackage.setLayoutManager(horizontalLayoutManagaer);

                            TicketUsageAdapter ticketUsageAdapter = new TicketUsageAdapter(InfoKartuActivity.this,
                                    response.body().getTicketUsageHistory(), new TicketUsageAdapter.OnItemClickListener() {
                                @Override
                                public void onItemClick(TicketUsageHistory item) {
                                    Log.i(TAG, "onItemClick: TUA "+item.getAttractionName());
                                }
                            });

                            rvRTopupPackage.setAdapter(ticketUsageAdapter);
                        }
                        tap = 1;
                    } else {
                        try {
                            Method.showErorRest(parent_view, response.errorBody().string());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        getCard();
                    }
                } else {
                    try {
                        Method.showErorRest(parent_view, response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    getCard();
                }
            }

            @Override
            public void onFailure(Call<CardInfoDataJava> call, Throwable t) {

                llError.setVisibility(View.VISIBLE);
                llTap.setVisibility(View.GONE);
                getCard();
            }
        });
    }

    private void showConfirmDialog(String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Kampoeng Radja");
        builder.setMessage(msg);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                getCard();
            }
        });
        builder.show();
    }


    public void showCustomDialogs(Context context) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.dialog_warning);
        dialog.setCancelable(true);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;


        ((AppCompatButton) dialog.findViewById(R.id.bt_close)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getCard();
            }
        });

        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }
}
