package com.cleva.kampoengradja.rest;

import com.cleva.kampoengradja.model.AuthCardResp;
import com.cleva.kampoengradja.model.CalcPriceDetail;
import com.cleva.kampoengradja.model.CalculatePriceResp;
import com.cleva.kampoengradja.model.CardActivationReq;
import com.cleva.kampoengradja.model.CardIdentifierId;
import com.cleva.kampoengradja.model.CardInfoDataJava;
import com.cleva.kampoengradja.model.CardPersoResp;
import com.cleva.kampoengradja.model.CardRepersoResp;
import com.cleva.kampoengradja.model.CardTerminal;
import com.cleva.kampoengradja.model.DetailTicketResp;
import com.cleva.kampoengradja.model.ItemCategory;
import com.cleva.kampoengradja.model.OutletItem;
import com.cleva.kampoengradja.model.OutletPurchase;
import com.cleva.kampoengradja.model.OutletPurchaseRequest;
import com.cleva.kampoengradja.model.PaymentMethodResp;
import com.cleva.kampoengradja.model.RefundDetailReq;
import com.cleva.kampoengradja.model.RefundDetailResp;
import com.cleva.kampoengradja.model.RefundResp;
import com.cleva.kampoengradja.model.TerminalAllocation;
import com.cleva.kampoengradja.model.TicketPurchaseResp;
import com.cleva.kampoengradja.model.TicketingPurchaseRequest;
import com.cleva.kampoengradja.model.TiketParkirResp;
import com.cleva.kampoengradja.model.TopUpReq;
import com.cleva.kampoengradja.model.TopUpResp;

import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;


public interface ApiConfig {

    @GET("card-info")
    Call<CardInfoDataJava> cardInfo(@Query("cardUid") String cardUID,
                                    @Header("Authorization") String authHeader);

    @GET("card/identifierid/{cardUID}")
    Call<CardIdentifierId> getCardIdentifierId(@Path("cardUID") String cardUID,
                                    @Header("Authorization") String authHeader);

    @GET("terminal/allocation")
    Call<TerminalAllocation> getTerminalAllocation(@Query("terminalSerialNumber") String terminalSerialNumber,
                                                   @Header("Authorization") String authHeader);

    @GET("ticketing/parking-tickets")
    Call<List<TiketParkirResp>> parkirTicket(@Query("pageNumber") int pageNumber,
                                             @Query("pageSize") int pageSize,
                                             @Header("Authorization") String authHeader);

    @GET("ticketing/entrance-package-by-age-type")
    Call<List<TiketParkirResp>> packageByAge(@Query("ageType") String ageType,
                                             @Query("pageNumber") int pageNumber,
                                             @Query("pageSize") int pageSize,
                                             @Header("Authorization") String authHeader);

    @GET("ticketing/detail/{itemId}")
    Call<DetailTicketResp> getItemDetail(@Path("itemId") String itemId,
                                         @Header("Authorization") String authHeader);

    @GET("ticketing/payment/method")
    Call<List<PaymentMethodResp>> getPaymentMethod(
            @Header("Authorization") String authHeader);

    @GET("item/category")
    Call<List<ItemCategory>> getAllCategories(@Header("Authorization") String authHeader);

    @GET("outlet/outlet-item-by-category")
    Call<List<OutletItem>> getOutletItemByCategory(@Query("itemCategory") String itemCategory,
                                                   @Query("outletCode") String outletCode,
                                                   @Query("pageNumber") int pageNumber,
                                                   @Query("pageSize") int pageSize,
                                                   @Header("Authorization") String authHeader);

    @GET("outlet/outlet-item-by-code")
    Call<OutletItem> getOutletItemByCode(@Query("itemCode") String itemCode,
                                                   @Query("outletCode") String outletCode,
                                                   @Header("Authorization") String authHeader);

    @POST("topup")
    Call<TopUpResp> topUpRequest(@Body TopUpReq topUpReq,
                                 @Header("Authorization") String authHeader);

    @POST("card/perso")
    Call<CardPersoResp> cardPerso(@Body CardTerminal cardTerminal,
                                  @Header("Authorization") String authHeader);

    @POST("card/reperso")
    Call<CardRepersoResp> cardReperso(@Body CardTerminal cardTerminal,
                                      @Header("Authorization") String authHeader);

    @POST("card/activate")
    Call<Void> activeCard(@Body CardActivationReq cardActivationReq,
                          @Header("Authorization") String authHeader);

    @POST("auth/card")
    Call<AuthCardResp> authCard(@Body CardTerminal cardTerminal,
                                  @Header("Authorization") String authHeader);

    @POST("ticketing/calculate-price-detail")
    Call<CalculatePriceResp> calculatePriceDetail(@Body CalcPriceDetail calcPriceDetail,
                                                  @Header("Authorization") String authHeader);

    @POST("ticketing/purchase")
    Call<TicketPurchaseResp> tiketPurchase(@Body TicketingPurchaseRequest ticketingPurchaseRequest,
                                           @Header("Authorization") String authHeader);

    @POST("outlet/purchase")
    Call<OutletPurchase> outletPurchase(@Body OutletPurchaseRequest outletPurchaseRequest,
                                        @Header("Authorization") String authHeader);

    @POST("refund/detail")
    Call<RefundDetailResp> refundDetail(@Body RefundDetailReq refundDetailReq,
                                        @Header("Authorization") String authHeader);

    @POST("refund")
    Call<RefundResp> refund(@Body RefundDetailReq refundDetailReq,
                            @Header("Authorization") String authHeader);

    @POST("parking/park-in")
    Call<Void> parkIn(@Body CardTerminal cardTerminal,
                      @Header("Authorization") String authHeader);

    @POST("parking/park-out")
    Call<Void> parkOut(@Body CardTerminal cardTerminal,
                       @Header("Authorization") String authHeader);

    @POST("attraction/access")
    Call<Void> wahanaAccess(@Body CardTerminal cardTerminal,
                            @Header("Authorization") String authHeader);

}