package com.cleva.kampoengradja.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import com.cleva.kampoengradja.model.OutletItem;
import com.cleva.kampoengradja.repo.OutletRepository;
import com.cleva.kampoengradja.repo.TiketRepository;

import java.util.List;

public class OutletViewModel extends AndroidViewModel {

    private OutletRepository repository;
    private LiveData<List<OutletItem>> allOutletItems;

    public OutletViewModel(@NonNull Application application) {
        super(application);
        repository = new OutletRepository(application);
        allOutletItems = repository.getAllOutletItems();
    }

    public void insert(OutletItem note){
        repository.insert(note);
    }
    public void update(OutletItem note){
        repository.update(note);
    }
    public void delete(OutletItem note){
        repository.delete(note);
    }
    public void deleteAllOutletItems(){
        repository.deleteAllOutletItems();
    }

    public LiveData<List<OutletItem>> getAllOutletItems(){
        return allOutletItems;
    }

    public List<OutletItem> getTiketById(int itemId){
        return repository.getAllTiketById(itemId);
    }
}
