package com.cleva.kampoengradja.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import com.cleva.kampoengradja.model.TiketParkirResp;
import com.cleva.kampoengradja.repo.TiketRepository;

import java.util.List;

public class TiketViewModel extends AndroidViewModel {

    private TiketRepository repository;
    private LiveData<List<TiketParkirResp>> allTiketParkirResps;

    public TiketViewModel(@NonNull Application application) {
        super(application);
        repository = new TiketRepository(application);
        allTiketParkirResps = repository.getAllTiketParkirResps();
    }

    public void insert(TiketParkirResp note){
        repository.insert(note);
    }
    public void update(TiketParkirResp note){
        repository.update(note);
    }
    public void delete(TiketParkirResp note){
        repository.delete(note);
    }
    public void deleteAllTiketParkirResps(){
        repository.deleteAllTiketParkirResps();
    }

    public LiveData<List<TiketParkirResp>> getAllTiketParkirResps(){
        return allTiketParkirResps;
    }

    public List<TiketParkirResp> getTiketById(int itemId){
        return repository.getAllTiketById(itemId);
    }
}
