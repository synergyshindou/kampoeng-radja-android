package com.cleva.kampoengradja;

import android.app.Dialog;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.CardView;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cleva.kampoengradja.model.CardActivationReq;
import com.cleva.kampoengradja.model.CardPersoResp;
import com.cleva.kampoengradja.model.CardRepersoResp;
import com.cleva.kampoengradja.model.CardTerminal;
import com.cleva.kampoengradja.utils.Constants;
import com.cleva.kampoengradja.utils.ISOUtil;
import com.cleva.kampoengradja.utils.Method;
import com.pos.device.SDKException;
import com.pos.device.config.DevConfig;
import com.pos.device.picc.EmvContactlessCard;
import com.pos.device.picc.MifareClassic;
import com.pos.device.picc.PiccReader;
import com.pos.device.picc.PiccReaderCallback;

import java.io.IOException;
import java.util.Random;

import javax.crypto.*;
import javax.crypto.spec.SecretKeySpec;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.cleva.kampoengradja.MyApp.restApi;
import static com.cleva.kampoengradja.MyApp.session;
import static com.cleva.kampoengradja.utils.Constants.DEFAULT_KEY_AUTH;
import static com.cleva.kampoengradja.utils.Method.showCustomDialogs;

public class CardPersoActivity extends AppCompatActivity {

    private static final String TAG = "CardPersoActivity";
    View parent_view;
    TextView tvCardNumber;

    ImageView imgLoad;

    CardView cvTapCard, cvTapSuccess;

    AppCompatButton btnPerso;
    final int min = 1000;
    final int max = 9999;
    int random, random2;
    int random3;
    int random4;

    PiccReader piccReader;
    private EmvContactlessCard emvContactlessCard = null;

    MifareClassic mifareClassic = null;

    CardActivationReq car = new CardActivationReq();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_perso);
        initView();
        setTitle("Card Perso");
        piccReader = PiccReader.getInstance();
        getCard();
    }

    public void initView() {
        imgLoad = findViewById(R.id.imgloads);

        Glide.with(this)
                .load(R.drawable.tap)
                .into(imgLoad);

        tvCardNumber = findViewById(R.id.tvCardNumber);

        cvTapCard = findViewById(R.id.cvTapCard);
        cvTapCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                random = new Random().nextInt((max - min) + 1) + min;
                random2 = new Random().nextInt((max - min) + 1) + min;
                random3 = new Random().nextInt((max - min) + 1) + min;
                random4 = new Random().nextInt((max - min) + 1) + min;

                tvCardNumber.setText(random + " " + random2 + " " + random3 + " " + random4);

//                cvTapCard.setVisibility(View.GONE);
//                cvTapSuccess.setVisibility(View.VISIBLE);
            }
        });

        cvTapSuccess = findViewById(R.id.cvTapCardSuccess);
        cvTapSuccess.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                cvTapCard.setVisibility(View.VISIBLE);
//                cvTapSuccess.setVisibility(View.GONE);
            }
        });

        btnPerso = findViewById(R.id.btnPerso);
        btnPerso.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                cvTapCard.setVisibility(View.VISIBLE);
                cvTapSuccess.setVisibility(View.GONE);
                getCard();
            }
        });

        parent_view = findViewById(R.id.parent_view);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (piccReader != null) {
            piccReader.stopSearchCard();
            try {
                piccReader.release();
            } catch (SDKException e) {
                e.printStackTrace();
            }
        }
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.menu_setting, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        if (item.getItemId() == android.R.id.home) {
//            finish();
//        } else {
//            showCustomDialog();
//        }
//        return super.onOptionsItemSelected(item);
//    }

    void getCardInfo(String a, byte[] sUID) {
        random = new Random().nextInt((max - min) + 1) + min;
        random2 = new Random().nextInt((max - min) + 1) + min;
        random3 = new Random().nextInt((max - min) + 1) + min;
        random4 = new Random().nextInt((max - min) + 1) + min;

//        tvCardNumber.setText(random + " " + random2 + " " + random3 + " " + random4);

        CardTerminal ct = new CardTerminal();
        ct.setTerminalSerialNumber(DevConfig.getSN());
//        ct.setCardUid("D925EC21");
        ct.setCardUid(a);

        restApi.cardPerso(ct, session.getAccessToken()).enqueue(new Callback<CardPersoResp>() {
            @Override
            public void onResponse(Call<CardPersoResp> call, Response<CardPersoResp> response) {
                if (response.code() == 200) {
                    if (response.body() != null) {
                        String num = response.body().getCardNumber();
                        String aD = response.body().getAuthData();
                        String sD = response.body().getSecretData();
                        persoCard(num, aD, sD, ct, sUID);

                    } else {
                        try {
                            Method.showErorRest(parent_view, response.errorBody().string());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        getCard();
                    }
                } else {
                    try {
                        Method.showErorRest(parent_view, response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    repersoCard(ct, sUID);
                }
            }

            @Override
            public void onFailure(Call<CardPersoResp> call, Throwable t) {
                t.printStackTrace();
                showCustomDialogs(CardPersoActivity.this);

            }
        });
    }

    void repersoCard(CardTerminal ct, byte[] sUID) {
        restApi.cardReperso(ct, session.getAccessToken()).enqueue(new Callback<CardRepersoResp>() {
            @Override
            public void onResponse(Call<CardRepersoResp> call, Response<CardRepersoResp> response) {
                if (response.code() == 200) {
                    if (response.body() != null) {
                        String num = response.body().getCardNumber();
                        String aD = response.body().getAuthData();
                        String sD = response.body().getSecretData();
                        String sKey = response.body().getAuthKey();
                        repersoCards(num, aD, sD, ct, sUID, sKey);

                    } else {
                        try {
                            Method.showErorRest(parent_view, response.errorBody().string());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        getCard();
                    }
                } else {
                    try {
                        Method.showErorRest(parent_view, response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    getCard();
                }
            }

            @Override
            public void onFailure(Call<CardRepersoResp> call, Throwable t) {
                t.printStackTrace();
                showCustomDialogs(CardPersoActivity.this);

            }
        });
    }

    void activeCard() {
        restApi.activeCard(car, session.getAccessToken()).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (response.isSuccessful()) {
                    cvTapCard.setVisibility(View.GONE);
                    cvTapSuccess.setVisibility(View.VISIBLE);
                } else {
                    try {
                        Method.showErorRest(parent_view, response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                t.printStackTrace();
                showCustomDialogs(CardPersoActivity.this);
            }
        });
    }

    void persoCard(String num, String aD, String sD, CardTerminal ct, byte[] sUID) {
        tvCardNumber.setText(Method.splitCardNum(num));
        byte[] decodeAuth = Base64.decode(aD, Base64.NO_WRAP);
        String ss = ISOUtil.hexString(decodeAuth);
        Log.i(TAG, "onResponse: Auth = " + ss);

        byte[] block4 = new byte[16];
        System.arraycopy(decodeAuth, 0, block4, 0, 16);
        String ss4 = ISOUtil.hexString(block4);
        Log.i(TAG, "onResponse: Auth block 4 = " + ss4);

        byte[] block5 = new byte[16];
        System.arraycopy(decodeAuth, 16, block5, 0, 16);
        String ss5 = ISOUtil.hexString(block5);
        Log.i(TAG, "onResponse: Auth block 5 = " + ss5);

        byte[] decodeSecret = Base64.decode(sD, Base64.NO_WRAP);
        String sc = ISOUtil.hexString(decodeSecret);
        Log.i(TAG, "onResponse: Secr = " + sc);

        car.setCardUid(ct.getCardUid());
        car.setEncryptedValue(aD);
        car.setTerminalSerialNumber(DevConfig.getSN());

        int w45 = writeCards(DEFAULT_KEY_AUTH, sUID, block4, block5);
        if (w45 == 1) {
            int w7 = writeCard(DEFAULT_KEY_AUTH, sUID, decodeSecret);
            if (w7 == 1) {
                activeCard();
            }
            Log.i(TAG, "onResponse: W7 = " + w7);
        } else {
            repersoCard(ct, sUID);
            Log.e(TAG, "onResponse: W45 = " + w45);
        }
    }

    void repersoCards(String num, String aD, String sD, CardTerminal ct, byte[] sUID, String sKey) {
        tvCardNumber.setText(Method.splitCardNum(num));
        byte[] decodeAuth = Base64.decode(aD, Base64.NO_WRAP);
        String ss = ISOUtil.hexString(decodeAuth);
        Log.i(TAG, "onResponse: Auth = " + ss);

        byte[] block4 = new byte[16];
        System.arraycopy(decodeAuth, 0, block4, 0, 16);
        String ss4 = ISOUtil.hexString(block4);
        Log.i(TAG, "onResponse: Auth block 4 = " + ss4);

        byte[] block5 = new byte[16];
        System.arraycopy(decodeAuth, 16, block5, 0, 16);
        String ss5 = ISOUtil.hexString(block5);
        Log.i(TAG, "onResponse: Auth block 5 = " + ss5);

        byte[] decodeSecret = Base64.decode(sD, Base64.NO_WRAP);
        String sc = ISOUtil.hexString(decodeSecret);
        Log.i(TAG, "onResponse: Secr = " + sc);

        car.setCardUid(ct.getCardUid());
        car.setEncryptedValue(aD);
        car.setTerminalSerialNumber(DevConfig.getSN());

        byte[] decodeKey = Base64.decode(sKey, Base64.NO_WRAP);
        int w45 = rewriteCards(ISOUtil.hexString(decodeKey), sUID, block4, block5);
        if (w45 == 1) {
            int w7 = -rewriteCard(ISOUtil.hexString(decodeKey), sUID, decodeSecret);
            if (w7 == 1) {
                activeCard();
            }
            Log.i(TAG, "onResponse: W7 = " + w7);
        } else {
            Log.e(TAG, "onResponse: W45 = " + w45);
        }
    }

    public void timeoutScan() {
        Snackbar.make(parent_view, "Kartu tidak ditemukan", Snackbar.LENGTH_INDEFINITE).setDuration(3000).show();
        getCard();

    }

    public void getCard() {
        new Thread() {
            @Override
            public void run() {
                Log.i(TAG, "CardManager>>getCard>>NFC");
                piccReader.startSearchCard(0, new PiccReaderCallback() {
                    @Override
                    public void onSearchResult(int i, int i1) {
                        try {
                            Thread.sleep(400);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        Log.i(TAG, "CardManager>>getCard>>NFC>>i=" + i);
                        if (0 == i) {
//                                        listener.callback(handlePICC(i1));
                            Log.i(TAG, "CardManager>>getCard>>NFC_TYPE>>i=" + i1);
                            if (i1 == 0 || i1 == 1) {
                                initMifare();
                            } else timeoutScan();
                        } else {
                            Log.i(TAG, "onSearchResult: not found");
                            timeoutScan();
                        }
                    }
                });
            }
        }.start();
    }

    public void initMifare() {
        try {
            mifareClassic = MifareClassic.connect();

            if (mifareClassic != null) {
                byte[] bUID = mifareClassic.getUID();
                if (bUID != null) {
                    Log.i(TAG, "initMifare: UID = " + ISOUtil.hexString(bUID));
                    getCardInfo(ISOUtil.hexString(bUID), bUID);
                }

            } else {
                Log.e(TAG, "initMifare: FAILED");
                timeoutScan();
            }
        } catch (SDKException e) {
            e.printStackTrace();
        }
    }

    private void showCustomDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.dialog_event);
        dialog.setCancelable(false);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        final TextView tv_email = (TextView) dialog.findViewById(R.id.tv_email);
        final EditText et_name = (EditText) dialog.findViewById(R.id.et_name);

        et_name.setText(session.getKeyActivation());


        ((ImageButton) dialog.findViewById(R.id.bt_close)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        ((Button) dialog.findViewById(R.id.bt_save)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                session.setKeyActivation(et_name.getText().toString());
                dialog.dismiss();
            }
        });

        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }

    int writeCards(String skey, byte[] bUID, byte[] bWrite, byte[] bWrite2) {
        byte[] bKey = ISOUtil.hex2byte(skey);
        try {
            if (mifareClassic != null) {
                boolean bAuth = mifareClassic.authenticate(4, MifareClassic.KEY_TYPE_A, bKey, bUID);
                if (bAuth) {
                    Log.w(TAG, "writeCard: block " + 4 + " Write = " + ISOUtil.hexString(bWrite));
                    mifareClassic.writeBlock(4, bWrite);
                    mifareClassic.writeBlock(5, bWrite2);

                    byte[] bBlock = mifareClassic.readBlock(4);
                    byte[] bBlock2 = mifareClassic.readBlock(5);
                    if (bBlock != null) {
                        Log.i(TAG, "writeCard: Block " + 4 + " Read = " + ISOUtil.hexString(bBlock));
                        if (bBlock2 != null)
                            Log.i(TAG, "writeCard: Block " + 5 + " Read = " + ISOUtil.hexString(bBlock2));

                        return 1;
                    } else
                        return -4;
                } else {
                    // auth failed
                    return -3;
                }
            } else {
                // mifare null
                return -2;
            }
        } catch (SDKException e) {
            e.printStackTrace();
            // SDK Exception
            return -1;
        }
    }

    int rewriteCards(String skey, byte[] bUID, byte[] bWrite, byte[] bWrite2) {
        byte[] bKey = ISOUtil.hex2byte(skey);
        try {
            if (mifareClassic != null) {
                boolean bAuth = mifareClassic.authenticate(4, MifareClassic.KEY_TYPE_B, bKey, bUID);
                if (bAuth) {
                    Log.w(TAG, "rewriteCard: block " + 4 + " Write = " + ISOUtil.hexString(bWrite));
                    mifareClassic.writeBlock(4, bWrite);
                    mifareClassic.writeBlock(5, bWrite2);

                    byte[] bBlock = mifareClassic.readBlock(4);
                    byte[] bBlock2 = mifareClassic.readBlock(5);
                    if (bBlock != null) {
                        Log.i(TAG, "rewriteCard: Block " + 4 + " Read = " + ISOUtil.hexString(bBlock));
                        if (bBlock2 != null)
                            Log.i(TAG, "rewriteCard: Block " + 5 + " Read = " + ISOUtil.hexString(bBlock2));

                        return 1;
                    } else
                        return -4;
                } else {
                    // auth failed
                    return -3;
                }
            } else {
                // mifare null
                return -2;
            }
        } catch (SDKException e) {
            e.printStackTrace();
            // SDK Exception
            return -1;
        }
    }

    int writeCard(String skey, byte[] bUID, byte[] bWrite) {
        byte[] bKey = ISOUtil.hex2byte(skey);
        try {
            if (mifareClassic != null) {
                boolean bAuth = mifareClassic.authenticate(7, MifareClassic.KEY_TYPE_A, bKey, bUID);
                if (bAuth) {
                    Log.w(TAG, "writeCard: block " + 7 + " Write = " + ISOUtil.hexString(bWrite));
                    mifareClassic.writeBlock(7, bWrite);

                    byte[] bBlock = mifareClassic.readBlock(7);
                    if (bBlock != null) {
                        Log.i(TAG, "writeCard: Block " + 7 + " Read = " + ISOUtil.hexString(bBlock));

                        return 1;
                    } else
                        return -4;
                } else {
                    // auth failed
                    return -3;
                }
            } else {
                // mifare null
                return -2;
            }
        } catch (SDKException e) {
            e.printStackTrace();
            // SDK Exception
            return -1;
        }
    }

    int rewriteCard(String skey, byte[] bUID, byte[] bWrite) {
        byte[] bKey = ISOUtil.hex2byte(skey);
        try {
            if (mifareClassic != null) {
                boolean bAuth = mifareClassic.authenticate(7, MifareClassic.KEY_TYPE_B, bKey, bUID);
                if (bAuth) {
                    Log.w(TAG, "rewriteCard: block " + 7 + " Write = " + ISOUtil.hexString(bWrite));
                    mifareClassic.writeBlock(7, bWrite);

                    byte[] bBlock = mifareClassic.readBlock(7);
                    if (bBlock != null) {
                        Log.i(TAG, "rewriteCard: Block " + 7 + " Read = " + ISOUtil.hexString(bBlock));

                        return 1;
                    } else
                        return -4;
                } else {
                    // auth failed
                    return -3;
                }
            } else {
                // mifare null
                return -2;
            }
        } catch (SDKException e) {
            e.printStackTrace();
            // SDK Exception
            return -1;
        }
    }
}
