package com.cleva.kampoengradja;

import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.SystemClock;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.cleva.kampoengradja.model.AuthCardResp;
import com.cleva.kampoengradja.model.CardIdentifierId;
import com.cleva.kampoengradja.model.CardInfoDataJava;
import com.cleva.kampoengradja.model.CardTerminal;
import com.cleva.kampoengradja.model.TopUpReq;
import com.cleva.kampoengradja.model.TopUpResp;
import com.cleva.kampoengradja.utils.Constants;
import com.cleva.kampoengradja.utils.ISOUtil;
import com.cleva.kampoengradja.utils.Method;
import com.pos.device.SDKException;
import com.pos.device.config.DevConfig;
import com.pos.device.picc.EmvContactlessCard;
import com.pos.device.picc.MifareClassic;
import com.pos.device.picc.PiccReader;
import com.pos.device.picc.PiccReaderCallback;
import com.pos.device.printer.PrintCanvas;
import com.pos.device.printer.PrintTask;
import com.pos.device.printer.Printer;
import com.pos.device.printer.PrinterCallback;

import java.io.IOException;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.CountDownLatch;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.cleva.kampoengradja.MyApp.readCards;
import static com.cleva.kampoengradja.MyApp.restApi;
import static com.cleva.kampoengradja.MyApp.session;
import static com.cleva.kampoengradja.utils.Constants.AUTH_0;
import static com.cleva.kampoengradja.utils.Constants.AUTH_M1;
import static com.cleva.kampoengradja.utils.Constants.AUTH_M2;
import static com.cleva.kampoengradja.utils.Constants.AUTH_M3;
import static com.cleva.kampoengradja.utils.Constants.AUTH_M4;
import static com.cleva.kampoengradja.utils.Method.getFormatIDR;
import static com.cleva.kampoengradja.utils.Method.loadImg;
import static com.cleva.kampoengradja.utils.Method.printLine;
import static com.cleva.kampoengradja.utils.Method.setFontStyle;
import static com.cleva.kampoengradja.utils.Method.showCustomDialogs;
import static com.cleva.kampoengradja.utils.Method.tapImg;

public class TopupActivity extends AppCompatActivity {

    private static final String TAG = "TopupActivity";
    TextView etAmount, tvAmount, tvBonus, tvAmountAdt, tvSaldo;
    String sAmount = "0", sSaldoFake = "0", sBayar = "", sBalance = "", sCNum = "";
    int itopadt = 0, ibonus = 0;

    View llTopup, llScanCard, parent_view, llSucces, ll10, ll20, ll30, ll40, ll50, ll100, ll200;
    ImageView imgLoad;
    AppCompatButton btnDel, btnTopup, btnBack, btnReprint;

    PiccReader piccReader;
    private EmvContactlessCard emvContactlessCard = null;

    MifareClassic mifareClassic = null;
    private Printer printer = null;
    private PrintTask printTask = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_topup);
        initView();
        setTitle("Topup");
        piccReader = PiccReader.getInstance();

    }

    @Override
    protected void onPause() {
        super.onPause();
        if (piccReader != null) {
            piccReader.stopSearchCard();
            try {
                piccReader.release();
            } catch (SDKException e) {
                e.printStackTrace();
            }
        }
    }

    public void initView() {

        etAmount = findViewById(R.id.etAmount);
        llTopup = findViewById(R.id.llTopup);
        llScanCard = findViewById(R.id.llScanCard);
        imgLoad = findViewById(R.id.imgload);
        llSucces = findViewById(R.id.cvTappedSuccess);

        Glide.with(this)
                .load(R.drawable.tap)
                .into(imgLoad);

        btnDel = findViewById(R.id.btnDel);
        btnDel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doPads(v);
            }
        });

        btnTopup = findViewById(R.id.btnTopUp);
        btnTopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doPads(v);
            }
        });

        btnBack = findViewById(R.id.btnBack);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doTopAgain(v);
            }
        });

        btnReprint = findViewById(R.id.btnReprint);
        btnReprint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                prints(sBayar, itopadt, ibonus, sBalance, false, true);
            }
        });

        tvAmount = findViewById(R.id.tvAmount);
        tvAmountAdt = findViewById(R.id.tvAmountAdt);
        tvBonus = findViewById(R.id.tvBonus);
        tvSaldo = findViewById(R.id.tvSaldo);
        parent_view = findViewById(R.id.parent_view);

        ll10 = findViewById(R.id.ll10);
        ll20 = findViewById(R.id.ll20);
        ll30 = findViewById(R.id.ll30);
        ll40 = findViewById(R.id.ll40);
        ll50 = findViewById(R.id.ll50);
        ll100 = findViewById(R.id.ll100);
        ll200 = findViewById(R.id.ll200);
    }

    public void doPay(View v){
        if (v.getTag().equals("10"))
            etAmount.setText("10.000");
        if (v.getTag().equals("20"))
            etAmount.setText("20.000");
        if (v.getTag().equals("30"))
            etAmount.setText("30.000");
        if (v.getTag().equals("40"))
            etAmount.setText("40.000");
        if (v.getTag().equals("50"))
            etAmount.setText("50.000");
        if (v.getTag().equals("100"))
            etAmount.setText("100.000");
        if (v.getTag().equals("200"))
            etAmount.setText("200.000");
        llTopup.setVisibility(View.GONE);
        llScanCard.setVisibility(View.VISIBLE);
        getCard();
    }

    public void doPad(View view) {
        TextView tv = (TextView) view;

        sSaldoFake = sAmount;
        sAmount = sAmount + tv.getText().toString();

        int ma = Integer.parseInt(getFormatIDR(sAmount.replace(".", "")).replace(".", ""));
        Log.i(TAG, "doPad: " + ma);

        if (ma < 500001) {
            etAmount.setText(getFormatIDR(sAmount.replace(".", "")));

        } else {
            Snackbar.make(parent_view, "Maksimum Topup 500.000", Snackbar.LENGTH_INDEFINITE).setDuration(3000).show();
            sAmount = sSaldoFake;

        }
    }

    public void doPads(View v) {
        AppCompatButton tv = (AppCompatButton) v;
        if (tv.getText().equals("Del")) {
            Log.i(TAG, "doPads: [" + sAmount + "]");
            if (sAmount.length() > 0) {
                sAmount = sAmount.substring(0, sAmount.length() - 1);
                if (sAmount.length() == 0)
                    sAmount = "0";
            } else {
                sAmount = "0";
            }

            etAmount.setText(getFormatIDR(sAmount.replace(".", "")));

        } else if (tv.getText().equals("Top Up")) {
            Log.i(TAG, "doPads: " + getFormatIDR(sAmount).replace(".", ""));
            int s = Integer.parseInt(getFormatIDR(sAmount).replace(".", ""));

            if (s > 0) {
                llTopup.setVisibility(View.GONE);
                llScanCard.setVisibility(View.VISIBLE);
                getCard();

            } else {
                Snackbar.make(parent_view, "Tidak boleh nol", Snackbar.LENGTH_INDEFINITE).setDuration(3000).show();

            }

        }
    }

    public void doTopAgain(View v) {
        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        llTopup.setVisibility(View.VISIBLE);
                        llSucces.setVisibility(View.GONE);
                    }
                },
                1);
    }

    void getCardInfo(String a) {
        CardTerminal ct = new CardTerminal();
        ct.setCardUid(a);
        ct.setTerminalSerialNumber(DevConfig.getSN());
        restApi.cardInfo(a, session.getAccessToken()).enqueue(new Callback<CardInfoDataJava>() {
            @Override
            public void onResponse(Call<CardInfoDataJava> call, Response<CardInfoDataJava> response) {
                if (response.code() == 200) {
                    if (response.body() != null) {
                        // HERE
                        sCNum = response.body().getCardIdentifierId();
                        doTopUp(a);
                    } else {
                        try {
                            Method.showErorRest(parent_view, response.errorBody().string());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        getCard();
                    }
                } else {
                    try {
                        Method.showErorRest(parent_view, response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    getCard();
                }
            }

            @Override
            public void onFailure(Call<CardInfoDataJava> call, Throwable t) {
                t.printStackTrace();
                showCustomDialogs(TopupActivity.this);
            }
        });
    }

    void doTopUp(String a) {
        TopUpReq topUpReq = new TopUpReq();
        topUpReq.setCardUid(a);
        topUpReq.setTerminalSerialNumber(DevConfig.getSN());
        topUpReq.setTopUpAmount(Integer.parseInt(etAmount.getText().toString().replace(".", "")));
        restApi.topUpRequest(topUpReq, session.getAccessToken()).enqueue(new Callback<TopUpResp>() {
            @Override
            public void onResponse(Call<TopUpResp> call, Response<TopUpResp> response) {
                if (response.code() == 200) {
                    if (response.body() != null) {

                        llScanCard.setVisibility(View.GONE);
                        llSucces.setVisibility(View.VISIBLE);
//                        int sum = Integer.parseInt(sSaldoFake.replace(".","")) +
//                                Integer.parseInt(etAmount.getText().toString().replace(".",""));
//                        sSaldoFake = Method.getFormatIDR(sum);
                        tvAmount.setText("Nilai Top Up: " + Method.getFormatIDR(response.body().getTopUpAmount()));
                        tvSaldo.setText("Saldo: " + Method.getFormatIDR(response.body().getCurrentBalance()));
                        etAmount.setText("0");
                        sAmount = "0";

                        if (response.body().getTopUpAdditionGiven() > 0) {
                            tvAmountAdt.setVisibility(View.VISIBLE);
                            tvAmountAdt.setText("Top Up Tambahan: " + Method.getFormatIDR(response.body().getTopUpAdditionGiven()));
                        } else {
                            tvAmountAdt.setVisibility(View.GONE);
                        }
                        if (response.body().getBonusGiven() > 0) {
                            tvBonus.setVisibility(View.VISIBLE);
                            tvBonus.setText("Bonus: " + Method.getFormatIDR(response.body().getBonusGiven()));
                        } else {
                            tvBonus.setVisibility(View.GONE);

                        }

                        sBayar = String.valueOf(response.body().getTopUpAmount());
                        itopadt = response.body().getTopUpAdditionGiven();
                        ibonus = response.body().getBonusGiven();
                        sBalance = String.valueOf(response.body().getCurrentBalance());
                        prints(sBayar, itopadt, ibonus, sBalance, false, false);
                    }
                } else {

                    llTopup.setVisibility(View.VISIBLE);
                    llScanCard.setVisibility(View.GONE);

                    try {
                        Method.showErorRest(parent_view, response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<TopUpResp> call, Throwable t) {
                t.printStackTrace();
                showCustomDialogs(TopupActivity.this);
            }
        });
    }

    public void timeoutScan() {

//        llError.setVisibility(View.VISIBLE);
        Snackbar.make(parent_view, "Kartu tidak ditemukan", Snackbar.LENGTH_INDEFINITE).setDuration(3000).show();
        getCard();

    }

    public void getCard() {
        tapImg(this, imgLoad, (findViewById(R.id.tvMsg)));
        new Thread() {
            @Override
            public void run() {
                Log.i(TAG, "CardManager>>getCard>>NFC");
                piccReader.startSearchCard(0, new PiccReaderCallback() {
                    @Override
                    public void onSearchResult(int i, int i1) {
                        try {
                            Thread.sleep(400);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        Log.i(TAG, "CardManager>>getCard>>NFC>>i=" + i);
                        if (0 == i) {
//                                        listener.callback(handlePICC(i1));
                            Log.i(TAG, "CardManager>>getCard>>NFC_TYPE>>i=" + i1);
                            if (i1 == 0 || i1 == 1) {
                                initMifare();
                            } else {
                                Log.e(TAG, "onSearchResult: Kartu no ident");
                            }
                        } else {
                            Log.i(TAG, "onSearchResult: not found");
                            timeoutScan();
                        }
                    }
                });
            }
        }.start();
    }

//    public void initEMV() {
////        try {
////            emvContactlessCard = EmvContactlessCard.connect();
////            if (emvContactlessCard != null) {
////                Log.i(TAG, "initEMV: " + emvContactlessCard);
////                byte[] rawData = emvContactlessCard.getUID();
////                // FF B0 00 04 10
////                if (rawData != null) {
////                    Log.i(TAG, "initEMV: UID = " + ISOUtil.hexString(rawData));
////                    getCardInfo(ISOUtil.hexString(rawData));
////                }
////
////            } else {
////                Log.e(TAG, "initEMV: FAILED");
////                timeoutScan();
////            }
////        } catch (SDKException e) {
////            e.printStackTrace();
////        }
////    }

    public void initMifare() {
        try {
            mifareClassic = MifareClassic.connect();
            loadImg(this, imgLoad, (findViewById(R.id.tvMsg)));

            if (mifareClassic != null) {
                byte[] bUID = mifareClassic.getUID();
                if (bUID != null) {
                    Log.i(TAG, "initMifare: UID = " + ISOUtil.hexString(bUID));
                    getCardAuth(ISOUtil.hexString(bUID), bUID);
                }

            } else {
                Log.e(TAG, "initMifare: FAILED");
                timeoutScan();
            }
        } catch (SDKException e) {
            e.printStackTrace();
        }
    }

    void getCardAuth(String a, byte[] bUid){
        CardTerminal ct = new CardTerminal();
        ct.setCardUid(a);
        ct.setTerminalSerialNumber(DevConfig.getSN());

        restApi.authCard(ct, session.getAccessToken()).enqueue(new Callback<AuthCardResp>() {
            @Override
            public void onResponse(Call<AuthCardResp> call, Response<AuthCardResp> response) {
                if (response.isSuccessful()){
                    if (response.body() != null){
                        int ares = readCards(mifareClassic, response.body().getAuthKey(), bUid, response.body().getAuthData());
                        Log.i(TAG, "onResponse: ares = "+ ares);
                        switch (ares) {
                            case 1:
                                getCardInfo(ct.getCardUid());
                                break;
                            case 0:
                                showConfirmDialog(AUTH_0);
                                break;
                            case -4:
                                showConfirmDialog(AUTH_M4);
                                break;
                            case -3:
                                showConfirmDialog(AUTH_M3);
                                break;
                            case -2:
                                showConfirmDialog(AUTH_M2);
                                break;
                            case -1:
                                showConfirmDialog(AUTH_M1);
                                break;
                        }
                    } else {
                        try {
                            Method.showErorRest(parent_view, response.errorBody().string());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        getCard();
                    }
                } else {
                    try {
                        Method.showErorRest(parent_view, response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    getCard();
                }
            }

            @Override
            public void onFailure(Call<AuthCardResp> call, Throwable t) {
                t.printStackTrace();
                showCustomDialogs(TopupActivity.this);

            }
        });

    }


    public void prints(final String a, final int topadt, final int ibonus, final String b, final boolean isprev, final boolean isreprint) {

        new Thread() {
            @Override
            public void run() {
                int az = printDetails(a, topadt, ibonus, b, isprev, isreprint);
                Log.i(TAG, "run: " + az);
            }
        }.start();
    }

    public int printDetails(String a, final int topadt, final int ibonus, String b, boolean isPreview, boolean isReprint) {
        this.printTask = new PrintTask();
        this.printTask.setGray(130);
        int ret = -1;
        printer = Printer.getInstance();
        if (printer == null) {
            ret = 112;
        } else {

            SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yy, HH:mm", new Locale("ID"));
            String currentDateandTime = sdf.format(new Date());

            Bitmap icon = BitmapFactory.decodeResource(this.getResources(),
                    R.drawable.kampoeng_log);

            Bitmap scaled = Bitmap.createScaledBitmap(icon, 320, 120, true);

            final PrintCanvas canvas = new PrintCanvas();
            Paint paint = new Paint();
            setFontStyle(this, paint, 2, true);
            if (isReprint)
                canvas.drawText(Constants.REPRINT, paint);
            canvas.drawText(" ", paint);
            canvas.drawBitmap(scaled, paint);
            setFontStyle(this, paint, 2, true);
            canvas.drawText(" ", paint);
            setFontStyle(this, paint, 2, true);
            canvas.drawText("------ Topup  Terminal ------", paint);
            canvas.drawText(currentDateandTime + "   " + DevConfig.getSN(), paint);
            printLine(this, paint, canvas);
            setFontStyle(this, paint, 2, true);
            canvas.drawText("No. Card  " + Method.splitCardNum(sCNum).trim(), paint);
            canvas.drawText(" ", paint);
            int num = 1;
            for (int i = 0; i < num; i++) {
                setFontStyle(this, paint, 2, true);
                canvas.drawText(Constants.TRANS_TYPE + "Top Up", paint);
                canvas.drawText(Constants.AMOUNT + Constants.RMB + Method.getFormatIDR(a), paint);
                if (itopadt > 0)
                    canvas.drawText(Constants.AMOUNTBONUS + Constants.RMB + Method.getFormatIDR(topadt), paint);
                if (ibonus > 0)
                    canvas.drawText(Constants.Bonus + Constants.RMB + Method.getFormatIDR(ibonus), paint);
                canvas.drawText(Constants.EC_AMOUNT + Constants.RMB + Method.getFormatIDR(b), paint);
//                printLine(this, paint, canvas);
            }
            setFontStyle(this, paint, 2, true);
            canvas.drawText(" ", paint);
            canvas.drawText("------- Terima  Kasih -------", paint);
            canvas.drawText(" ", paint);

            if (isPreview) {
                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {

                        // Stuff that updates the UI
                        imgLoad.setImageBitmap(canvas.getBitmap());

                    }
                });
            } else {
                ret = printData(canvas);
            }

            if (printer != null) {
                printer = null;
            }
        }
        return ret;
    }

    private int printData(PrintCanvas pCanvas) {
        final CountDownLatch latch = new CountDownLatch(1);
        printer = Printer.getInstance();
        int ret = printer.getStatus();
        if (Printer.PRINTER_STATUS_PAPER_LACK == ret) {
            long start = SystemClock.uptimeMillis();
            while (true) {
                if (SystemClock.uptimeMillis() - start > 60 * 1000) {
                    ret = Printer.PRINTER_STATUS_PAPER_LACK;
                    break;
                }
                if (printer.getStatus() == Printer.PRINTER_OK) {
                    ret = Printer.PRINTER_OK;
                    break;
                } else {
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                    }
                }
            }
        }
        if (ret == Printer.PRINTER_OK) {
            printTask.setPrintCanvas(pCanvas);
            printer.startPrint(printTask, new PrinterCallback() {
                @Override
                public void onResult(int i, PrintTask printTask) {
                    latch.countDown();
                }
            });
            try {
                latch.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return ret;
    }

    private void showConfirmDialog(String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Kampoeng Radja");
        builder.setMessage(msg);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                getCard();
            }
        });
        builder.show();
    }

}
