package com.cleva.kampoengradja;

import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.SystemClock;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cleva.kampoengradja.model.AuthCardResp;
import com.cleva.kampoengradja.model.CardIdentifierId;
import com.cleva.kampoengradja.model.CardInfoDataJava;
import com.cleva.kampoengradja.model.CardTerminal;
import com.cleva.kampoengradja.model.RefundDetailReq;
import com.cleva.kampoengradja.model.RefundDetailResp;
import com.cleva.kampoengradja.model.RefundResp;
import com.cleva.kampoengradja.utils.Constants;
import com.cleva.kampoengradja.utils.ISOUtil;
import com.cleva.kampoengradja.utils.Method;
import com.pos.device.SDKException;
import com.pos.device.config.DevConfig;
import com.pos.device.picc.EmvContactlessCard;
import com.pos.device.picc.MifareClassic;
import com.pos.device.picc.PiccReader;
import com.pos.device.picc.PiccReaderCallback;
import com.pos.device.printer.PrintCanvas;
import com.pos.device.printer.PrintTask;
import com.pos.device.printer.Printer;
import com.pos.device.printer.PrinterCallback;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Random;
import java.util.concurrent.CountDownLatch;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.cleva.kampoengradja.MyApp.readCards;
import static com.cleva.kampoengradja.MyApp.restApi;
import static com.cleva.kampoengradja.MyApp.session;
import static com.cleva.kampoengradja.utils.Constants.AUTH_0;
import static com.cleva.kampoengradja.utils.Constants.AUTH_M1;
import static com.cleva.kampoengradja.utils.Constants.AUTH_M2;
import static com.cleva.kampoengradja.utils.Constants.AUTH_M3;
import static com.cleva.kampoengradja.utils.Constants.AUTH_M4;
import static com.cleva.kampoengradja.utils.Method.printLine;
import static com.cleva.kampoengradja.utils.Method.setFontStyle;
import static com.cleva.kampoengradja.utils.Method.showCustomDialogs;

public class RefundActivity extends AppCompatActivity {

    private static final String TAG = "RefundActivity";

    View parent_view;

    ImageView imgLoad;

    CardView cvTapCard, cvTapped, cvTapSuccess;

    AppCompatButton btnRefund, btnRefundAgain, btnReprint;
    TextView tvRemain, tvDeposit, tvTotalRefund;

    int back = 0;

    PiccReader piccReader;

    MifareClassic mifareClassic = null;

    RefundDetailReq rdr = new RefundDetailReq();

    private Printer printer = null;
    private PrintTask printTask = null;

    String sCNum = "", sRemain = "", sDeposit = "", sRefund = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_refund);
        initView();
        setTitle("Refund");

        piccReader = PiccReader.getInstance();

        getCard();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (piccReader != null){
            piccReader.stopSearchCard();
            try {
                piccReader.release();
            } catch (SDKException e) {
                e.printStackTrace();
            }
        }
    }

    public void initView(){
        imgLoad = findViewById(R.id.imgloads);

        Glide.with(this)
                .load(R.drawable.tap)
                .into(imgLoad);

        cvTapCard = findViewById(R.id.cvTapCard);
        cvTapCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//
//                cvTapCard.setVisibility(View.GONE);
//                cvTapped.setVisibility(View.VISIBLE);
//                back = 1;
            }
        });

        cvTapped = findViewById(R.id.cvTapped);
        cvTapSuccess= findViewById(R.id.cvTappedSuccess);

        btnRefund = findViewById(R.id.btnRefund);
        btnRefund.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doRefund();
            }
        });

        btnRefundAgain = findViewById(R.id.btnRefundAgain);
        btnRefundAgain.setOnClickListener(v -> {
            cvTapCard.setVisibility(View.VISIBLE);
            cvTapSuccess.setVisibility(View.GONE);
            back = 0;
            getCard();
        });

        btnReprint = findViewById(R.id.btnReprint);
        btnReprint.setOnClickListener(v->{
            prints(sRemain, sDeposit, sRefund, false, true);
        });

        parent_view = findViewById(R.id.parent_view);
        tvRemain = findViewById(R.id.tvRemainSaldo);
        tvDeposit = findViewById(R.id.tvDeposit);
        tvTotalRefund = findViewById(R.id.tvTotalRefund);
    }

    @Override
    public void onBackPressed() {
        if (back == 1){
            cvTapCard.setVisibility(View.VISIBLE);
            cvTapped.setVisibility(View.GONE);
            back = 0;
            getCard();
        } else if (back == 2){
            cvTapCard.setVisibility(View.VISIBLE);
            cvTapSuccess.setVisibility(View.GONE);
            back = 0;
            getCard();
        } else
            super.onBackPressed();
    }

    void getCardAuth(String a, byte[] bUid) {
        CardTerminal ct = new CardTerminal();
        ct.setCardUid(a);
        ct.setTerminalSerialNumber(DevConfig.getSN());

        restApi.authCard(ct, session.getAccessToken()).enqueue(new Callback<AuthCardResp>() {
            @Override
            public void onResponse(Call<AuthCardResp> call, Response<AuthCardResp> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        int ares = readCards(mifareClassic, response.body().getAuthKey(), bUid, response.body().getAuthData());
                        Log.i(TAG, "onResponse: ares = " + ares);
                        switch (ares) {
                            case 1:
                                getCardInfo(ct.getCardUid());
                                break;
                            case 0:
                                showConfirmDialog(AUTH_0);
                                break;
                            case -4:
                                showConfirmDialog(AUTH_M4);
                                break;
                            case -3:
                                showConfirmDialog(AUTH_M3);
                                break;
                            case -2:
                                showConfirmDialog(AUTH_M2);
                                break;
                            case -1:
                                showConfirmDialog(AUTH_M1);
                                break;
                        }
                    } else {
                        try {
                            Method.showErorRest(parent_view, response.errorBody().string());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        getCard();
                    }
                } else {
                    try {
                        Method.showErorRest(parent_view, response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    getCard();
                }
            }

            @Override
            public void onFailure(Call<AuthCardResp> call, Throwable t) {
                t.printStackTrace();
                showCustomDialogs(RefundActivity.this);

            }
        });

    }

    void getCardInfo(String cardUID){
        rdr.setCardUid(cardUID);
        rdr.setTerminalSerialNumber(DevConfig.getSN());
        restApi.cardInfo(cardUID, session.getAccessToken()).enqueue(new Callback<CardInfoDataJava>() {
            @Override
            public void onResponse(Call<CardInfoDataJava> call, Response<CardInfoDataJava> response) {
                if (response.code() == 200) {
                    if (response.body() != null) {
                        // HERE
                        sCNum = response.body().getCardIdentifierId();
                        refundDetail();
                    } else {
                        try {
                            Method.showErorRest(parent_view, response.errorBody().string());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        getCard();
                    }
                } else {
                    try {
                        Method.showErorRest(parent_view, response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    getCard();
                }
            }

            @Override
            public void onFailure(Call<CardInfoDataJava> call, Throwable t) {
                t.printStackTrace();
                showCustomDialogs(RefundActivity.this);
            }
        });
    }

    void refundDetail(){
        restApi.refundDetail(rdr,session.getAccessToken()).enqueue(new Callback<RefundDetailResp>() {
            @Override
            public void onResponse(Call<RefundDetailResp> call, Response<RefundDetailResp> response) {
                if (response.code() == 200 ){
                    if (response.body() != null){

                        tvRemain.setText(Method.getFormatIDR(response.body().getRemainingBalance()));
                        tvDeposit.setText(Method.getFormatIDR(response.body().getDeposit()));
                        tvTotalRefund.setText(Method.getFormatIDR(response.body().getRefundAmount()));

                        cvTapCard.setVisibility(View.GONE);
                        cvTapped.setVisibility(View.VISIBLE);
                        back = 1;
                    }
                }
            }

            @Override
            public void onFailure(Call<RefundDetailResp> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    void doRefund(){
        restApi.refund(rdr,session.getAccessToken()).enqueue(new Callback<RefundResp>() {
            @Override
            public void onResponse(Call<RefundResp> call, Response<RefundResp> response) {
                if (response.code() == 200 ) {
                    if (response.body() != null) {
                        Log.i(TAG, "onResponse: refund "+response.body().getTransactionId());
                        cvTapSuccess.setVisibility(View.VISIBLE);
                        cvTapped.setVisibility(View.GONE);
                        back = 1;

                        tvRemain.setText(Method.getFormatIDR(response.body().getRemainingBalance()));
                        tvDeposit.setText(Method.getFormatIDR(response.body().getDeposit()));
                        tvTotalRefund.setText(Method.getFormatIDR(response.body().getRefundAmount()));
                        sRemain = String.valueOf(response.body().getRemainingBalance());
                        sDeposit = String.valueOf(response.body().getDeposit());
                        sRefund = String.valueOf(response.body().getRefundAmount());
                        prints(sRemain,sDeposit,sRefund, false, false);
                    }
                }
            }

            @Override
            public void onFailure(Call<RefundResp> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    public void timeoutScan(){

//        llError.setVisibility(View.VISIBLE);
        Snackbar.make(parent_view, "Kartu tidak ditemukan", Snackbar.LENGTH_INDEFINITE).setDuration(3000).show();
        getCard();

    }

    public void getCard() {
        new Thread() {
            @Override
            public void run() {
                Log.i(TAG, "CardManager>>getCard>>NFC");
                piccReader.startSearchCard(0, new PiccReaderCallback() {
                    @Override
                    public void onSearchResult(int i, int i1) {
                        try {
                            Thread.sleep(400);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        Log.i(TAG, "CardManager>>getCard>>NFC>>i=" + i);
                        if (0 == i) {
//                                        listener.callback(handlePICC(i1));
                            Log.i(TAG, "CardManager>>getCard>>NFC_TYPE>>i=" + i1);
                            if (i1 == 0 || i1 == 1) {
                                initMifare();
                            } else
                                timeoutScan();
                        } else {
                            Log.i(TAG, "onSearchResult: not found");
                            timeoutScan();
                        }
                    }
                });
            }
        }.start();
    }

    public void initMifare() {
        try {
            mifareClassic = MifareClassic.connect();

            if (mifareClassic != null) {
                byte[] bUID = mifareClassic.getUID();
                if (bUID != null) {
                    Log.i(TAG, "initMifare: UID = " + ISOUtil.hexString(bUID));
                    getCardAuth(ISOUtil.hexString(bUID), bUID);
                }

            } else {
                Log.e(TAG, "initMifare: FAILED");
                timeoutScan();
            }
        } catch (SDKException e) {
            e.printStackTrace();
        }
    }


    public void prints(final String a, final String b, final String c, final boolean isprev, final boolean isreprint) {
        new Thread() {
            @Override
            public void run() {
                int az = printDetails(a, b, c, isprev, isreprint);
                Log.i(TAG, "run: " + a);
            }
        }.start();
    }

    public int printDetails(String a, String b, String c, boolean isPreview, final boolean isReprint) {
        this.printTask = new PrintTask();
        this.printTask.setGray(130);
        int ret = -1;
        printer = Printer.getInstance();
        if (printer == null) {
            ret = 112;
        } else {

            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yy, HH:mm", new Locale("ID"));
            String currentDateandTime = sdf.format(new Date());

            Bitmap icon = BitmapFactory.decodeResource(this.getResources(),
                    R.drawable.kampoeng_log);

            Bitmap scaled = Bitmap.createScaledBitmap(icon, 320, 120, true);

            final PrintCanvas canvas = new PrintCanvas();
            Paint paint = new Paint();
            setFontStyle(this, paint, 2, true);
            if (isReprint)
                canvas.drawText(Constants.REPRINT, paint);
            canvas.drawText(" ", paint);
            canvas.drawBitmap(scaled, paint);
            setFontStyle(this, paint, 2, true);
            canvas.drawText(" ", paint);
            setFontStyle(this, paint, 2, true);
            canvas.drawText("------ Refund Terminal ------", paint);
            canvas.drawText(Method.printSpace(currentDateandTime+" "+DevConfig.getSN(), currentDateandTime, DevConfig.getSN()), paint);
            printLine(this, paint, canvas);
            setFontStyle(this, paint, 2, true);
            canvas.drawText("No. Card  "+Method.splitCardNum(sCNum).trim(), paint);
            canvas.drawText(" ", paint);
            int num = 1;
            for (int i = 0; i < num; i++) {
                setFontStyle(this, paint, 2, true);
                canvas.drawText(Constants.TRANS_TYPE + "Refund", paint);
                canvas.drawText(Constants.SISA_SALDO + Constants.RMB + Method.getFormatIDR(a), paint);
                canvas.drawText(Constants.Jaminan+ Constants.RMB + Method.getFormatIDR(b), paint);
                canvas.drawText(Constants.TOTAL_Refund+ Constants.RMB + Method.getFormatIDR(c), paint);
                printLine(this, paint, canvas);
            }

            if (isPreview) {
                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {

                        // Stuff that updates the UI
                        imgLoad.setImageBitmap(canvas.getBitmap());

                    }
                });
            } else {
                ret = printData(canvas);
            }

            if (printer != null) {
                printer = null;
            }
        }
        return ret;
    }

    private int printData(PrintCanvas pCanvas) {
        final CountDownLatch latch = new CountDownLatch(1);
        printer = Printer.getInstance();
        int ret = printer.getStatus();
        if (Printer.PRINTER_STATUS_PAPER_LACK == ret) {
            long start = SystemClock.uptimeMillis();
            while (true) {
                if (SystemClock.uptimeMillis() - start > 60 * 1000) {
                    ret = Printer.PRINTER_STATUS_PAPER_LACK;
                    break;
                }
                if (printer.getStatus() == Printer.PRINTER_OK) {
                    ret = Printer.PRINTER_OK;
                    break;
                } else {
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                    }
                }
            }
        }
        if (ret == Printer.PRINTER_OK) {
            printTask.setPrintCanvas(pCanvas);
            printer.startPrint(printTask, new PrinterCallback() {
                @Override
                public void onResult(int i, PrintTask printTask) {
                    latch.countDown();
                }
            });
            try {
                latch.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return ret;
    }

    private void showConfirmDialog(String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Kampoeng Radja");
        builder.setMessage(msg);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                getCard();
            }
        });
        builder.show();
    }
}
