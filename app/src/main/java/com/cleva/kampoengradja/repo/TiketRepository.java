package com.cleva.kampoengradja.repo;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;
import android.util.Log;

import com.cleva.kampoengradja.database.TiketDao;
import com.cleva.kampoengradja.database.TiketDatabase;
import com.cleva.kampoengradja.model.TiketParkirResp;

import java.util.List;
import java.util.concurrent.ExecutionException;

public class TiketRepository {
    private static final String TAG = "TiketRepository";
    private TiketDao noteDao;
    private LiveData<List<TiketParkirResp>> allTiketParkirResps;
    List<TiketParkirResp> allTiketById;

    public TiketRepository(Application application) {
        TiketDatabase database = TiketDatabase.getInstance(application);
        noteDao = database.noteDao();
        allTiketParkirResps = noteDao.getAllTiketParkirResps();
        Log.i(TAG, "TiketRepository: " + allTiketParkirResps.toString());
    }

    public void insert(TiketParkirResp note) {
        new InsertTiketParkirRespAsyncTask(noteDao).execute(note);
    }

    public void update(TiketParkirResp note) {
        new UpdateTiketParkirRespAsyncTask(noteDao).execute(note);
    }

    public void delete(TiketParkirResp note) {
        new DeleteTiketParkirRespAsyncTask(noteDao).execute(note);
    }

    public void deleteAllTiketParkirResps() {
        new DeleteAllTiketParkirRespAsyncTask(noteDao).execute();
    }

    public LiveData<List<TiketParkirResp>> getAllTiketParkirResps() {
        return allTiketParkirResps;
    }

    public List<TiketParkirResp> getAllTiketById(int itemId) {

        try {
            return new GetTiketByIdAsyncTask(noteDao).execute(itemId).get();
        } catch (ExecutionException e) {
            e.printStackTrace();
            return null;
        } catch (InterruptedException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void getTiketById(int itemId) {
        new GetTiketByIdAsyncTask(noteDao).execute(itemId);
    }

    private static class InsertTiketParkirRespAsyncTask extends AsyncTask<TiketParkirResp, Void, Void>{

        // DATABASE OPERATION
        private TiketDao noteDao;

        private InsertTiketParkirRespAsyncTask(TiketDao noteDao){
            this.noteDao = noteDao;
        }

        @Override
        protected Void doInBackground(TiketParkirResp... cTiketParkirResps) {
            noteDao.insert(cTiketParkirResps[0]);
            return null;
        }
    }
    private static class UpdateTiketParkirRespAsyncTask extends AsyncTask<TiketParkirResp, Void, Void>{

        // DATABASE OPERATION
        private TiketDao noteDao;

        private UpdateTiketParkirRespAsyncTask(TiketDao noteDao){
            this.noteDao = noteDao;
        }

        @Override
        protected Void doInBackground(TiketParkirResp... cTiketParkirResps) {
            noteDao.update(cTiketParkirResps[0]);
            return null;
        }
    }
    private static class DeleteTiketParkirRespAsyncTask extends AsyncTask<TiketParkirResp, Void, Void>{

        // DATABASE OPERATION
        private TiketDao noteDao;

        private DeleteTiketParkirRespAsyncTask(TiketDao noteDao){
            this.noteDao = noteDao;
        }

        @Override
        protected Void doInBackground(TiketParkirResp... cTiketParkirResps) {
            noteDao.delete(cTiketParkirResps[0]);
            return null;
        }
    }
    private static class DeleteAllTiketParkirRespAsyncTask extends AsyncTask<Void, Void, Void>{

        // DATABASE OPERATION
        private TiketDao noteDao;

        private DeleteAllTiketParkirRespAsyncTask(TiketDao noteDao){
            this.noteDao = noteDao;
        }

        @Override
        protected Void doInBackground(Void... cTiketParkirResps) {
            noteDao.deleteAllTiketParkirResps();
            return null;
        }
    }

    private static class GetTiketByIdAsyncTask extends AsyncTask<Integer, Void, List<TiketParkirResp>>{

        // DATABASE OPERATION
        private TiketDao noteDao;

        private GetTiketByIdAsyncTask(TiketDao noteDao){
            this.noteDao = noteDao;
        }

        @Override
        protected List<TiketParkirResp> doInBackground(Integer... pos) {
            return noteDao.getTiketById(pos[0]);
        }

        @Override
        protected void onPostExecute(List<TiketParkirResp> tiketParkirResps) {
            super.onPostExecute(tiketParkirResps);
        }
    }
}
