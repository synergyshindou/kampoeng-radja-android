package com.cleva.kampoengradja.repo;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;
import android.util.Log;

import com.cleva.kampoengradja.database.OutletDao;
import com.cleva.kampoengradja.database.TiketDatabase;
import com.cleva.kampoengradja.model.OutletItem;

import java.util.List;
import java.util.concurrent.ExecutionException;

public class OutletRepository {
    private static final String TAG = "OutletRepository";
    private OutletDao noteDao;
    private LiveData<List<OutletItem>> allOutletItems;
    List<OutletItem> allTiketById;

    public OutletRepository(Application application) {
        TiketDatabase database = TiketDatabase.getInstance(application);
        noteDao = database.outletDao();
        allOutletItems = noteDao.getAllOutletItems();
        Log.i(TAG, "OutletRepository: " + allOutletItems.toString());
    }

    public void insert(OutletItem note) {
        new InsertOutletItemAsyncTask(noteDao).execute(note);
    }

    public void update(OutletItem note) {
        new UpdateOutletItemAsyncTask(noteDao).execute(note);
    }

    public void delete(OutletItem note) {
        new DeleteOutletItemAsyncTask(noteDao).execute(note);
    }

    public void deleteAllOutletItems() {
        new DeleteAllOutletItemAsyncTask(noteDao).execute();
    }

    public LiveData<List<OutletItem>> getAllOutletItems() {
        return allOutletItems;
    }

    public List<OutletItem> getAllTiketById(int itemId) {

        try {
            return new GetTiketByIdAsyncTask(noteDao).execute(itemId).get();
        } catch (ExecutionException e) {
            e.printStackTrace();
            return null;
        } catch (InterruptedException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void getTiketById(int itemId) {
        new GetTiketByIdAsyncTask(noteDao).execute(itemId);
    }

    private static class InsertOutletItemAsyncTask extends AsyncTask<OutletItem, Void, Void>{

        // DATABASE OPERATION
        private OutletDao noteDao;

        private InsertOutletItemAsyncTask(OutletDao noteDao){
            this.noteDao = noteDao;
        }

        @Override
        protected Void doInBackground(OutletItem... cOutletItems) {
            noteDao.insert(cOutletItems[0]);
            return null;
        }
    }
    private static class UpdateOutletItemAsyncTask extends AsyncTask<OutletItem, Void, Void>{

        // DATABASE OPERATION
        private OutletDao noteDao;

        private UpdateOutletItemAsyncTask(OutletDao noteDao){
            this.noteDao = noteDao;
        }

        @Override
        protected Void doInBackground(OutletItem... cOutletItems) {
            noteDao.update(cOutletItems[0]);
            return null;
        }
    }
    private static class DeleteOutletItemAsyncTask extends AsyncTask<OutletItem, Void, Void>{

        // DATABASE OPERATION
        private OutletDao noteDao;

        private DeleteOutletItemAsyncTask(OutletDao noteDao){
            this.noteDao = noteDao;
        }

        @Override
        protected Void doInBackground(OutletItem... cOutletItems) {
            noteDao.delete(cOutletItems[0]);
            return null;
        }
    }
    private static class DeleteAllOutletItemAsyncTask extends AsyncTask<Void, Void, Void>{

        // DATABASE OPERATION
        private OutletDao noteDao;

        private DeleteAllOutletItemAsyncTask(OutletDao noteDao){
            this.noteDao = noteDao;
        }

        @Override
        protected Void doInBackground(Void... cOutletItems) {
            noteDao.deleteAllOutletItems();
            return null;
        }
    }

    private static class GetTiketByIdAsyncTask extends AsyncTask<Integer, Void, List<OutletItem>>{

        // DATABASE OPERATION
        private OutletDao noteDao;

        private GetTiketByIdAsyncTask(OutletDao noteDao){
            this.noteDao = noteDao;
        }

        @Override
        protected List<OutletItem> doInBackground(Integer... pos) {
            return noteDao.getItemById(pos[0]);
        }

        @Override
        protected void onPostExecute(List<OutletItem> tiketParkirResps) {
            super.onPostExecute(tiketParkirResps);
        }
    }
}
