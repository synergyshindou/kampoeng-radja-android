package com.cleva.kampoengradja;

import android.app.Application;
import android.util.Base64;
import android.util.Log;
import android.view.View;

import com.cleva.kampoengradja.model.AuthCardResp;
import com.cleva.kampoengradja.model.CardInfoDataJava;
import com.cleva.kampoengradja.model.CardTerminal;
import com.cleva.kampoengradja.rest.ApiConfig;
import com.cleva.kampoengradja.rest.AppConfig;
import com.cleva.kampoengradja.utils.ISOUtil;
import com.cleva.kampoengradja.utils.SessionManager;
import com.pos.device.SDKException;
import com.pos.device.SDKManager;
import com.pos.device.config.DevConfig;
import com.pos.device.picc.MifareClassic;
import com.pos.device.printer.Printer;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyApp extends Application {

    private static final String TAG = "MyApp";
    public static ApiConfig restApi;
    public static SessionManager session;

    @Override
    public void onCreate() {
        super.onCreate();
        new Thread(() -> SDKManager.init(getApplicationContext(), () -> {
            int status = Printer.getInstance().getStatus();
            String a = "nulls";
            if (status == 0)
                a = "PRINTER_OK";
            else if (status == -1)
                a = "PRINTER_STATUS_BUSY";
            else if (status == -2)
                    a = "PRINTER_STATUS_HIGHT_TEMP";
            else if (status == -3)
                a = "PRINTER_STATUS_PAPER_LACK";
            else if (status == -4)
                a = "PRINTER_STATUS_NO_BATTERY";
            else if (status == -5)
                a = "PRINTER_STATUS_FEED";
            else if (status == -6)
                a = "PRINTER_STATUS_PRINT";
            else if (status == -7)
                a = "PRINTER_STATUS_FORCE_FEED";
            else if (status == -8)
                a = "PRINTER_STATUS_POWER_ON";

            Log.i(TAG, "onCreate: Status Printer " + status);
            Log.i(TAG, "onCreate: Status Printer " + a);
            Log.w(TAG, "onFinish: SN = " + DevConfig.getSN());
            Log.w(TAG, "onFinish: HV = " + DevConfig.getHardwareVersion());
            Log.w(TAG, "onFinish: MC = " + DevConfig.getMachine());
            Log.w(TAG, "onFinish: SP = " + DevConfig.getSPVersion());
            Log.w(TAG, "onFinish: MB = " + DevConfig.getModuleByName(DevConfig.PRINTER));
            Log.w(TAG, "onFinish: MO = " + DevConfig.getModules().toString());
        })).start();

        restApi = AppConfig.getRetrofit().create(ApiConfig.class);

        session = new SessionManager(this);
    }

    public static int readCards(MifareClassic mifareClassic, String skey, byte[] bUID, String sAuth){
        byte[] bKey = Base64.decode(skey, Base64.NO_WRAP);
        try {
            if (mifareClassic != null) {
                boolean bAuth = mifareClassic.authenticate(4, MifareClassic.KEY_TYPE_A, bKey, bUID);
                if (bAuth){

                    byte[] bBlock = mifareClassic.readBlock(4);
                    byte[] bBlock2 = mifareClassic.readBlock(5);
                    if (bBlock != null && bBlock2 != null) {
                        Log.i(TAG, "writeCard: Block "+ 4 + " Read = " + ISOUtil.hexString(bBlock));
                        Log.i(TAG, "writeCard: Block "+ 5 + " Read = " + ISOUtil.hexString(bBlock2));

                        byte[] btAuth = new byte[bBlock.length + bBlock2.length];
                        System.arraycopy(bBlock, 0, btAuth, 0, bBlock.length);
                        System.arraycopy(bBlock2, 0, btAuth, bBlock.length, bBlock2.length);
                        Log.i(TAG, "writeCard: Block 4 "+ 5 + " Read = " + ISOUtil.hexString(btAuth));
                        String sCAuth = Base64.encodeToString(btAuth, Base64.DEFAULT);
                        Log.w(TAG, "readCards data : "+sAuth );
                        Log.w(TAG, "readCards card : "+sCAuth );
                        if (sCAuth.trim().equals(sAuth.trim()))
                            return 1;
                        else // data auth failed
                            return 0;

                    } else // read Failed
                        return -4;
                } else {
                    // auth key failed
                    return -3;
                }
            } else {
                // mifare null
                return -2;
            }
        } catch (SDKException e) {
            e.printStackTrace();
            // SDK Exception
            return -1;
        }
    }
}
