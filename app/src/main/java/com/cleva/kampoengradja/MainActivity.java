package com.cleva.kampoengradja;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.cleva.kampoengradja.model.TerminalAllocation;
import com.pos.device.SDKManager;
import com.pos.device.SDKManagerCallback;
import com.pos.device.config.DevConfig;
import com.pos.device.printer.Printer;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.cleva.kampoengradja.MyApp.restApi;
import static com.cleva.kampoengradja.MyApp.session;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        doAllocation();
    }

    public void doTopup(View view) {
        startActivity(new Intent(this, TopupActivity.class));
    }

    public void doTiket(View view) {
        startActivity(new Intent(this, TicketingActivity.class));
    }

    public void doRefund(View view) {
        startActivity(new Intent(this, RefundActivity.class));
    }

    public void doInfoCard(View view) {
        startActivity(new Intent(this, InfoKartuActivity.class));
    }

    // Not in Menu
    public void doCardPerso(View view) {
        startActivity(new Intent(this, CardPersoActivity.class));
    }

    public void doWahana(View view) {
        startActivity(new Intent(this, WahanaActivity.class));
    }

    public void doOutlet(View view) {
        startActivity(new Intent(this, OutletActivity.class));
    }

    public void doParkir(View view) {
        startActivity(new Intent(this, ParkingActivity.class));
    }

    void doAllocation(){
        restApi.getTerminalAllocation(DevConfig.getSN(), session.getAccessToken()).enqueue(new Callback<TerminalAllocation>() {
            @Override
            public void onResponse(Call<TerminalAllocation> call, Response<TerminalAllocation> response) {
                if (response.isSuccessful())
                {
                    if (response.body()!=null){
                        String t = response.body().getAllocationType();
                        Log.i(TAG, "onResponse: Allocation type "+t);
                        if (t.equals("THEMEPARK_IN_GATE")){
                            Log.i(TAG, "onResponse: Nothing to do here");
                        } else if (t.equalsIgnoreCase("VISITOR")){
                            Intent intent = new Intent(MainActivity.this, InfoKartuActivity.class);
                            startActivity(intent);
                            finish();
                        }  else if (t.equalsIgnoreCase("CARD_ACTIVATION")){
                            Intent intent = new Intent(MainActivity.this, CardPersoActivity.class);
                            startActivity(intent);
                            finish();
                        } else if (t.equalsIgnoreCase("ATTRACTION_IN_GATE")){
                            Intent intent = new Intent(MainActivity.this, WahanaActivity.class);
                            intent.putExtra("ATR", response.body().getAttractionName());
                            startActivity(intent);
                            finish();
                        } else if (t.equalsIgnoreCase("PARKING_IN_GATE")){
                            Intent intent = new Intent(MainActivity.this, ParkingActivity.class);
                            intent.putExtra("TYPES", 1);
                            startActivity(intent);
                            finish();
                        } else if (t.equalsIgnoreCase("PARKING_OUT_GATE")){
                            Intent intent = new Intent(MainActivity.this, ParkingActivity.class);
                            intent.putExtra("TYPES", 2);
                            startActivity(intent);
                            finish();
                        } else if (t.equalsIgnoreCase("OUTLET")){
                            Intent intent = new Intent(MainActivity.this, OutletActivity.class);
                            intent.putExtra("KODE", response.body().getOutletCode());
                            intent.putExtra("NAMA", response.body().getOutletName());
                            Log.e(TAG, "onResponse: "+response.body().getOutletName() );
                            startActivity(intent);
                            finish();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<TerminalAllocation> call, Throwable t) {

            }
        });
    }
}
