package com.cleva.kampoengradja;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cleva.kampoengradja.model.AuthCardResp;
import com.cleva.kampoengradja.model.CardInfoDataJava;
import com.cleva.kampoengradja.model.CardTerminal;
import com.cleva.kampoengradja.utils.ISOUtil;
import com.cleva.kampoengradja.utils.Method;
import com.pos.device.SDKException;
import com.pos.device.config.DevConfig;
import com.pos.device.picc.EmvContactlessCard;
import com.pos.device.picc.MifareClassic;
import com.pos.device.picc.PiccReader;
import com.pos.device.picc.PiccReaderCallback;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.cleva.kampoengradja.MyApp.readCards;
import static com.cleva.kampoengradja.MyApp.restApi;
import static com.cleva.kampoengradja.MyApp.session;
import static com.cleva.kampoengradja.utils.Constants.*;

public class WahanaActivity extends AppCompatActivity {

    private static final String TAG = "WahanaActivity";
    ImageView imgLoad;
    View llSucces, llTap, parent_view;

    PiccReader piccReader;
    private EmvContactlessCard emvContactlessCard = null;

    MifareClassic mifareClassic = null;

    TextView tvTitle,tvMsg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wahana);
        initView();
        setTitle("Wahana Bermain");
        piccReader = PiccReader.getInstance();
        getCard();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (piccReader != null){
            piccReader.stopSearchCard();
            try {
                piccReader.release();
            } catch (SDKException e) {
                e.printStackTrace();
            }
        }
    }

    public void initView() {
        imgLoad = findViewById(R.id.imgloads);

        Glide.with(this)
                .load(R.drawable.tap)
                .into(imgLoad);

        llSucces = findViewById(R.id.llSuccess);

        llTap = findViewById(R.id.llTap);

        parent_view = findViewById(R.id.parent_view);

        tvMsg = findViewById(R.id.tvMsg);
        tvTitle = findViewById(R.id.tvTitle);
        tvTitle.setText(getIntent().getStringExtra("ATR"));
    }

    void getCardAuth(String a, byte[] bUid){
        CardTerminal ct = new CardTerminal();
        ct.setCardUid(a);
        ct.setTerminalSerialNumber(DevConfig.getSN());

        restApi.authCard(ct, session.getAccessToken()).enqueue(new Callback<AuthCardResp>() {
            @Override
            public void onResponse(Call<AuthCardResp> call, Response<AuthCardResp> response) {
                if (response.isSuccessful()){
                    if (response.body() != null){
                        int ares = readCards(mifareClassic, response.body().getAuthKey(), bUid, response.body().getAuthData());
                        Log.i(TAG, "onResponse: ares = "+ ares);
                        switch (ares) {
                            case 1:
                                getCardInfo(ct.getCardUid(), ct);
                                break;
                            case 0:
                                showConfirmDialog(AUTH_0);
                                break;
                            case -4:
                                showConfirmDialog(AUTH_M4);
                                break;
                            case -3:
                                showConfirmDialog(AUTH_M3);
                                break;
                            case -2:
                                showConfirmDialog(AUTH_M2);
                                break;
                            case -1:
                                showConfirmDialog(AUTH_M1);
                                break;
                        }
                    } else {
                        try {
                            Method.showErorRest(parent_view, response.errorBody().string());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        getCard();
                    }
                } else {
                    try {
                        Method.showErorRest(parent_view, response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    getCard();
                }
            }

            @Override
            public void onFailure(Call<AuthCardResp> call, Throwable t) {
                t.printStackTrace();
                showCustomDialogs(WahanaActivity.this);

            }
        });

    }

    public void getCardInfo(String cardUID, CardTerminal ct){
        restApi.cardInfo(cardUID, session.getAccessToken()).enqueue(new Callback<CardInfoDataJava>() {
            @Override
            public void onResponse(Call<CardInfoDataJava> call, Response<CardInfoDataJava> response) {
                if (response.code() == 200) {
                    if (response.body() != null) {
                        if (response.body().getMemberName() != null && response.body().getMemberName().length() != 0) {
                            tvMsg.setText("Selamat Bermain "+response.body().getMemberName());
                        } else
                            tvMsg.setText("Selamat Bermain");
                        doWahana(ct);
                    } else {
                        try {
                            Method.showErorRest(parent_view, response.errorBody().string());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        getCard();
                    }
                } else {
                    try {
                        Method.showErorRest(parent_view, response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    getCard();
                }
            }

            @Override
            public void onFailure(Call<CardInfoDataJava> call, Throwable t) {
                t.printStackTrace();
                showCustomDialogs(WahanaActivity.this);
            }
        });
    }

    void doWahana(CardTerminal ct){
        restApi.wahanaAccess(ct,session.getAccessToken()).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (response.code() == 200){

                    llTap.setVisibility(View.GONE);
                    llSucces.setVisibility(View.VISIBLE);
                    new android.os.Handler().postDelayed(
                            new Runnable() {
                                public void run() {
                                    llTap.setVisibility(View.VISIBLE);
                                    llSucces.setVisibility(View.GONE);
                                    getCard();
                                }
                            },
                            3000);
                } else {
                    try {
                        Method.showErorRest(parent_view, response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    new android.os.Handler().postDelayed(
                            new Runnable() {
                                public void run() {
                                    getCard();
                                }
                            },
                            3000);
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                t.printStackTrace();
                showCustomDialogs(WahanaActivity.this);
            }
        });
    }

    public void timeoutScan(){

//        llError.setVisibility(View.VISIBLE);
        Snackbar.make(parent_view, "Kartu tidak ditemukan/ tidak sesuai", Snackbar.LENGTH_INDEFINITE).setDuration(3000).show();
        getCard();

    }

    public void getCard() {
        new Thread() {
            @Override
            public void run() {
                Log.i(TAG, "CardManager>>getCard>>NFC");
                piccReader.startSearchCard(0, (i, i1) -> {
                    try {
                        Thread.sleep(400);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    Log.i(TAG, "CardManager>>getCard>>NFC>>i=" + i);
                    if (0 == i) {
//                                        listener.callback(handlePICC(i1));
                        Log.i(TAG, "CardManager>>getCard>>NFC_TYPE>>i=" + i1);
                        if (i1 == 0 || i1 == 1) {
                            initMifare();
                        } else
                            timeoutScan();
                    } else {
                        Log.i(TAG, "onSearchResult: not found");
                        timeoutScan();
                    }
                });
            }
        }.start();
    }

    public void initMifare() {
        try {
            mifareClassic = MifareClassic.connect();

            if (mifareClassic != null) {
                byte[] bUID = mifareClassic.getUID();
                if (bUID != null) {
                    Log.i(TAG, "initMifare: UID = " + ISOUtil.hexString(bUID));
                    getCardAuth(ISOUtil.hexString(bUID), bUID);
                }

            } else {
                Log.e(TAG, "initMifare: FAILED");
                timeoutScan();
            }
        } catch (SDKException e) {
            e.printStackTrace();
        }
    }

    private void showConfirmDialog(String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Kampoeng Radja");
        builder.setMessage(msg);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                getCard();
            }
        });
        builder.show();
    }

    public void showCustomDialogs(Context context) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.dialog_warning);
        dialog.setCancelable(true);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;


        ((AppCompatButton) dialog.findViewById(R.id.bt_close)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getCard();
            }
        });

        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }

}
