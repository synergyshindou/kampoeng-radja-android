package com.cleva.kampoengradja;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cleva.kampoengradja.model.DetailTicketResp;
import com.cleva.kampoengradja.rest.ApiConfig;
import com.cleva.kampoengradja.rest.AppConfig;
import com.cleva.kampoengradja.utils.Constants;
import com.cleva.kampoengradja.utils.Method;
import com.cleva.kampoengradja.widgets.ElegantNumberButton;
import com.nex3z.notificationbadge.NotificationBadge;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.cleva.kampoengradja.MyApp.restApi;
import static com.cleva.kampoengradja.MyApp.session;


public class DetailTicketingActivity extends AppCompatActivity {

    private static final String TAG = "DetailTicketingActivity";
    View parent_view, llKeranjang;
    NotificationBadge badge;
    String num = "";
    ImageView img;
    TextView tvTitle, tvPrice, tvType, tvValid, tvDesc, tvAddSaldo, tvBonus, tvFree;
    DetailTicketResp dtr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_ticketing);
        initToolbar();
        AppCompatButton btn = findViewById(R.id.bt_add_to_cart);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialogImageCenter();
            }
        });
        restApi.getItemDetail(String.valueOf(getIntent().getIntExtra("itemID",0)), session.getAccessToken()).enqueue(new Callback<DetailTicketResp>() {
            @Override
            public void onResponse(Call<DetailTicketResp> call, Response<DetailTicketResp> response) {
                if (response.code() == 200){
                    if (response.body() != null){

                        dtr = response.body();
                        if (response.body().getImagePath() != null) {
                            byte[] imageByteArray = Base64.decode(response.body().getImagePath(), Base64.DEFAULT);
                            // here imageBytes is base64String

                            Glide.with(DetailTicketingActivity.this)
                                    .load(Constants.BASE_URL_IMAGE+response.body().getImagePath())
                                    .into(img);
                        }

                        tvTitle.setText(dtr.getTitle());
                        tvPrice.setText(dtr.getPrice() == 0 ? Method.getFormatIDRS(0) : Method.getFormatIDRS(dtr.getPrice()));
                        tvAddSaldo.setText(dtr.getBalanceAddition() == 0 ? Method.getFormatIDRS(0) : Method.getFormatIDRS(dtr.getBalanceAddition()));
                        tvBonus.setText(dtr.getBonusAddition() == 0 ? Method.getFormatIDRS(0) : Method.getFormatIDRS(dtr.getBonusAddition()));
                        tvType.setText(dtr.getAgeType());
                        tvValid.setText(dtr.getValidPeriod());
                        tvDesc.setText(dtr.getDescription());
                        tvFree.setText("-");
                    }
                }
            }

            @Override
            public void onFailure(Call<DetailTicketResp> call, Throwable t) {

            }
        });
    }

    private void initToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Detail Tiket");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        parent_view = findViewById(R.id.parent_view);
        badge = findViewById(R.id.badge);
        llKeranjang = findViewById(R.id.llKeranjang);

        llKeranjang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doChart(v);
            }
        });

        img = findViewById(R.id.image);
        tvTitle = findViewById(R.id.tvTitle);
        tvPrice = findViewById(R.id.tvPrice);
        tvType = findViewById(R.id.tvType);
        tvValid = findViewById(R.id.tvValid);
        tvDesc= findViewById(R.id.tvDesc);
        tvAddSaldo = findViewById(R.id.tvAddtionalSaldo);
        tvBonus = findViewById(R.id.tvBonus);
        tvFree = findViewById(R.id.tvFreeAttr);
    }

    public void doChart(View v){
        startActivity(new Intent(this, KeranjangActivity.class));
    }

    public void showDialogImageCenter() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.dialog_tiket_keranjang);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(true);
        final TextView tvTitle, tvPrice, tvType, tvValid;
        ImageView img = dialog.findViewById(R.id.image);
        if (dtr.getImagePath() != null) {
            // byte[] imageByteArray = Base64.decode(dtr.getImageBase64(), Base64.DEFAULT);
            // here imageBytes is base64String

            Glide.with(DetailTicketingActivity.this)
                    .load(Constants.BASE_URL_IMAGE+dtr.getImagePath())
                    .into(img);
        }

        tvTitle = dialog.findViewById(R.id.tvTitle);
        tvPrice = dialog.findViewById(R.id.tvPrice);
        tvType = dialog.findViewById(R.id.tvType);
        tvValid = dialog.findViewById(R.id.tvValid);

        tvTitle.setText(dtr.getTitle());
        tvPrice.setText(Method.getFormatIDRS(dtr.getPrice()));

        final ElegantNumberButton button = dialog.findViewById(R.id.number_button);
        button.setNumber("1");
        num = "1";
        button.setOnClickListener(new ElegantNumberButton.OnClickListener() {
            @Override
            public void onClick(View view) {
                num = button.getNumber();
                Log.i(TAG, "onClick: " + num);
                if (button.getNumbers() == 0)
                    dialog.hide();
            }
        });

        AppCompatButton btn = dialog.findViewById(R.id.bt_add_to_cart);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Snackbar.make(parent_view, tvTitle.getText().toString() + " berhasil masuk keranjang", Snackbar.LENGTH_INDEFINITE).setDuration(3000).show();
                dialog.hide();
                badge.setNumber(Integer.parseInt(num));
            }
        });

        dialog.show();
    }
}
