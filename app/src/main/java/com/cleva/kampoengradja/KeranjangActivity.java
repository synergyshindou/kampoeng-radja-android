package com.cleva.kampoengradja;

import android.app.Dialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.cleva.kampoengradja.adapter.KeranjangAdapter;
import com.cleva.kampoengradja.adapter.KeranjangTicketParkingAdapter;
import com.cleva.kampoengradja.model.AuthCardResp;
import com.cleva.kampoengradja.model.CalcPriceDetail;
import com.cleva.kampoengradja.model.CalculatePriceResp;
import com.cleva.kampoengradja.model.CardIdentifierId;
import com.cleva.kampoengradja.model.CardInfoDataJava;
import com.cleva.kampoengradja.model.CardTerminal;
import com.cleva.kampoengradja.model.PaymentMethodResp;
import com.cleva.kampoengradja.model.TicketPurchaseResp;
import com.cleva.kampoengradja.model.TicketingPurchaseRequest;
import com.cleva.kampoengradja.model.TiketParkirResp;
import com.cleva.kampoengradja.rest.ApiConfig;
import com.cleva.kampoengradja.rest.AppConfig;
import com.cleva.kampoengradja.utils.Constants;
import com.cleva.kampoengradja.utils.ISOUtil;
import com.cleva.kampoengradja.utils.Method;
import com.cleva.kampoengradja.viewmodel.TiketViewModel;
import com.cleva.kampoengradja.widgets.ElegantNumberButton;
import com.cleva.kampoengradja.widgets.VerticalSpaceItemDecoration;
import com.pos.device.SDKException;
import com.pos.device.config.DevConfig;
import com.pos.device.picc.EmvContactlessCard;
import com.pos.device.picc.MifareClassic;
import com.pos.device.picc.PiccReader;
import com.pos.device.picc.PiccReaderCallback;
import com.pos.device.printer.PrintCanvas;
import com.pos.device.printer.PrintTask;
import com.pos.device.printer.Printer;
import com.pos.device.printer.PrinterCallback;

import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.CountDownLatch;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.cleva.kampoengradja.MyApp.readCards;
import static com.cleva.kampoengradja.MyApp.restApi;
import static com.cleva.kampoengradja.MyApp.session;
import static com.cleva.kampoengradja.utils.Constants.*;
import static com.cleva.kampoengradja.utils.Method.loadImg;
import static com.cleva.kampoengradja.utils.Method.printLine;
import static com.cleva.kampoengradja.utils.Method.setFontStyle;
import static com.cleva.kampoengradja.utils.Method.*;

public class KeranjangActivity extends AppCompatActivity {

    private static final String TAG = "KeranjangActivity";

    View llBayar;

    AppCompatButton btnBayar;

    TiketViewModel tiketViewModel;

    KeranjangAdapter keranjangAdapter;

    RecyclerView rvkeranjang;

    Spinner spinner1;

    List<TiketParkirResp> trp;

    View parent_view, llTap;

    PiccReader piccReader;

    private EmvContactlessCard emvContactlessCard = null;

    MifareClassic mifareClassic = null;

    TextView tvDiskon, tvBDiskon, tvTotal, tvKembali, tvMsg;

    Dialog dialog;

    EditText etBayar, editText1;

    CalculatePriceResp cpr;

    String sCardUid = "", sCNum = "";

    List<String> listvalue;

    ImageView imgLoad;

    String a, b, c, d, e;

    int result = 0, posPay = 0;

    public static Printer printer = null;

    public static PrintTask printTask = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_keranjang);
        piccReader = PiccReader.getInstance();
        initView();

        tiketViewModel = ViewModelProviders.of(this).get(TiketViewModel.class);
        tiketViewModel.getAllTiketParkirResps().observe(this, new Observer<List<TiketParkirResp>>() {
            @Override
            public void onChanged(@Nullable List<TiketParkirResp> notes) {
                if (notes.size() == 0) {

                    Snackbar.make(parent_view, "Keranjang kosong", Snackbar.LENGTH_INDEFINITE).setDuration(3000).show();
                    finish();
                }
                keranjangAdapter.submitList(notes);
                trp = notes;
            }
        });

        getPaymentMethod();
        showDialog();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (piccReader != null) {
            piccReader.stopSearchCard();
            try {
                piccReader.release();
            } catch (SDKException e) {
                e.printStackTrace();
            }
        }
    }

    // The function called to calculate and display the result of the multiplication
    private void calculateResult() throws NumberFormatException {
        // Gets the two EditText controls' Editable values
        Editable editableValue1 = editText1.getText(),
                editableValue2 = etBayar.getText();

        // Initializes the double values and result
        int value1 = 0, value2 = 0;

        // If the Editable values are not null, obtains their double values by parsing
        if (editableValue1 != null)
            value1 = Integer.parseInt(editableValue1.toString());

        if (editableValue2 != null && editableValue2.toString().length() != 0)
            value2 = Integer.parseInt(editableValue2.toString());

        // Calculates the result
        result = value2 - value1;

        // Displays the calculated result
        if (result < 0)
            tvKembali.setText(Method.getFormatIDR(0));
        else
            tvKembali.setText(Method.getFormatIDR(result));
    }

    public void initView() {
        llBayar = findViewById(R.id.llBayar);
        btnBayar = findViewById(R.id.btnBayar);
        btnBayar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etBayar.getText().length() == 0 && posPay != 0) {
                    doSnack("Mohon isi jumlah pembayaran");
                    etBayar.requestFocus();
                    etBayar.setError("mohon diisi!");
                } else if (result < 0 && posPay != 0) {
                    doSnack("Pembayaran kurang dari harga total");
                    etBayar.requestFocus();
                    etBayar.setError("Pembayaran kurang dari harga total!");
                } else {

                    if (posPay == 0) {
                        d = c;
                        etBayar.setText(d);
                    } else
                        d = etBayar.getText().toString();

                    int kem = Integer.parseInt(d) - Integer.parseInt(c);
                    e = String.valueOf(kem);
                    TicketingPurchaseRequest tpr = new TicketingPurchaseRequest(sCardUid, trp,
                            DevConfig.getSN(), Integer.parseInt(etBayar.getText().toString()), listvalue.get(spinner1.getSelectedItemPosition()), cpr);
                    restApi.tiketPurchase(tpr, session.getAccessToken()).enqueue(new Callback<TicketPurchaseResp>() {
                        @Override
                        public void onResponse(Call<TicketPurchaseResp> call, Response<TicketPurchaseResp> response) {
                            if (response.code() == 200) {
                                if (response.body() != null) {
                                    Log.i(TAG, "onResponse: " + response.body().getTransactionId());
                                    showSuccessDialog();
                                    prints(Method.getFormatIDR(a),
                                            Method.getFormatIDR(b), Method.getFormatIDR(c),
                                            Method.getFormatIDR(d), Method.getFormatIDR(e), false,
                                            false);
                                } else {
                                    try {
                                        Method.showErorRest(parent_view, response.errorBody().string());
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                    showDialog();
                                }
                            } else {
                                try {
                                    Method.showErorRest(parent_view, response.errorBody().string());
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                                showDialog();
                            }
                        }

                        @Override
                        public void onFailure(Call<TicketPurchaseResp> call, Throwable t) {
                            t.printStackTrace();
                            showCustomDialogs(KeranjangActivity.this);

                        }
                    });
                }
            }
        });

        rvkeranjang = findViewById(R.id.rvKeranjang);
        rvkeranjang.setLayoutManager(new LinearLayoutManager(this));
        rvkeranjang.setHasFixedSize(true);
        rvkeranjang.addItemDecoration(new VerticalSpaceItemDecoration(20));

        keranjangAdapter = new KeranjangAdapter(this);
        rvkeranjang.setAdapter(keranjangAdapter);
        keranjangAdapter.setOnItemClickListener(new KeranjangAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(TiketParkirResp note) {
                showDialogImageCenter(note);
            }
        });

        spinner1 = findViewById(R.id.spinMethod);

        parent_view = findViewById(R.id.parent_view);
        llTap = findViewById(R.id.llTap);

        tvBDiskon = findViewById(R.id.tvBDiskon);
        tvDiskon = findViewById(R.id.tvDiskon);
        tvTotal = findViewById(R.id.tvTotal);
        tvKembali = findViewById(R.id.tvKembali);

        etBayar = findViewById(R.id.etBayar);
        editText1 = findViewById(R.id.editText1);

        TextWatcher textWatcher = new TextWatcher() {
            public void afterTextChanged(Editable s) {
                calculateResult();
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        };

//        editText1.addTextChangedListener(textWatcher);
        etBayar.addTextChangedListener(textWatcher);

        imgLoad = findViewById(R.id.imgloads);
        tvMsg = findViewById(R.id.tvMsg);

        Glide.with(this)
                .load(R.drawable.tap)
                .into(imgLoad);
    }

    public void showDialog() {
//        dialog = new Dialog(this);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
//        dialog.setContentView(R.layout.layout_tap_card);
//        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
//        dialog.setCancelable(false);
//        dialog.setOnKeyListener(new Dialog.OnKeyListener() {
//
//            @Override
//            public boolean onKey(DialogInterface arg0, int keyCode,
//                                 KeyEvent event) {
//                // TODO Auto-generated method stub
//                if (keyCode == KeyEvent.KEYCODE_BACK) {
//                    finish();
//                    dialog.dismiss();
//                }
//                return true;
//            }
//        });
//
//        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
//        lp.copyFrom(dialog.getWindow().getAttributes());
//        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
//        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
//
//        imgLoad = dialog.findViewById(R.id.imgloads);
//        tvMsg = dialog.findViewById(R.id.tvMsg);
//
//        Glide.with(this)
//                .load(R.drawable.tap)
//                .into(imgLoad);
//
//        dialog.show();
//        dialog.getWindow().setAttributes(lp);

        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        llTap.setVisibility(View.VISIBLE);
                        Method.tapImgs(KeranjangActivity.this, imgLoad, tvMsg);
                        getCard();
                    }
                },
                3000);
    }

    public void showSuccessDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.layout_success_buy);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(false);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        AppCompatButton btnReprint, btnBack;
        btnReprint = dialog.findViewById(R.id.btnReprint);
        btnReprint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                prints(Method.getFormatIDR(a),
                        Method.getFormatIDR(b), Method.getFormatIDR(c),
                        Method.getFormatIDR(d), Method.getFormatIDR(e), false,
                        true);
            }
        });

        btnBack = dialog.findViewById(R.id.btnBack);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tiketViewModel.deleteAllTiketParkirResps();
                dialog.hide();
                finish();
            }
        });
        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }

    public void doCancel(View v) {
        showConfirmDialogCancel();
    }

    private void showConfirmDialogCancel() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Peringatan");
        builder.setMessage("Keranjang akan di kosongkan.\n\nLanjut?");
        builder.setPositiveButton("Lanjut", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                tiketViewModel.deleteAllTiketParkirResps();
                onBackPressed();
            }
        });
        builder.setNegativeButton("Batal", null);
        builder.show();
    }

    void getPaymentMethod() {
        restApi.getPaymentMethod(session.getAccessToken()).enqueue(new Callback<List<PaymentMethodResp>>() {
            @Override
            public void onResponse(Call<List<PaymentMethodResp>> call, Response<List<PaymentMethodResp>> response) {
                if (response.code() == 200) {
                    if (response.body() != null) {

                        List<String> list = new ArrayList<String>();
                        listvalue = new ArrayList<String>();
                        for (int i = 0; i < response.body().size(); i++) {
                            list.add(response.body().get(i).getLabel());
                            listvalue.add(response.body().get(i).getValue());
                        }
                        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(KeranjangActivity.this,
                                android.R.layout.simple_spinner_item, list);
                        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spinner1.setAdapter(dataAdapter);
                        spinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                                // your code here
                                posPay = position;
                                Log.i(TAG, "onItemSelected: " + position);
                                Log.i(TAG, "onItemSelected: " + list.get(position));
                                if (!listvalue.get(position).equals("EWALLET")) {
                                    llBayar.setVisibility(View.VISIBLE);
                                    etBayar.setText("");

                                } else {
                                    llBayar.setVisibility(View.GONE);
                                    tvKembali.setText("0");

                                }
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parentView) {
                                // your code here
                            }

                        });
                    }
                }
            }

            @Override
            public void onFailure(Call<List<PaymentMethodResp>> call, Throwable t) {
                t.printStackTrace();
                showCustomDialogs(KeranjangActivity.this);

            }
        });
    }

    void getCardInfoPrice() {
        CalcPriceDetail cpd = new CalcPriceDetail(sCardUid, trp, DevConfig.getSN());
        restApi.calculatePriceDetail(cpd, session.getAccessToken()).enqueue(new Callback<CalculatePriceResp>() {
            @Override
            public void onResponse(Call<CalculatePriceResp> call, Response<CalculatePriceResp> r) {
                Log.i(TAG, "onResponse: " + r.code());
                if (r.code() == 200) {
                    llTap.setVisibility(View.GONE);
                    if (r.body() != null) {
                        cpr = r.body();
                        a = String.valueOf(r.body().getPriceBeforeDiscount());
                        b = String.valueOf(r.body().getDiscountedAmount());
                        c = String.valueOf(r.body().getTotalPrice());
                        tvBDiskon.setText(Method.getFormatIDR(a));
                        tvDiskon.setText("- " + Method.getFormatIDR(b));
                        tvTotal.setText(Method.getFormatIDR(c));
                        editText1.setText(c);
                    } else {
                        try {
                            JSONObject jObjError = new JSONObject(r.errorBody().string());
                            if (jObjError.has("message"))
                                Snackbar.make(parent_view, jObjError.getString("message"), Snackbar.LENGTH_INDEFINITE).setDuration(3000).show();
                            else
                                Snackbar.make(parent_view, "Terjadi kesalahan, silahkan coba lagi", Snackbar.LENGTH_INDEFINITE).setDuration(3000).show();

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        showDialog();
                    }
                } else {
                    try {
                        JSONObject jObjError = new JSONObject(r.errorBody().string());
                        if (jObjError.has("message"))
                            Snackbar.make(parent_view, jObjError.getString("message"), Snackbar.LENGTH_INDEFINITE).setDuration(3000).show();
                        else
                            Snackbar.make(parent_view, "Terjadi kesalahan, silahkan coba lagi", Snackbar.LENGTH_INDEFINITE).setDuration(3000).show();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    showDialog();
                }
            }

            @Override
            public void onFailure(Call<CalculatePriceResp> call, Throwable t) {
                t.printStackTrace();
                showDialog();
                Snackbar.make(parent_view, "Terjadi kesalahan, silahkan coba lagi", Snackbar.LENGTH_INDEFINITE).setDuration(3000).show();

            }
        });

    }

    void getCardAuth(String a, byte[] bUid) {
        CardTerminal ct = new CardTerminal();
        ct.setCardUid(a);
        ct.setTerminalSerialNumber(DevConfig.getSN());

        restApi.authCard(ct, session.getAccessToken()).enqueue(new Callback<AuthCardResp>() {
            @Override
            public void onResponse(Call<AuthCardResp> call, Response<AuthCardResp> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        int ares = readCards(mifareClassic, response.body().getAuthKey(), bUid, response.body().getAuthData());
                        Log.i(TAG, "onResponse: ares = " + ares);
                        switch (ares) {
                            case 1:
                                getCardInfo(ct.getCardUid());
                                break;
                            case 0:
                                showConfirmDialog(AUTH_0);
                                break;
                            case -4:
                                showConfirmDialog(AUTH_M4);
                                break;
                            case -3:
                                showConfirmDialog(AUTH_M3);
                                break;
                            case -2:
                                showConfirmDialog(AUTH_M2);
                                break;
                            case -1:
                                showConfirmDialog(AUTH_M1);
                                break;
                        }
                    } else {
                        try {
                            Method.showErorRest(parent_view, response.errorBody().string());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        showDialog();
                    }
                } else {
                    try {
                        Method.showErorRest(parent_view, response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    showDialog();
                }
            }

            @Override
            public void onFailure(Call<AuthCardResp> call, Throwable t) {
                t.printStackTrace();
                showCustomDialogs(KeranjangActivity.this);

            }
        });

    }

    public void getCardInfo(String cardUID) {
        sCardUid = cardUID;
        restApi.cardInfo(sCardUid, session.getAccessToken()).enqueue(new Callback<CardInfoDataJava>() {
            @Override
            public void onResponse(Call<CardInfoDataJava> call, Response<CardInfoDataJava> response) {
                if (response.code() == 200) {
                    if (response.body() != null) {
                        // HERE
                        sCNum = response.body().getCardIdentifierId();
                        getCardInfoPrice();
                    } else {
                        try {
                            Method.showErorRest(parent_view, response.errorBody().string());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        showDialog();
                    }
                } else {
                    try {
                        Method.showErorRest(parent_view, response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    showDialog();
                }
            }

            @Override
            public void onFailure(Call<CardInfoDataJava> call, Throwable t) {
                t.printStackTrace();
                showCustomDialogs(KeranjangActivity.this);
            }
        });
    }

    public void timeoutScan() {
        Snackbar.make(parent_view, "Kartu tidak dikenali atau tidak aktif", Snackbar.LENGTH_INDEFINITE).setDuration(3000).show();
//        Method.tapImg(this, imgLoad, (findViewById(R.id.tvMsg)));
//        dialog.hide();
        showDialog();
    }

    public void getCard() {
        new Thread() {
            @Override
            public void run() {
                Log.i(TAG, "CardManager>>getCard>>NFC");
                piccReader.startSearchCard(0, new PiccReaderCallback() {
                    @Override
                    public void onSearchResult(int i, int i1) {
                        try {
                            Thread.sleep(400);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        Log.i(TAG, "CardManager>>getCard>>NFC>>i=" + i);
                        if (0 == i) {
//                                        listener.callback(handlePICC(i1));
                            Log.i(TAG, "CardManager>>getCard>>NFC_TYPE>>i=" + i1);
                            if (i1 == 0 || i1 == 1) {
                                initMifare();
                            } else {
                                Log.i(TAG, "CardManager>>getCard: not found");
                                timeoutScan();
                            }
                        } else {
                            Log.i(TAG, "onSearchResult: not found");
                            timeoutScan();
                        }
                    }
                });
            }
        }.start();
    }

    public void initMifare() {
        try {
            mifareClassic = MifareClassic.connect();
            loadImg(this, imgLoad, tvMsg);

            if (mifareClassic != null) {
                byte[] bUID = mifareClassic.getUID();
                if (bUID != null) {
                    Log.i(TAG, "initMifares: UID = " + ISOUtil.hexString(bUID));
                    getCardAuth(ISOUtil.hexString(bUID), bUID);
                } else {
                    Log.e(TAG, "initMifare: FAILED");
                    timeoutScan();
                }

            } else {
                Log.e(TAG, "initMifare:Null FAILED");
                timeoutScan();
            }
        } catch (SDKException e) {
            e.printStackTrace();
        }
    }

    void doSnack(String waht) {
        Snackbar.make(parent_view, waht, Snackbar.LENGTH_INDEFINITE).setDuration(3000).show();
    }

    public void prints(final String a, final String b, final String c, final String d,
                       final String e, final boolean isprev, final boolean isreprint) {
        new Thread() {
            @Override
            public void run() {

                int az = printDetails(a, b, c, d, e, isprev, isreprint);
                Log.i(TAG, "run: " + az);
            }
        }.start();
    }

    public int printDetails(String a, String b, String c, String d, String e, boolean isPreview,
                            boolean isReprint) {
        printTask = new PrintTask();
        printTask.setGray(130);
        int ret = -1;
        printer = Printer.getInstance();
        if (printer == null) {
            ret = 112;
        } else {

            SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yy, HH:mm", new Locale("ID"));
            String currentDateandTime = sdf.format(new Date());

            Bitmap icon = BitmapFactory.decodeResource(this.getResources(),
                    R.drawable.kampoeng_log);

            Bitmap scaled = Bitmap.createScaledBitmap(icon, 320, 120, true);

            final PrintCanvas canvas = new PrintCanvas();
            Paint paint = new Paint();
            setFontStyle(this, paint, 2, true);
            if (isReprint)
                canvas.drawText(Constants.REPRINT, paint);
            canvas.drawText(" ", paint);
            canvas.drawBitmap(scaled, paint);
            setFontStyle(this, paint, 2, true);
            canvas.drawText(" ", paint);
            setFontStyle(this, paint, 2, true);
            canvas.drawText("------ Ticket Terminal ------", paint);
            canvas.drawText(currentDateandTime + "   " + DevConfig.getSN(), paint);
            printLine(this, paint, canvas);
            setFontStyle(this, paint, 2, true);
            canvas.drawText("No. Card  " + Method.splitCardNum(sCNum).trim(), paint);
            canvas.drawText(" ", paint);
            setFontStyle(this, paint, 2, true);
            for (int i = 0; i < trp.size(); i++) {
                setFontStyle(this, paint, 2, true);
                canvas.drawText(trp.get(i).getTitle(), paint);
                String ber = trp.get(i).getQuantity() + "x " + Method.getFormatIDR(trp.get(i).getPrice()) +
                        Method.getFormatIDR(trp.get(i).getPrice() * trp.get(i).getQuantity());
                String dep = trp.get(i).getQuantity() + "x " + Method.getFormatIDR(trp.get(i).getPrice());
                String bel = Method.getFormatIDR(trp.get(i).getPrice() * trp.get(i).getQuantity());
                int kos = 29 - ber.length();
                StringBuilder spa = new StringBuilder();
                for (int j = 0; j < kos; j++) {
                    spa.append(" ");
                }
                canvas.drawText(dep + spa + bel, paint);
            }

            printLine(this, paint, canvas);

            setFontStyle(this, paint, 2, true);
            canvas.drawText("Harga       : " + a, paint);
            canvas.drawText("Diskon      : " + b, paint);
            printLine(this, paint, canvas);
            setFontStyle(this, paint, 2, true);
            canvas.drawText("Total Harga : " + c, paint);
            canvas.drawText("Tunai       : " + d, paint);
            canvas.drawText("Kembali     : " + e, paint);

            canvas.drawText(" ", paint);
            canvas.drawText("------- Terima  Kasih -------", paint);
            canvas.drawText(" ", paint);

            if (isPreview) {
                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {

                        // Stuff that updates the UI
                        imgLoad.setImageBitmap(canvas.getBitmap());

                    }
                });
            } else {
                ret = printData(canvas);
            }

            if (printer != null) {
                printer = null;
            }
        }
        return ret;
    }

    private int printData(PrintCanvas pCanvas) {
        final CountDownLatch latch = new CountDownLatch(1);
        printer = Printer.getInstance();
        int ret = printer.getStatus();
        if (Printer.PRINTER_STATUS_PAPER_LACK == ret) {
            long start = SystemClock.uptimeMillis();
            while (true) {
                if (SystemClock.uptimeMillis() - start > 60 * 1000) {
                    ret = Printer.PRINTER_STATUS_PAPER_LACK;
                    break;
                }
                if (printer.getStatus() == Printer.PRINTER_OK) {
                    ret = Printer.PRINTER_OK;
                    break;
                } else {
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                    }
                }
            }
        }
        if (ret == Printer.PRINTER_OK) {
            printTask.setPrintCanvas(pCanvas);
            printer.startPrint(printTask, new PrinterCallback() {
                @Override
                public void onResult(int i, PrintTask printTask) {
                    latch.countDown();
                }
            });
            try {
                latch.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return ret;
    }


    public void showDialogImageCenter(final TiketParkirResp item) {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.dialog_tiket_keranjang);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(true);

        final TextView tvTitle, tvPrice, tvType, tvValid;

        tvTitle = dialog.findViewById(R.id.tvTitle);
        tvPrice = dialog.findViewById(R.id.tvPrice);
        tvType = dialog.findViewById(R.id.tvType);
        tvValid = dialog.findViewById(R.id.tvValid);

        tvTitle.setText(item.getTitle());
        tvPrice.setText(Method.getFormatIDRS(item.getPrice()));
        ImageView img = dialog.findViewById(R.id.image);
        if (item.getImagePath() != null) {
            // byte[] imageByteArray = Base64.decode(item.getImageBase64(), Base64.DEFAULT);
            // here imageBytes is base64String

            Glide.with(KeranjangActivity.this)
                    .load(Constants.BASE_URL_IMAGE + item.getImagePath())
                    .into(img);
        }

        final ElegantNumberButton button = dialog.findViewById(R.id.number_button);
        button.setNumber(String.valueOf(item.getQuantity()));
        button.setOnClickListener(new ElegantNumberButton.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });

        AppCompatButton btn = dialog.findViewById(R.id.bt_add_to_cart);
        btn.setText("Ubah Transaksi");
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                List<TiketParkirResp> getTiket = tiketViewModel.getTiketById(item.getItemId());
                if (getTiket != null) {
                    Log.i(TAG, "onClick: " + getTiket.size());

                    TiketParkirResp tpr;
                    if (getTiket.size() == 1) {
                        tpr = getTiket.get(0);
                        dialog.hide();

                        tpr.setItemId(item.getItemId());
                        tpr.setTitle(item.getTitle());
                        tpr.setPrice(item.getPrice());
                        tpr.setImagePath(item.getImagePath());
                        tpr.setQuantity(button.getNumbers());
                        if (button.getNumbers() == 0) {
                            tiketViewModel.delete(tpr);
                            Snackbar.make(parent_view, tvTitle.getText().toString() + " berhasil ubah transaksi", Snackbar.LENGTH_INDEFINITE).setDuration(3000).show();
                            showDialog();
                        } else {
                            tiketViewModel.update(tpr);
                            Snackbar.make(parent_view, tvTitle.getText().toString() + " berhasil ubah transaksi", Snackbar.LENGTH_INDEFINITE).setDuration(3000).show();

                        }
                    } else {
                        Log.e(TAG, "onClick: Size 0");
                        Snackbar.make(parent_view, tvTitle.getText().toString() + " berhasil ubah transaksi", Snackbar.LENGTH_INDEFINITE).setDuration(3000).show();
                        dialog.hide();

                        item.setQuantity(button.getNumbers());
                        tiketViewModel.insert(item);
                    }

                } else {
                    Log.i(TAG, "onClick: else ");
                    Snackbar.make(parent_view, tvTitle.getText().toString() + " berhasil ubah transaksi", Snackbar.LENGTH_INDEFINITE).setDuration(3000).show();
                    dialog.hide();

                    item.setQuantity(button.getNumbers());
                    tiketViewModel.insert(item);
                }
            }
        });

        dialog.show();
    }

    private void showConfirmDialog(String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Kampoeng Radja");
        builder.setMessage(msg);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                getCard();
            }
        });
        builder.show();
    }
}
