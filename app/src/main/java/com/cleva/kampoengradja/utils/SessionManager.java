package com.cleva.kampoengradja.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class SessionManager {

    private static final String STORE_NAME = "CARDPERSOKEY";
    private static final String KEY_STATE = "keyActivation";
    private static final String KEY_ACCESS_TOKEN = "accessToken";

    private SharedPreferences mPrefs = null;

    public SessionManager(Context context) {
        mPrefs = context.getSharedPreferences(STORE_NAME, Context.MODE_PRIVATE);
    }

    public void setKeyActivation(String sKey){
        SharedPreferences.Editor editor = mPrefs.edit();
        editor.putString(KEY_STATE, sKey);
        if (!editor.commit()) {
            throw new IllegalStateException("Failed to write state to shared prefs");
        }
    }

    public String getKeyActivation(){
        return mPrefs.getString(KEY_STATE, "");
    }

    public void setAccessToken(String sKey){
        SharedPreferences.Editor editor = mPrefs.edit();
        editor.putString(KEY_ACCESS_TOKEN, sKey);
        if (!editor.commit()) {
            throw new IllegalStateException("Failed to write state to shared prefs");
        }
    }

    public String getAccessToken(){
        return mPrefs.getString(KEY_ACCESS_TOKEN, "");
    }
}
