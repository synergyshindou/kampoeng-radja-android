package com.cleva.kampoengradja.utils;

public interface OnLoadMoreListener {
    void onLoadMore();
}
