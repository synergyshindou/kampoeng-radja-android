package com.cleva.kampoengradja.utils;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatButton;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.cleva.kampoengradja.R;
import com.pos.device.printer.PrintCanvas;

import org.json.JSONObject;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class Method {

    public static String getFormatIDR(String r) {
        Double a = Double.parseDouble(r);
        Locale localeID = new Locale("in", "ID");
        NumberFormat formatRupiah = NumberFormat.getCurrencyInstance(localeID);
        String currency = formatRupiah.format(a).replace("Rp", "");
        return currency.replace(",00", "");
    }

    public static String getFormatIDR(int r) {
        String rp = String.valueOf(r);
        Double a = Double.parseDouble(rp);
        Locale localeID = new Locale("in", "ID");
        NumberFormat formatRupiah = NumberFormat.getCurrencyInstance(localeID);
        String currency = formatRupiah.format(a).replace("Rp", "");
        return currency.replace(",00", "");
    }

    public static String getFormatIDRS(int r) {
        String rp = String.valueOf(r);
        Double a = Double.parseDouble(rp);
        Locale localeID = new Locale("in", "ID");
        NumberFormat formatRupiah = NumberFormat.getCurrencyInstance(localeID);
        String currency = formatRupiah.format(a);
        return currency.replace(",00", "");
    }

    public static void showErorRest(View parent_view, String jMessage) {
        try {
            JSONObject jObjError = new JSONObject(jMessage);
            if (jObjError.has("message"))
                Snackbar.make(parent_view, jObjError.getString("message"), Snackbar.LENGTH_INDEFINITE).setDuration(3000).show();
            else
                Snackbar.make(parent_view, "Terjadi kesalahan, silahkan coba lagi", Snackbar.LENGTH_INDEFINITE).setDuration(3000).show();

        } catch (Exception e) {
            e.printStackTrace();
            Log.e("JSON Parser", "Error parsing data [" + e.getMessage()+"] ");
            Snackbar.make(parent_view, "Terjadi kesalahan, silahkan coba lagi", Snackbar.LENGTH_INDEFINITE).setDuration(3000).show();
        }
    }

    public static void setFontStyle(Context context, Paint paint, int size, boolean isBold) {

        Typeface fonts = Typeface.createFromAsset(context.getAssets(),  "fonts/hackregular.ttf");
        Typeface fonts_bold = Typeface.createFromAsset(context.getAssets(),  "fonts/hackbold.ttf");
        if (isBold) {
            paint.setTypeface(fonts_bold);
        } else {
            paint.setTypeface(fonts);
        }
        switch (size) {
            case 0:
                break;
            case 1:
                paint.setTextSize(18F);
                break;
            case 2:
                paint.setTextSize(22F);
                break;
            case 3:
                paint.setTextSize(30F);
                break;
            default:
                break;
        }
    }

    public static PrintCanvas printLine(Context context, Paint paint, PrintCanvas canvas) {
//        String line = "----------------------------------------------------------------";
        String line = "----------------------------------";
        setFontStyle(context, paint, 1, true);
        canvas.drawText(line, paint);
        return canvas;
    }


    public static void showCustomDialogs(Context context) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.dialog_warning);
        dialog.setCancelable(true);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;


        ((AppCompatButton) dialog.findViewById(R.id.bt_close)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }

    public static String splitCardNum(String num){
        StringBuilder a = new StringBuilder();
        int k = num.length()/4;
        if (k == 4){
            List<String> strings = new ArrayList<String>();
            int index = 0;
            while (index < num.length()) {
                strings.add(num.substring(index, Math.min(index + 4,num.length())));
                index += 4;
            }
            for (int i = 0; i < strings.size(); i++) {
                a.append(strings.get(i)).append(" ");
            }
        }
        return String.valueOf(a);
    }

    public static void loadImg(Context mContext, ImageView img, TextView tvMsg){

        Glide.with(mContext)
                .load(R.drawable.load)
                .into(img);

        tvMsg.setText("Proses kartu, tetap tempelkan kartu");
    }

    public static void tapImg(Context mContext, ImageView img, TextView tvMsg){
        Glide.with(mContext)
                .load(R.drawable.tap)
                .into(img);
        tvMsg.setText(mContext.getString(R.string.tempelkan_kartu_diatas_pos));
    }

    public static void tapImgs(Context mContext, ImageView img, TextView tvMsg){
        Glide.with(mContext)
                .load(R.drawable.tap)
                .into(img);
        tvMsg.setText("Tap Kartu untuk menampilkan detail harga");
    }

    public static String printSpace(String sum, String depan, String belakang){
        int kos = 29-sum.length();
        StringBuilder spa = new StringBuilder();
        for (int j = 0; j < kos; j++) {
            spa.append(" ");
        }
        return depan + spa + belakang;
    }
}
