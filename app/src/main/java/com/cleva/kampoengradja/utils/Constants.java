package com.cleva.kampoengradja.utils;

public class Constants {

    public static String TiketTopup = "9220025234";
    public static String WahanaRenang = "9220025231";
    public static String WahanaPaintball = "9220025232";
    public static String InfoKartu = "9220025233";
    public static String ParkIn = "9220025235";
    public static String ParkOut = "9220025236";
    public static String Outlet = "9220025237";
    public static String CardPerso = "9220025238";
    public static String BASE_URLs = "http://ticket.cleva.id:8081/api/";
    public static String BASE_URL = "https://ticket.cleva.id/api/";
    public static String BASE_URL_IMAGES = "http://ticket.cleva.id:8081";
    public static String BASE_URL_IMAGE = "https://ticket.cleva.id";

    public static String DEFAULT_KEY_AUTH = "FFFFFFFFFFFF";

    public static String AUTH_1 = "SUCCES AUTH";
    public static String AUTH_0 = "Data auth failed, Please tap again";
    public static String AUTH_M4 = "Read Failed, Please tap again";
    public static String AUTH_M3 = "Auth key failed, Please tap again";
    public static String AUTH_M2 = "Mifare null, Please tap again";
    public static String AUTH_M1 = "SDK Exception, Please tap again";

    public static final String MERCHANT_ID      = "NO MERCHANT   :";
    public static final String MERCHANT_NAME    = "NAMA MERCHANT :";
    public static final String TERNIMAL_ID      = "NO TERMINAL   :";
    public static final String DATE_TIME        = "TGL/WAKTU     :";
    public static final String OPERATOR_NO = "OPERATOR NO";
    public static final String CARD_NO = "CARD NO:";
    public static final String SCANCODE ="PayCode:" ;
    public static final String ISSUER = "ISSUER : China Bank";
    public static final String ISSUER2 = "ISSUER:";
    public static final String ACQUIRER = "ACQ : Unionpay";
    public static final String ACQUIRER2 = "ACQ:";
    public static final String TRANS_AAC = "AAC:";
    public static final String TRANS_AAC_ARQC = "ARQC";
    public static final String TRANS_AAC_TC = "TC";
    public static final String CARD_EXPDATE = "EXP. DATE:";
    public static final String BATCH_NO = "TRX. ID:";
    public static final String VOUCHER_NO = "VOUCHER NO:";
    public static final String AUTH_NO = "AUTH NO:";
    public static final String REF_NO = "REF. NO:";
    public static final String TRANS_TYPE   = "TXN. TIPE    : ";
    public static final String AMOUNT       = "NILAI Top Up : ";
    public static final String AMOUNTBONUS  = "BONUS Top Up : ";
    public static final String Bonus        = "BONUS        : ";
    public static final String EC_AMOUNT    = "SALDO        : ";
    public static final String SISA_SALDO   = "Sisa Saldo   : ";
    public static final String Jaminan      = "Deposit      : ";
    public static final String TOTAL_Refund = "Total Refund : ";
    public static final String CARD_AMOUNT = "AMOUNT:";
    public static final String RMB = "Rp ";
    public static final String REFERENCE = "REFERENCE";
    public static final String REPRINT = "********** REPRINT **********";
    public static final String CARDHOLDER_SIGN = "CardHolder Signature";
    public static final String AGREE_TRANS = "I agree these transaction above";
    public static final String SETTLE_SUMMARY = "Settle Sum Receipt";
    public static final String DETAILS = "Detail Transaksi";
}
