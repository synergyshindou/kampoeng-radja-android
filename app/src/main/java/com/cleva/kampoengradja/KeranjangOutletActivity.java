package com.cleva.kampoengradja;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cleva.kampoengradja.adapter.KeranjangAdapter;
import com.cleva.kampoengradja.adapter.KeranjangOutletAdapter;
import com.cleva.kampoengradja.model.AuthCardResp;
import com.cleva.kampoengradja.model.CardIdentifierId;
import com.cleva.kampoengradja.model.CardInfoDataJava;
import com.cleva.kampoengradja.model.CardTerminal;
import com.cleva.kampoengradja.model.OutletItem;
import com.cleva.kampoengradja.model.OutletPurchase;
import com.cleva.kampoengradja.model.OutletPurchaseRequest;
import com.cleva.kampoengradja.utils.Constants;
import com.cleva.kampoengradja.utils.ISOUtil;
import com.cleva.kampoengradja.utils.Method;
import com.cleva.kampoengradja.viewmodel.OutletViewModel;
import com.cleva.kampoengradja.widgets.ElegantNumberButton;
import com.cleva.kampoengradja.widgets.VerticalSpaceItemDecoration;
import com.pos.device.SDKException;
import com.pos.device.config.DevConfig;
import com.pos.device.picc.EmvContactlessCard;
import com.pos.device.picc.MifareClassic;
import com.pos.device.picc.PiccReader;
import com.pos.device.picc.PiccReaderCallback;
import com.pos.device.printer.PrintCanvas;
import com.pos.device.printer.PrintTask;
import com.pos.device.printer.Printer;
import com.pos.device.printer.PrinterCallback;
import com.pos.device.scanner.OnScanListener;
import com.pos.device.scanner.Scanner;

import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.CountDownLatch;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.cleva.kampoengradja.MyApp.readCards;
import static com.cleva.kampoengradja.MyApp.restApi;
import static com.cleva.kampoengradja.MyApp.session;
import static com.cleva.kampoengradja.utils.Constants.AUTH_0;
import static com.cleva.kampoengradja.utils.Constants.AUTH_M1;
import static com.cleva.kampoengradja.utils.Constants.AUTH_M2;
import static com.cleva.kampoengradja.utils.Constants.AUTH_M3;
import static com.cleva.kampoengradja.utils.Constants.AUTH_M4;
import static com.cleva.kampoengradja.utils.Method.printLine;
import static com.cleva.kampoengradja.utils.Method.setFontStyle;
import static com.cleva.kampoengradja.utils.Method.showCustomDialogs;

public class KeranjangOutletActivity extends AppCompatActivity {

    private static final String TAG = "KeranjangOutletActivity";
    View llTop, llBot, llMain, llTap, llSuccess, parent_view;
    AppCompatButton btnBayar, btnBack, btnReprint;
    ImageView imgLoad;
    NestedScrollView nsv;
    AppBarLayout appBarLayout;
    KeranjangOutletAdapter keranjangOutletAdapter;
    OutletViewModel outletViewModel;
    RecyclerView rvkeranjang;
    PiccReader piccReader;

    private EmvContactlessCard emvContactlessCard = null;

    MifareClassic mifareClassic = null;
    TextView tvTotal, tvAmount, tvSaldo;

    String sCNum = "", cardUid = "", sTotal = "", sDiskon = "", sTotalBersih = "", sBayar = "", sBalance = "", sNama = "Radja Resto";
    List<OutletItem> outletItemsz;
    int sum = 0;
    public static Printer printer = null;

    public static PrintTask printTask = null;

    int back = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_keranjang_outlet);
        piccReader = PiccReader.getInstance();
        if (getIntent().hasExtra("NAMA"))
            sNama = getIntent().getStringExtra("NAMA");
        Log.e(TAG, "onCreate: KOA " + sNama);
        initView();

        outletViewModel = ViewModelProviders.of(this).get(OutletViewModel.class);
        outletViewModel.getAllOutletItems().observe(this, new Observer<List<OutletItem>>() {
            @Override
            public void onChanged(@Nullable List<OutletItem> outletItems) {
                keranjangOutletAdapter.submitList(outletItems);
                outletItemsz = outletItems;
                if (outletItems != null) {
                    sum = 0;
                    for (int i = 0; i < outletItems.size(); i++) {
                        Log.i(TAG, "onChanged: " + outletItems.get(i).getQuantity());
                        sum += outletItems.get(i).getQuantity() * outletItems.get(i).getPrice();

                    }
                    tvTotal.setText(Method.getFormatIDRS(sum));
                    if (outletItems.size() == 0) {
                        Snackbar.make(parent_view, "Keranjang kosong", Snackbar.LENGTH_INDEFINITE).setDuration(3000).show();
                        finish();
                    }
                }
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (piccReader != null) {
            piccReader.stopSearchCard();
            try {
                piccReader.release();
            } catch (SDKException e) {
                e.printStackTrace();
            }
        }
    }

    public void initView() {
        parent_view = findViewById(R.id.parent_view);
        imgLoad = findViewById(R.id.imgloads);

        Glide.with(this)
                .load(R.drawable.tap)
                .into(imgLoad);

        llTop = findViewById(R.id.llTop);
        llBot = findViewById(R.id.llBot);
        llMain = findViewById(R.id.llMain);
        llTap = findViewById(R.id.llTap);
        llSuccess = findViewById(R.id.llSuccess);

        btnBayar = findViewById(R.id.btnBayar);
        btnBayar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                llMain.setVisibility(View.GONE);
//                llBot.setVisibility(View.GONE);
//                llTop.setVisibility(View.GONE);
//                llTap.setVisibility(View.VISIBLE);
            }
        });

        btnBack = findViewById(R.id.btnBack);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                outletViewModel.deleteAllOutletItems();
                finish();
            }
        });

        btnReprint = findViewById(R.id.btnReprint);
        btnReprint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                prints(sTotal, sDiskon, sTotalBersih, sBayar, sBalance, false, true);
            }
        });

        nsv = findViewById(R.id.nested_scroll_view);
        appBarLayout = findViewById(R.id.appBar);

        rvkeranjang = findViewById(R.id.rvKeranjang);
        rvkeranjang.setLayoutManager(new LinearLayoutManager(this));
        rvkeranjang.setHasFixedSize(true);
        rvkeranjang.addItemDecoration(new VerticalSpaceItemDecoration(20));

        keranjangOutletAdapter = new KeranjangOutletAdapter(this, new KeranjangOutletAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(OutletItem item) {
                showDialogItem(item);
            }
        });
        rvkeranjang.setAdapter(keranjangOutletAdapter);

        tvTotal = findViewById(R.id.tvTotal);
        tvAmount = findViewById(R.id.tvAmount);
        tvSaldo = findViewById(R.id.tvSaldo);
    }

    @Override
    public void onBackPressed() {
        if (back == 0)
            super.onBackPressed();
        else {
            Log.e(TAG, "onBackPressed: Do nothing");
        }
    }

    public void doTap(View v) {
        nsv.scrollTo(0, 0);
        appBarLayout.setExpanded(false, true);
        llMain.setVisibility(View.GONE);
        llBot.setVisibility(View.GONE);
        llTop.setVisibility(View.GONE);
        llTap.setVisibility(View.VISIBLE);
        getCard();
    }

    public void doPay(View v) {
        appBarLayout.setExpanded(false, true);
        llTap.setVisibility(View.GONE);
        llSuccess.setVisibility(View.VISIBLE);
        outletViewModel.deleteAllOutletItems();
    }

    public void doCancel(View v) {
        showConfirmDialog();
    }

    private void showConfirmDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Peringatan");
        builder.setMessage("Keranjang akan di kosongkan.\n\nLanjut?");
        builder.setPositiveButton("Lanjut", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                outletViewModel.deleteAllOutletItems();
                onBackPressed();
            }
        });
        builder.setNegativeButton("Batal", null);
        builder.show();
    }

    public void doAdd(View v) {
        finish();
    }

    public void doSuccess(int a, int b) {
        tvAmount.setText(Method.getFormatIDRS(a));
        tvSaldo.setText(Method.getFormatIDRS(b));
        appBarLayout.setExpanded(false, true);
        sTotal = String.valueOf(sum);
        sDiskon = "0";
        sTotalBersih = String.valueOf(sum);
        sBayar = String.valueOf(a);
        sBalance = String.valueOf(b);

        prints(sTotal, sDiskon, sTotalBersih, sBayar, sBalance, false, false);
//        prints(, "0", String.valueOf(sum), String.valueOf(a), String.valueOf(b), false, false);
        llTap.setVisibility(View.GONE);
        llSuccess.setVisibility(View.VISIBLE);
        back = 1;
    }

    public void timeoutScan() {
        Snackbar.make(parent_view, "Kartu tidak ditemukan", Snackbar.LENGTH_INDEFINITE).setDuration(3000).show();
        getCard();

    }

    public void getCard() {
        new Thread() {
            @Override
            public void run() {
                Log.i(TAG, "CardManager>>getCard>>NFC");
                piccReader.startSearchCard(0, new PiccReaderCallback() {
                    @Override
                    public void onSearchResult(int i, int i1) {
                        try {
                            Thread.sleep(400);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        Log.i(TAG, "CardManager>>getCard>>NFC>>i=" + i);
                        if (0 == i) {
//                                        listener.callback(handlePICC(i1));
                            Log.i(TAG, "CardManager>>getCard>>NFC_TYPE>>i=" + i1);
                            if (i1 == 0 || i1 == 1) {
                                initMifare();
                            } else
                                timeoutScan();
                        } else {
                            Log.i(TAG, "onSearchResult: not found");
                            timeoutScan();
                        }
                    }
                });
            }
        }.start();
    }

    public void initMifare() {
        try {
            mifareClassic = MifareClassic.connect();

            if (mifareClassic != null) {
                byte[] bUID = mifareClassic.getUID();
                if (bUID != null) {
                    Log.i(TAG, "initMifare: UID = " + ISOUtil.hexString(bUID));
                    getCardAuth(ISOUtil.hexString(bUID), bUID);
                }

            } else {
                Log.e(TAG, "initMifare: FAILED");
                timeoutScan();
            }
        } catch (SDKException e) {
            e.printStackTrace();
        }
    }

    void getCardAuth(String a, byte[] bUid) {
        CardTerminal ct = new CardTerminal();
        ct.setCardUid(a);
        ct.setTerminalSerialNumber(DevConfig.getSN());

        restApi.authCard(ct, session.getAccessToken()).enqueue(new Callback<AuthCardResp>() {
            @Override
            public void onResponse(Call<AuthCardResp> call, Response<AuthCardResp> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        int ares = readCards(mifareClassic, response.body().getAuthKey(), bUid, response.body().getAuthData());
                        Log.i(TAG, "onResponse: ares = " + ares);
                        switch (ares) {
                            case 1:
                                getCardInfo(ct.getCardUid());
                                break;
                            case 0:
                                showConfirmDialog(AUTH_0);
                                break;
                            case -4:
                                showConfirmDialog(AUTH_M4);
                                break;
                            case -3:
                                showConfirmDialog(AUTH_M3);
                                break;
                            case -2:
                                showConfirmDialog(AUTH_M2);
                                break;
                            case -1:
                                showConfirmDialog(AUTH_M1);
                                break;
                        }
                    } else {
                        try {
                            Method.showErorRest(parent_view, response.errorBody().string());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        getCard();
                    }
                } else {
                    try {
                        Method.showErorRest(parent_view, response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    getCard();
                }
            }

            @Override
            public void onFailure(Call<AuthCardResp> call, Throwable t) {
                t.printStackTrace();
                showCustomDialogs(KeranjangOutletActivity.this);

            }
        });

    }

    public void getCardInfo(String cardUID) {
        cardUid = cardUID;
        restApi.cardInfo(cardUid, session.getAccessToken()).enqueue(new Callback<CardInfoDataJava>() {
            @Override
            public void onResponse(Call<CardInfoDataJava> call, Response<CardInfoDataJava> response) {
                if (response.code() == 200) {
                    if (response.body() != null) {
                        // HERE
                        sCNum = response.body().getCardIdentifierId();
                        if (response.body().getBalance() < sum) {
                            showConfirmDialog("Saldo Anda kurang untuk membayar, silahkan topup terlebih dahulu");
                        } else
                            doPurchase();
                    } else {
                        try {
                            Method.showErorRest(parent_view, response.errorBody().string());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        getCard();
                    }
                } else {
                    try {
                        Method.showErorRest(parent_view, response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    getCard();
                }
            }

            @Override
            public void onFailure(Call<CardInfoDataJava> call, Throwable t) {
                t.printStackTrace();
                showCustomDialogs(KeranjangOutletActivity.this);
            }
        });
    }

//    public void getCardInfo(String cardUID){
//        cardUid = cardUID;
//        restApi.getCardIdentifierId(cardUID, session.getAccessToken()).enqueue(new Callback<CardIdentifierId>() {
//            @Override
//            public void onResponse(Call<CardIdentifierId> call, Response<CardIdentifierId> response) {
//                if (response.isSuccessful()){
//                    if (response.body() != null){
//                        sCNum = response.body().getValue();
//                    }
//                }
//            }
//
//            @Override
//            public void onFailure(Call<CardIdentifierId> call, Throwable t) {
//
//            }
//        });
//
//        restApi.cardInfo(cardUID, session.getAccessToken()).enqueue(new Callback<CardInfoDataJava>() {
//            @Override
//            public void onResponse(Call<CardInfoDataJava> call, Response<CardInfoDataJava> response) {
//                if (response.code() == 200) {
//                    if (response.body() != null) {
//                        Log.i(TAG, "onResponse: CARD INFO Balanca " + response.body().getBalance());
//                        if (response.body().getBalance() < sum){
//                            Log.e(TAG, "onResponse: Saldo Kurang");
//                        }
//
//                        doPurchase();
//
//                    }
//                }
//            }
//
//            @Override
//            public void onFailure(Call<CardInfoDataJava> call, Throwable t) {
//
//                t.printStackTrace();
//            }
//        });
//
//    }

    void doPurchase() {
        OutletPurchaseRequest opr = new OutletPurchaseRequest();
        opr.setCardUid(cardUid);
        opr.setOutletItems(outletItemsz);
        opr.setTerminalSerialNumber(DevConfig.getSN());
        opr.setTotalPrice(sum);

        restApi.outletPurchase(opr, session.getAccessToken()).enqueue(new Callback<OutletPurchase>() {
            @Override
            public void onResponse(Call<OutletPurchase> call, Response<OutletPurchase> response) {
                if (response.code() == 200) {
                    if (response.body() != null) {
                        Log.i(TAG, "onResponse: CARD INFO Balanca " + response.body().getTransactionId());
                        doSuccess(response.body().getTotalPrice(), response.body().getRemainingBalance());
                    } else {
                        try {
                            Method.showErorRest(parent_view, response.errorBody().string());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        getCard();
                    }
                } else {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        if (jObjError.has("message"))
                            Snackbar.make(parent_view, jObjError.getString("message"), Snackbar.LENGTH_INDEFINITE).setDuration(3000).show();
                        else
                            Snackbar.make(parent_view, "Terjadi kesalahan, silahkan coba lagi", Snackbar.LENGTH_INDEFINITE).setDuration(3000).show();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<OutletPurchase> call, Throwable t) {
                t.printStackTrace();
                showCustomDialogs(KeranjangOutletActivity.this);

            }
        });
    }


    public void prints(final String a, final String b, final String c, final String d,
                       final String e, final boolean isprev, final boolean isreprint) {
        new Thread() {
            @Override
            public void run() {

                int az = printDetails(a, b, c, d, e, isprev, isreprint);
                Log.i(TAG, "run: " + az);
            }
        }.start();
    }

    public int printDetails(String a, String b, String c, String d, String e, boolean isPreview,
                            boolean isReprint) {
        printTask = new PrintTask();
        printTask.setGray(130);
        int ret = -1;
        printer = Printer.getInstance();
        if (printer == null) {
            ret = 112;
        } else {

            SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yy, HH:mm", new Locale("ID"));
            String currentDateandTime = sdf.format(new Date());

            Bitmap icon = BitmapFactory.decodeResource(this.getResources(),
                    R.drawable.kampoeng_log);

            Bitmap scaled = Bitmap.createScaledBitmap(icon, 320, 120, true);

            final PrintCanvas canvas = new PrintCanvas();
            Paint paint = new Paint();

            setFontStyle(this, paint, 2, true);
            if (isReprint)
                canvas.drawText(Constants.REPRINT, paint);
            canvas.drawText(" ", paint);
            canvas.drawBitmap(scaled, paint);
            setFontStyle(this, paint, 2, true);
            canvas.drawText(" ", paint);
            setFontStyle(this, paint, 2, true);
            canvas.drawText("--- Outlet "+sNama, paint);
            printLine(this, paint, canvas);
            canvas.drawText(currentDateandTime + "   " + DevConfig.getSN(), paint);
            printLine(this, paint, canvas);
            setFontStyle(this, paint, 2, true);
            canvas.drawText("No. Card  " + Method.splitCardNum(sCNum).trim(), paint);
            canvas.drawText(" ", paint);
            setFontStyle(this, paint, 2, true);
            for (int i = 0; i < outletItemsz.size(); i++) {
                setFontStyle(this, paint, 2, true);
                canvas.drawText(outletItemsz.get(i).getTitle(), paint);
                String ber = outletItemsz.get(i).getQuantity() + "x " + Method.getFormatIDR(outletItemsz.get(i).getPrice()) +
                        Method.getFormatIDR(outletItemsz.get(i).getPrice() * outletItemsz.get(i).getQuantity());
                String dep = outletItemsz.get(i).getQuantity() + "x " + Method.getFormatIDR(outletItemsz.get(i).getPrice());
                String bel = Method.getFormatIDR(outletItemsz.get(i).getPrice() * outletItemsz.get(i).getQuantity());
                int kos = 29 - ber.length();
                StringBuilder spa = new StringBuilder();
                for (int j = 0; j < kos; j++) {
                    spa.append(" ");
                }
                canvas.drawText(dep + spa + bel, paint);
            }

            printLine(this, paint, canvas);

            setFontStyle(this, paint, 2, true);
            canvas.drawText("Harga        : " + Method.getFormatIDR(a), paint);
            canvas.drawText("Diskon       : " + Method.getFormatIDR(b), paint);
            printLine(this, paint, canvas);
            setFontStyle(this, paint, 2, true);
            canvas.drawText("Total Harga  : " + Method.getFormatIDR(c), paint);
            canvas.drawText("Saldo Potong : " + Method.getFormatIDR(d), paint);
            canvas.drawText("Saldo Sisa   : " + Method.getFormatIDR(e), paint);

            canvas.drawText(" ", paint);
            canvas.drawText("------- Terima  Kasih -------", paint);
            canvas.drawText(" ", paint);

            if (isPreview) {
                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {

                        // Stuff that updates the UI
                        imgLoad.setImageBitmap(canvas.getBitmap());

                    }
                });
            } else {
                ret = printData(canvas);
            }

            if (printer != null) {
                printer = null;
            }
        }
        return ret;
    }

    private int printData(PrintCanvas pCanvas) {
        final CountDownLatch latch = new CountDownLatch(1);
        printer = Printer.getInstance();
        int ret = printer.getStatus();
        if (Printer.PRINTER_STATUS_PAPER_LACK == ret) {
            long start = SystemClock.uptimeMillis();
            while (true) {
                if (SystemClock.uptimeMillis() - start > 60 * 1000) {
                    ret = Printer.PRINTER_STATUS_PAPER_LACK;
                    break;
                }
                if (printer.getStatus() == Printer.PRINTER_OK) {
                    ret = Printer.PRINTER_OK;
                    break;
                } else {
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                    }
                }
            }
        }
        if (ret == Printer.PRINTER_OK) {
            printTask.setPrintCanvas(pCanvas);
            printer.startPrint(printTask, new PrinterCallback() {
                @Override
                public void onResult(int i, PrintTask printTask) {
                    latch.countDown();
                }
            });
            try {
                latch.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return ret;
    }

    public void showDialogItem(final OutletItem item) {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.dialog_tiket_keranjang);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(true);

        final TextView tvTitle, tvPrice, tvType, tvValid;

        tvTitle = dialog.findViewById(R.id.tvTitle);
        tvPrice = dialog.findViewById(R.id.tvPrice);
        tvType = dialog.findViewById(R.id.tvType);
        tvValid = dialog.findViewById(R.id.tvValid);

        tvTitle.setText(item.getTitle());
        tvPrice.setText(Method.getFormatIDRS(item.getPrice()));
        ImageView img = dialog.findViewById(R.id.image);
        if (item.getImagePath() != null) {
            // byte[] imageByteArray = Base64.decode(item.getImageBase64(), Base64.DEFAULT);
            // here imageBytes is base64String

//            Glide.with(KeranjangOutletActivity.this)
//                    .load(Constants.BASE_URL_IMAGE+item.getImagePath())
//                    .into(img);
        }

        final ElegantNumberButton button = dialog.findViewById(R.id.number_button);
        button.setNumber(String.valueOf(item.getQuantity()));
        button.setOnClickListener(new ElegantNumberButton.OnClickListener() {
            @Override
            public void onClick(View view) {
//                if (button.getNumbers() == 0)
//                    dialog.hide();
            }
        });

        AppCompatButton btn = dialog.findViewById(R.id.bt_add_to_cart);
        btn.setText("Ubah Transaksi");
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                List<OutletItem> getTiket = outletViewModel.getTiketById(item.getItemId());
                if (getTiket != null) {
                    Log.i(TAG, "onClick: " + getTiket.size());

                    OutletItem tpr;
                    if (getTiket.size() == 1) {
                        tpr = getTiket.get(0);
                        dialog.hide();

                        tpr.setItemId(item.getItemId());
                        tpr.setTitle(item.getTitle());
                        tpr.setPrice(item.getPrice());
                        tpr.setImagePath(item.getImagePath());
                        tpr.setQuantity(button.getNumbers());
                        if (button.getNumbers() == 0) {
                            outletViewModel.delete(tpr);
                            Snackbar.make(parent_view, tvTitle.getText().toString() + " berhasil ubah transaksi", Snackbar.LENGTH_INDEFINITE).setDuration(3000).show();

                        } else {
                            outletViewModel.update(tpr);
                            Snackbar.make(parent_view, tvTitle.getText().toString() + " berhasil ubah transaksi", Snackbar.LENGTH_INDEFINITE).setDuration(3000).show();

                        }
                    } else {
                        Log.e(TAG, "onClick: Size 0");
                        Snackbar.make(parent_view, tvTitle.getText().toString() + " berhasil ubah transaksi", Snackbar.LENGTH_INDEFINITE).setDuration(3000).show();
                        dialog.hide();

                        item.setQuantity(button.getNumbers());
                        outletViewModel.insert(item);
                    }

                } else {
                    Log.i(TAG, "onClick: else ");
                    Snackbar.make(parent_view, tvTitle.getText().toString() + " berhasil ubah transaksi", Snackbar.LENGTH_INDEFINITE).setDuration(3000).show();
                    dialog.hide();

                    item.setQuantity(button.getNumbers());
                    outletViewModel.insert(item);
                }
            }
        });

        dialog.show();
    }

    private void showConfirmDialog(String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Kampoeng Radja");
        builder.setMessage(msg);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                getCard();
            }
        });
        builder.show();
    }
}
