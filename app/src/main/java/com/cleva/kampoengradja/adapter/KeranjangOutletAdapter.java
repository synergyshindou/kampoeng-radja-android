package com.cleva.kampoengradja.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.recyclerview.extensions.ListAdapter;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cleva.kampoengradja.R;
import com.cleva.kampoengradja.model.OutletItem;
import com.cleva.kampoengradja.utils.Constants;
import com.cleva.kampoengradja.utils.Method;
import com.cleva.kampoengradja.widgets.ElegantNumberButton;


public class KeranjangOutletAdapter extends ListAdapter<OutletItem, KeranjangOutletAdapter.OutletItemHolder> {
    private OnItemClickListener listener;
    Context mContext;
    private static final String TAG = "KeranjangOutletAdapter";

    public KeranjangOutletAdapter(Context context, OnItemClickListener listener) {
        super(DIFF_CALLBACK);
        this.mContext = context;
        this.listener = listener;
    }

    private static final DiffUtil.ItemCallback<OutletItem> DIFF_CALLBACK = new DiffUtil.ItemCallback<OutletItem>() {
        @Override
        public boolean areItemsTheSame(OutletItem oldItem, OutletItem newItem) {
            boolean s = oldItem.getItemId() == newItem.getItemId();
            Log.i(TAG, "areItemsTheSame: "+s);
            return oldItem.getItemId() == newItem.getItemId();
        }

        @Override
        public boolean areContentsTheSame(OutletItem oldItem, OutletItem newItem) {
            return oldItem.getTitle().equals(newItem.getTitle()) &&
                    oldItem.getImagePath().equals(newItem.getImagePath()) &&
                    oldItem.getPrice() == newItem.getPrice() &&
                    oldItem.getQuantity() == newItem.getQuantity();
        }
    };

    @NonNull
    @Override
    public OutletItemHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_paket_keranjang, parent, false);
        return new OutletItemHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull OutletItemHolder holder, int position) {
        OutletItem currentOutletItem = getItem(position);
        holder.tvName.setText(currentOutletItem.getTitle());
        holder.tvPrice.setText(Method.getFormatIDRS(currentOutletItem.getPrice()));
        if (currentOutletItem.getImagePath() != null) {
            // byte[] imageByteArray = Base64.decode(currentOutletItem.getImagePath(), Base64.DEFAULT);
            // here imageBytes is base64String

            Glide.with(mContext)
                    .load(Constants.BASE_URL_IMAGE+currentOutletItem.getImagePath())
                    .into(holder.img);
        }
        holder.ele.setNumber(String.valueOf(currentOutletItem.getQuantity()));
    }

    public OutletItem getOutletItemAt(int position) {
        return getItem(position);
    }

    class OutletItemHolder extends RecyclerView.ViewHolder {
        TextView tvName, tvPrice;
        ImageView img;
        ElegantNumberButton ele;

        public OutletItemHolder(View view) {
            super(view);
            tvName = view.findViewById(R.id.tvTitle);
            tvPrice = view.findViewById(R.id.tvPrice);
            img = view.findViewById(R.id.img);
            ele = view.findViewById(R.id.number_button);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    if (listener != null && position != RecyclerView.NO_POSITION) {
                        listener.onItemClick(getItem(position));
                    }
                }
            });
        }
    }

    public interface OnItemClickListener {
        void onItemClick(OutletItem note);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }
}