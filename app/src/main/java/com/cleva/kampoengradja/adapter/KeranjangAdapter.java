package com.cleva.kampoengradja.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.recyclerview.extensions.ListAdapter;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cleva.kampoengradja.R;
import com.cleva.kampoengradja.model.TiketParkirResp;
import com.cleva.kampoengradja.utils.Constants;
import com.cleva.kampoengradja.utils.Method;
import com.cleva.kampoengradja.widgets.ElegantNumberButton;


public class KeranjangAdapter extends ListAdapter<TiketParkirResp, KeranjangAdapter.TiketParkirRespHolder> {
    private OnItemClickListener listener;
    Context mContext;

    public KeranjangAdapter(Context context) {
        super(DIFF_CALLBACK);
        this.mContext = context;
    }

    private static final DiffUtil.ItemCallback<TiketParkirResp> DIFF_CALLBACK = new DiffUtil.ItemCallback<TiketParkirResp>() {
        @Override
        public boolean areItemsTheSame(TiketParkirResp oldItem, TiketParkirResp newItem) {
            return oldItem.getItemId() == newItem.getItemId();
        }

        @Override
        public boolean areContentsTheSame(TiketParkirResp oldItem, TiketParkirResp newItem) {
            return oldItem.getTitle().equals(newItem.getTitle()) &&
                    oldItem.getImagePath().equals(newItem.getImagePath()) &&
                    oldItem.getPrice() == newItem.getPrice();
        }
    };

    @NonNull
    @Override
    public TiketParkirRespHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_paket_keranjang, parent, false);
        return new TiketParkirRespHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull TiketParkirRespHolder holder, int position) {
        TiketParkirResp currentTiketParkirResp = getItem(position);
        holder.tvName.setText(currentTiketParkirResp.getTitle());
        holder.tvPrice.setText(Method.getFormatIDRS(currentTiketParkirResp.getPrice()));
        if (currentTiketParkirResp.getImagePath() != null) {
            // byte[] imageByteArray = Base64.decode(currentTiketParkirResp.getImagePath(), Base64.DEFAULT);
            // here imageBytes is base64String

            Glide.with(mContext)
                    .load(Constants.BASE_URL_IMAGE+currentTiketParkirResp.getImagePath())
                    .into(holder.img);
        }
        holder.ele.setNumber(String.valueOf(currentTiketParkirResp.getQuantity()));
    }

    public TiketParkirResp getTiketParkirRespAt(int position) {
        return getItem(position);
    }

    class TiketParkirRespHolder extends RecyclerView.ViewHolder {
        TextView tvName, tvPrice;
        ImageView img;
        ElegantNumberButton ele;

        public TiketParkirRespHolder(View view) {
            super(view);
            tvName = view.findViewById(R.id.tvTitle);
            tvPrice = view.findViewById(R.id.tvPrice);
            img = view.findViewById(R.id.img);
            ele = view.findViewById(R.id.number_button);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    if (listener != null && position != RecyclerView.NO_POSITION) {
                        listener.onItemClick(getItem(position));
                    }
                }
            });
        }
    }

    public interface OnItemClickListener {
        void onItemClick(TiketParkirResp note);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }
}