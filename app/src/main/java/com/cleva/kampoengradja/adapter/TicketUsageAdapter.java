package com.cleva.kampoengradja.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.Space;
import android.widget.TextView;

import com.cleva.kampoengradja.R;
import com.cleva.kampoengradja.model.TicketUsageHistory;
import com.cleva.kampoengradja.utils.Method;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dell_Cleva on 04/01/2017.
 */

public class TicketUsageAdapter extends RecyclerView.Adapter<TicketUsageAdapter.MyViewHolder> implements Filterable {

    public interface OnItemClickListener {
        void onItemClick(TicketUsageHistory item);
    }

    private final OnItemClickListener listener;

    private List<TicketUsageHistory> horizontalList;
    private List<TicketUsageHistory> orig;
    private Context context;
    CustomFilter filter;

    public TicketUsageAdapter(Context context, List<TicketUsageHistory> horizontalList, OnItemClickListener listener) {
        this.horizontalList = horizontalList;
        this.orig = horizontalList;
        this.context = context;
        this.listener = listener;
    }

    @Override
    public Filter getFilter() {

        if (filter == null)
            filter = new CustomFilter();

        return filter;
    }

    class CustomFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            FilterResults results = new FilterResults();

            if (constraint != null && constraint.length() > 0) {

                constraint = constraint.toString().toUpperCase();

                List<TicketUsageHistory> filters = new ArrayList<>();

                for (int i = 0; i < orig.size(); i++) {
                    if (orig.get(i).getTimestamp().toUpperCase().contains(constraint)) {
                        TicketUsageHistory r = orig.get(i);
                        filters.add(r);
                    }
                }

                results.count = filters.size();
                results.values = filters;

            } else {
                results.count = orig.size();
                results.values = orig;
            }

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults result) {

            horizontalList = (ArrayList<TicketUsageHistory>) result.values;
            notifyDataSetChanged();

        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_history_topup, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        holder.bind(horizontalList.get(position), listener);

    }

    @Override
    public int getItemCount() {
        return horizontalList.size();
    }


    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvAmount, tvTopupAdd, tvBonus, tvDate;
        Space space;

        MyViewHolder(View view) {
            super(view);
            tvAmount = view.findViewById(R.id.tvAmount);
            tvTopupAdd = view.findViewById(R.id.tvTopupAdd);
            tvBonus = view.findViewById(R.id.tvBonus);
            tvDate = view.findViewById(R.id.tvDate);
            space = view.findViewById(R.id.spaces);

        }

        void bind(final TicketUsageHistory item, final OnItemClickListener listener) {

            tvAmount.setText(item.getAttractionName());
            if (item.getBalanceUsed() > 0) {
                tvTopupAdd.setText("Saldo terpotong: " +Method.getFormatIDR(item.getBalanceUsed()));
                space.setVisibility(View.VISIBLE);
            } else {
                tvTopupAdd.setText("");
                space.setVisibility(View.GONE);
            }

            if (item.getBonusUsed() > 0) {
                tvBonus.setText("Bonus terpotong: " + Method.getFormatIDR(item.getBonusUsed()));
            } else
                tvBonus.setText("");

            tvDate.setText(item.getTimestamp());
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(item);
                }
            });
//            bg.setBackgroundColor(Color.parseColor(item.getsChannelColor()));
        }
    }
}
