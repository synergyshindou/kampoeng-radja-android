package com.cleva.kampoengradja.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cleva.kampoengradja.R;
import com.cleva.kampoengradja.model.TiketParkirResp;
import com.cleva.kampoengradja.utils.Constants;
import com.cleva.kampoengradja.utils.Method;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dell_Cleva on 04/01/2017.
 */

public class KeranjangTicketParkingAdapter extends RecyclerView.Adapter<KeranjangTicketParkingAdapter.MyViewHolder> implements Filterable {

    public interface OnItemClickListener {
        void onItemClick(TiketParkirResp item);
    }

    private final OnItemClickListener listener;

    private List<TiketParkirResp> horizontalList;
    private List<TiketParkirResp> orig;
    private Context context;
    CustomFilter filter;

    public KeranjangTicketParkingAdapter(Context context, List<TiketParkirResp> horizontalList, OnItemClickListener listener) {
        this.horizontalList = horizontalList;
        this.orig = horizontalList;
        this.context = context;
        this.listener = listener;
    }

    @Override
    public Filter getFilter() {

        if (filter == null)
            filter = new CustomFilter();

        return filter;
    }

    class CustomFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            FilterResults results = new FilterResults();

            if (constraint != null && constraint.length() > 0) {

                constraint = constraint.toString().toUpperCase();

                List<TiketParkirResp> filters = new ArrayList<>();

                for (int i = 0; i < orig.size(); i++) {
                    if (orig.get(i).getTitle().toUpperCase().contains(constraint)) {
                        TiketParkirResp r = orig.get(i);
                        filters.add(r);
                    }
                }

                results.count = filters.size();
                results.values = filters;

            } else {
                results.count = orig.size();
                results.values = orig;
            }

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults result) {

            horizontalList = (ArrayList<TiketParkirResp>) result.values;
            notifyDataSetChanged();

        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_paket_keranjang, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        holder.bind(horizontalList.get(position), listener);

    }

    @Override
    public int getItemCount() {
        return horizontalList.size();
    }


    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvName, tvPrice;
        ImageView img;

        MyViewHolder(View view) {
            super(view);
            tvName = view.findViewById(R.id.tvTitle);
            tvPrice = view.findViewById(R.id.tvPrice);
            img = view.findViewById(R.id.img);

        }

        void bind(final TiketParkirResp item, final OnItemClickListener listener) {

            tvName.setText(item.getTitle());
            tvPrice.setText(Method.getFormatIDRS(item.getPrice()));

            if (item.getImagePath() != null) {
                // byte[] imageByteArray = Base64.decode(item.getImageBase64(), Base64.DEFAULT);
                // here imageBytes is base64String

                Glide.with(context)
                        .load(Constants.BASE_URL_IMAGE+item.getImagePath())
                        .into(img);
            }

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(item);
                }
            });
//            bg.setBackgroundColor(Color.parseColor(item.getsChannelColor()));
        }
    }
}
