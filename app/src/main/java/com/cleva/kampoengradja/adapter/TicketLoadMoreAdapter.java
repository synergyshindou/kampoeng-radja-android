package com.cleva.kampoengradja.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cleva.kampoengradja.R;
import com.cleva.kampoengradja.model.TiketParkirResp;
import com.cleva.kampoengradja.utils.Constants;
import com.cleva.kampoengradja.utils.Method;
import com.cleva.kampoengradja.utils.OnLoadMoreListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dell_Cleva on 04/01/2017.
 */

public class TicketLoadMoreAdapter extends RecyclerView.Adapter implements Filterable {

    public interface OnItemClickListener {
        void onItemClick(TiketParkirResp item);

        void onItemClick(TiketParkirResp item, int w);
    }

    private final int VIEW_ITEM = 1;
    private final int VIEW_PROG = 0;

    private final OnItemClickListener listener;

    private List<TiketParkirResp> horizontalList;
    private List<TiketParkirResp> orig;
    private Context context;
    CustomFilter filter;

    private int visibleThreshold = 3;
    private int lastVisibleItem, totalItemCount;
    private boolean loading;
    private OnLoadMoreListener onLoadMoreListener;

    public TicketLoadMoreAdapter(Context context, List<TiketParkirResp> horizontalList, OnItemClickListener listener,
                                 RecyclerView recyclerView) {
        this.horizontalList = horizontalList;
        this.orig = horizontalList;
        this.context = context;
        this.listener = listener;
//        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {
//
//            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView
//                    .getLayoutManager();
//            recyclerView
//                    .addOnScrollListener(new RecyclerView.OnScrollListener() {
//                        @Override
//                        public void onScrolled(RecyclerView recyclerView,
//                                               int dx, int dy) {
//                            super.onScrolled(recyclerView, dx, dy);
//
//                            totalItemCount = linearLayoutManager.getItemCount();
//                            lastVisibleItem = linearLayoutManager
//                                    .findLastVisibleItemPosition();
//                            if (!loading
//                                    && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
//                                // End has been reached
//                                // Do something
//                                if (onLoadMoreListener != null) {
//                                    onLoadMoreListener.onLoadMore();
//                                }
//                                loading = true;
//                            }
//                        }
//                    });
//        }
    }

    @Override
    public Filter getFilter() {

        if (filter == null)
            filter = new CustomFilter();

        return filter;
    }

    class CustomFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            FilterResults results = new FilterResults();

            if (constraint != null && constraint.length() > 0) {

                constraint = constraint.toString().toUpperCase();

                List<TiketParkirResp> filters = new ArrayList<>();

                for (int i = 0; i < orig.size(); i++) {
                    if (orig.get(i).getTitle().toUpperCase().contains(constraint)) {
                        TiketParkirResp r = orig.get(i);
                        filters.add(r);
                    }
                }

                results.count = filters.size();
                results.values = filters;

            } else {
                results.count = orig.size();
                results.values = orig;
            }

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults result) {

            horizontalList = (ArrayList<TiketParkirResp>) result.values;
            notifyDataSetChanged();

        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_ticketing, parent, false);

        RecyclerView.ViewHolder vh;
        if (viewType == VIEW_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.item_paket_detail, parent, false);

            vh = new MyViewHolder(v);
        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.progressbar, parent, false);

            vh = new ProgressViewHolder(v);
        }
        return vh;

//        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof MyViewHolder) {
            ((MyViewHolder) holder).bind(horizontalList.get(position), listener);
        } else {
            ((ProgressViewHolder) holder).progressBar.setIndeterminate(true);
        }
    }

//    @Override
//    public void onBindViewHolder(final MyViewHolder holder, final int position) {
//        holder.bind(horizontalList.get(position), listener);
//
//    }

    @Override
    public int getItemCount() {
        return horizontalList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return horizontalList.get(position) != null ? VIEW_ITEM : VIEW_PROG;
    }

    public void setLoaded() {
        loading = false;
    }

    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvName, tvPrice;
        ImageView img;
        AppCompatButton btnDetail, btnKeranjang;

        MyViewHolder(View view) {
            super(view);
            tvName = view.findViewById(R.id.tvTitle);
            tvPrice = view.findViewById(R.id.tvPrice);
            img = view.findViewById(R.id.img);
            btnDetail = view.findViewById(R.id.btnDetail);
            btnKeranjang = view.findViewById(R.id.btnKeranjang);

        }

        void bind(final TiketParkirResp item, final OnItemClickListener listener) {

            tvName.setText(item.getTitle());
            tvPrice.setText(Method.getFormatIDRS(item.getPrice()));

            if (item.getImagePath() != null) {
//                byte[] imageByteArray = Base64.decode(item.getImageBase64(), Base64.DEFAULT);
                // here imageBytes is base64String

//                Glide.with(context)
//                        .load(Constants.BASE_URL_IMAGE+item.getImagePath())
//                        .into(img);
            }

            btnDetail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(item, 0);
                }
            });

            btnKeranjang.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(item, 1);
                }
            });

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(item);
                }
            });
//            bg.setBackgroundColor(Color.parseColor(item.getsChannelColor()));
        }
    }


    public static class ProgressViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public ProgressViewHolder(View v) {
            super(v);
            progressBar = v.findViewById(R.id.progressBar1);
        }
    }
}
